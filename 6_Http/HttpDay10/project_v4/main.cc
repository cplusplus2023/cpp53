#include "CloudiskServer.hpp"

#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    CloudiskServer cloudiskServer(1);
    cloudiskServer.loadModules();
    cloudiskServer.start(8888);
}


int main()
{
    test0();
    return 0;
}

