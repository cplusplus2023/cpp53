#ifndef __WD_Publisher_HPP__ 
#define __WD_Publisher_HPP__ 

#include <string>
using std::string;

#include <SimpleAmqpClient/SimpleAmqpClient.h>

using AmqpClient::Channel;
using AmqpClient::BasicMessage;

class Publisher
{
public:
    Publisher() 
    : _channel(Channel::Create())
    {}

    void doPublish(const string & exchange, const string & routingkey, const string & msg)
    {
        BasicMessage::ptr_t message = BasicMessage::Create(msg);
        _channel->BasicPublish(exchange, routingkey, message);
    }

    ~Publisher() {}

private:

    Channel::ptr_t _channel;
};

#endif

