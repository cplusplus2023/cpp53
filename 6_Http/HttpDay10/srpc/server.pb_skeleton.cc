
#include "user.srpc.h"
#include "workflow/WFFacilities.h"

#include <iostream>
using std::cout;
using std::endl;
using std::string;

using namespace srpc;

static WFFacilities::WaitGroup wait_group(1);

void sig_handler(int signo)
{
	wait_group.done();
}

class UserSignupServiceImpl : public UserSignup::Service
{
public:

	void Signup(ReqSignup *request, RespSignup *response, 
                srpc::RPCContext *ctx) override
    {
        //服务器端的用户注册服务函数Signup被调用时，已经接收到了request
        //1. 解析请求
        string username = request->username();
        string password = request->password();
        cout << "username:" << username << endl;
        cout << "password:" << password << endl;
        //2. 生成响应
        if(username == "admin" && password == "123") {
            //注册成功
            response->set_code(200);
            response->set_msg("OK");
        } else {
            response->set_code(404);
            response->set_msg("Signup Failed");
        }
    }
};

int main()
{
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	unsigned short port = 1412;
	SRPCServer server;

	UserSignupServiceImpl usersignup_impl;
	server.add_service(&usersignup_impl);

	server.start(port);
	wait_group.wait();
	server.stop();
	google::protobuf::ShutdownProtobufLibrary();
	return 0;
}
