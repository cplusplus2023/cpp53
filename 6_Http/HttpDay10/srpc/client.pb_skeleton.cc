
#include "user.srpc.h"
#include "workflow/WFFacilities.h"

#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;

using namespace srpc;

static WFFacilities::WaitGroup wait_group(1);

void sig_handler(int signo)
{
	wait_group.done();
}

static void signup_done(RespSignup *response, srpc::RPCContext *context)
{
    //当服务器发送过来的数据之后，就可以在这里进行解析
    cout << "code:" << response->code() << endl;
    cout << "msg:" << response->msg() << endl;
}

int main()
{
    signal(SIGINT, sig_handler);
	GOOGLE_PROTOBUF_VERIFY_VERSION;
	const char *ip = "127.0.0.1";
	unsigned short port = 1412;

	UserSignup::SRPCClient client(ip, port);

	// example for RPC method call
	ReqSignup signup_req;
    signup_req.set_username("admin");
    signup_req.set_password("1234");
    //异步接口
	client.Signup(&signup_req, signup_done);

	wait_group.wait();
	google::protobuf::ShutdownProtobufLibrary();
	return 0;
}
