#include "user.pb.h"
#include <iostream>
using std::cout;
using std::endl;
using std::string;

void test0()
{
    ReqSignup req;
    req.set_username("admin");
    req.set_password("123");
    //进行序列化
    string serializeStr;
    req.SerializeToString(&serializeStr);
    //cout << "serializeStr:" << serializeStr << endl;
    cout << "serilizeStr.size:" << serializeStr.size() << endl;
    for(size_t i = 0; i < serializeStr.size(); ++i) {
        printf("%02x", serializeStr[i]);
    }
    printf("\n");
}


int main()
{
    test0();
    return 0;
}

