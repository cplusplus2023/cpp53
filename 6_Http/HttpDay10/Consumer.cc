#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <SimpleAmqpClient/SimpleAmqpClient.h>

void test0()
{
    using namespace AmqpClient;
    //1. 创建通道对象
    Channel::ptr_t channel = Channel::Create();
    //2. 指定要获取消息的队列
    channel->BasicConsume("queue1");

    while(1) {
        Envelope::ptr_t envelope;
        //3. 获取消息
        bool ret = channel->BasicConsumeMessage(envelope, 3000);
        if(ret) {
            string msg = envelope->Message()->Body();
            cout << "msg:" << msg << endl;
        } else {
            cout << "timeout." << endl;
        }
    }
}


int main()
{
    test0();
    return 0;
}

