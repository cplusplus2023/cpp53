#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <SimpleAmqpClient/SimpleAmqpClient.h>

struct AmqpInfo
{
    string exchange = "myexchange";
    string routingkey = "key1";
};

void test0()
{
    using namespace AmqpClient;
    AmqpInfo info;
    BasicMessage::ptr_t message = BasicMessage::Create("this is a third test");
    Channel::ptr_t channel = Channel::Create();
    channel->BasicPublish(info.exchange, info.routingkey, message);
    pause();
}


int main()
{
    test0();
    return 0;
}

