#include "OssUploader.hpp"
#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <nlohmann/json.hpp>
#include <SimpleAmqpClient/SimpleAmqpClient.h>


using AmqpClient::Channel;
using AmqpClient::Envelope;
using AmqpClient::BasicMessage;


class Consumer
{
public:
    Consumer(const string & queuename)
    : _channel(Channel::Create())
    {
        _channel->BasicConsume(queuename);
    }

    void doConsume()
    {
        while(1) {
            Envelope::ptr_t  envelope;
            bool ret = _channel->BasicConsumeMessage(envelope, 1000);
            if(ret) {
                string msg = envelope->Message()->Body();
                cout << "msg:" << msg << endl;
                using Json = nlohmann::json;
                Json uploadInfo = Json::parse(msg);
                string filepath = uploadInfo["filepath"];
                string ObjectName = uploadInfo["ObjectName"];
                _ossUploader.doUploadFile(filepath, ObjectName);
            } else {
                cout << "timeout." << endl;
            }
        }
    }

private:
    OssUploader _ossUploader;
    Channel::ptr_t _channel;
};

void test0()
{
    Consumer consumer("oss_queue");
    consumer.doConsume();
}


int main()
{
    test0();
    return 0;
}

