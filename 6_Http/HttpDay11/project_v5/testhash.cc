#include "hash.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0()
{
    Hash hash("Makefile");
    cout << hash.sha1() << " Makefile " << endl;
}


int main()
{
    test0();
    return 0;
}

