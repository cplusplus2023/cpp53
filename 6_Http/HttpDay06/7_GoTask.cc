#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>


//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}

void add(int x, int y, int & z)
{
    z = x + y;
}

int number = 10;

void test0()
{
    signal(SIGINT, sighandler);
    int a = 1, b = 2, c = -1;
    //为了表示采用的是引用传递，需要使用引用包装器std::ref
    auto goTask = WFTaskFactory::create_go_task("test", add, a, b, std::ref(c));
    goTask->set_callback([&](WFGoTask * ){
        printf("%d + %d = %d, number: %d\n", a, b, c, number);
    });
    goTask->start();

    waitGroup.wait();
}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

