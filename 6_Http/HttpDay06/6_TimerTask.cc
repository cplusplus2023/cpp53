#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>


//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}

void timerCallback(WFTimerTask * timertask)
{
    //...do something
    printf("timerCallback is running.\n");

    //设置下一个定时器任务, 周期性的执行
    auto nextTask = WFTaskFactory::create_timer_task(1000 * 1000, timerCallback);
    series_of(timertask)->push_back(nextTask);
}

void test0()
{
    signal(SIGINT, sighandler);
    auto timerTask = WFTaskFactory::create_timer_task(1000 * 1000, timerCallback);
    timerTask->start();
    waitGroup.wait();
}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

