#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;

#include <workflow/WFFacilities.h>


//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}


void test0()
{
    signal(SIGINT, sighandler);
    waitGroup.wait();
}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

