#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>
#include <workflow/MySQLMessage.h>
#include <workflow/MySQLResult.h>

using std::vector;

//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}

void mysqlCallback(WFMySQLTask * mysqltask)
{
    printf("mysqlCallback is running.\n");
    //0. 对任务的状态进行检测
    int state = mysqltask->get_state();
    int error = mysqltask->get_error();
    if(state != WFT_STATE_SUCCESS) {
        printf("%s\n", WFGlobal::get_error_string(state, error));
        return;
    }
    //1. 检测SQL语句是否存在语法错误
    auto resp = mysqltask->get_resp();
    if(resp->get_packet_type() == MYSQL_PACKET_ERROR) {
        printf("ERROR %d: %s\n", resp->get_error_code(),
               resp->get_error_msg().c_str());
        return;
    }
    printf("sql sentence is ok.\n");
    using namespace protocol;
    MySQLResultCursor cursor(resp);
    if(cursor.get_cursor_status() == MYSQL_STATUS_OK) {
        //2. 写操作
        printf("Query OK. %llu row affected.\n",cursor.get_affected_rows());
    } else if(cursor.get_cursor_status() == MYSQL_STATUS_GET_RESULT) {
        //3. 读操作
        const MySQLField * const * arrFields = cursor.fetch_fields();
        for(int i = 0; i < cursor.get_field_count(); ++i) {
            cout << arrFields[i]->get_name() << " "
                 << arrFields[i]->get_table() << " " 
                 << arrFields[i]->get_db() << " "
                 << datatype2str(arrFields[i]->get_data_type()) << endl;
        }
        cout << endl;
        vector<vector<MySQLCell>> matrix;
        cursor.fetch_all(matrix);
        for(auto & row : matrix) {
            for(auto & cell : row) {
                if(cell.is_int()) {
                    cout << cell.as_int() << " ";
                } else if(cell.is_string()) {
                    cout << cell.as_string() << " ";
                } else if(cell.is_datetime()) {
                    cout << cell.as_datetime() << " ";
                }
            }
            cout << endl;
        }
    }
}


void test0()
{
    signal(SIGINT, sighandler);

    //1. 创建MySQL任务
    string url("mysql://root:1234@localhost");
    auto mysqlTask = WFTaskFactory::create_mysql_task(url, 1, mysqlCallback);
    string sql("select * from cloudisk.tbl_user_token");
    //2. 设置任务的属性
    mysqlTask->get_req()->set_query(sql);
    //3. 将任务交给框架来调度执行
    mysqlTask->start();

    waitGroup.wait();
}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

