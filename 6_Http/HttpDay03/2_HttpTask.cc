#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>


//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}

void httpCallback(WFHttpTask * )
{
    printf("httpCallback is runnging.\n");
}


void test0()
{
    signal(SIGINT, sighandler);

    //1. 创建HTTP任务
    string url("www.baid.com");
    WFHttpTask * httpTask = WFTaskFactory::create_http_task(url, 1, 1, httpCallback);

    //2. 将任务交给框架来调度执行
    httpTask->start();//启动另外一个线程来运行任务

    waitGroup.wait();

}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

