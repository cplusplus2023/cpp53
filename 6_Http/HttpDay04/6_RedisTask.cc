#include <signal.h>
#include <unistd.h>

#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <workflow/WFFacilities.h>
#include <workflow/WFTaskFactory.h>
#include <workflow/RedisMessage.h>

using std::vector;

//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}

void redisCallback(WFRedisTask * redistask)
{
    printf("redisCallback is running.\n");
    //0. 对任务的状态进行检测
    int state = redistask->get_state();
    int error = redistask->get_error();
    switch(state) {
    case WFT_STATE_SYS_ERROR:
        printf("sys error: %s\n", strerror(error)); break;
    case WFT_STATE_DNS_ERROR:
        printf("dns error: %s\n", gai_strerror(error)); break;
    case WFT_STATE_SUCCESS:
        break;
    }
    if(state != WFT_STATE_SUCCESS) {
        printf("task error!\n");
        return;
    }
    
    //1. 打印请求信息
    auto req = redistask->get_req();
    string command;
    vector<string> params;
    req->get_command(command);
    req->get_params(params);
    cout << "request: " << command << " ";
    for(auto & param : params) {
        cout << param << " ";
    }
    cout << endl << endl;
    
    //2. 打印响应信息
    using namespace protocol;
    auto resp = redistask->get_resp();
    RedisValue result;
    resp->get_result(result);

    if(result.is_string()) {
        cout << "response is string\n";
        cout << result.string_value() << endl;
    } else if(result.is_int()) {
        cout << "response is int\n";
        cout << result.int_value() << endl;
        //4. 创建Redis任务
        string url("redis://127.0.0.1:6379");
        auto redisTask2 = WFTaskFactory::create_redis_task(url, 1, redisCallback);
        //5. 设置任务的属性
        string command2("HGETALL");
        std::vector<string> params2{"liubei"};
        redisTask2->get_req()->set_request(command2, params2);
        //6. 交给框架来调度执行
        redisTask2->start();
    } else if(result.is_array()) {
        cout << "response is array\n";
        for(size_t i = 0; i < result.arr_size(); ++i) { 
            cout << (i + 1) << ") " 
                 << result.arr_at(i).string_value()  << endl;
        }
    }
}


void test0()
{
    signal(SIGINT, sighandler);

    //1. 创建Redis任务
    string url("redis://127.0.0.1:6379");
    auto redisTask1 = WFTaskFactory::create_redis_task(url, 1, redisCallback);
    //2. 设置任务的属性
    string command("HSET");
    std::vector<string> params{"liubei", "age", "40", "sex", "male"};
    redisTask1->get_req()->set_request(command, params);
    //3. 交给框架来调度执行
    redisTask1->start();
    waitGroup.wait();

}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

