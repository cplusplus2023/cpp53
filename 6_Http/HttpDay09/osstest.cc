#include <alibabacloud/oss/OssClient.h>
#include <fstream>
using namespace AlibabaCloud::OSS;

int main(void)
{
    /* 初始化OSS账号信息。*/
            
    std::string Endpoint = "oss-cn-hangzhou.aliyuncs.com";
    /* 填写Bucket名称，例如examplebucket。*/
    std::string BucketName = "bucket-lwh-test";
    /* 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。*/
    std::string ObjectName = "oss/1.txt";

    std::string accessKeyid = "LTAI5t6CJLdHYEP2vAmRa7PT";
    std::string accessKeysecret = "BXvRPQBYm8w7DGuJuzjRibKUpIR4eT";

    /* 初始化网络等资源。*/
    InitializeSdk();

    ClientConfiguration conf;
    /* 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。*/
    //OssClient client(Endpoint, credentialsProvider, conf);
    OssClient client(Endpoint, accessKeyid, accessKeysecret, conf);
    /* 填写本地文件完整路径，例如D:\\localpath\\examplefile.txt，其中localpath为本地文件examplefile.txt所在本地路径。*/
    std::shared_ptr<std::iostream> content = std::make_shared<std::fstream>("1.txt", std::ios::in | std::ios::binary);
    PutObjectRequest request(BucketName, ObjectName, content);

    auto outcome = client.PutObject(request);

    if (!outcome.isSuccess()) {
        /* 异常处理。*/
        std::cout << "PutObject fail" <<
        ",code:" << outcome.error().Code() <<
        ",message:" << outcome.error().Message() <<
        ",requestId:" << outcome.error().RequestId() << std::endl;
        return -1;
    }

    /* 释放网络等资源。*/
    ShutdownSdk();
        return 0;
}
