#ifndef __WD_OssUploader_HPP__ 
#define __WD_OssUploader_HPP__ 


#include <string>
#include <fstream>
using std::string;
using std::iostream;
using std::fstream;

#include  <alibabacloud/oss/OssClient.h>

struct OssInfo
{
    string Endpoint = "oss-cn-hangzhou.aliyuncs.com";
    string BucketName = "bucket-lwh-test";
    string AccesskeyId = "LTAI5t6CJLdHYEP2vAmRa7PT";
    string AccesskeySecret = "BXvRPQBYm8w7DGuJuzjRibKUpIR4eT";

};

class OssUploader
{
public:
    OssUploader() 
    : _ossInfo()
    , _conf()
    , _ossClient(_ossInfo.Endpoint, _ossInfo.AccesskeyId, _ossInfo.AccesskeySecret, _conf)
    {
        AlibabaCloud::OSS::InitializeSdk();
    }

    bool doUploadFile(const string & filename, const string & ObjectName)
    {
        std::shared_ptr<iostream> content = std::make_shared<fstream>(filename, std::ios::in | std::ios::binary);
        AlibabaCloud::OSS::PutObjectRequest request(_ossInfo.BucketName, ObjectName, content);
        auto outcome = _ossClient.PutObject(request);
        
        if (!outcome.isSuccess()) {
            std::cout << "PutObject fail" <<
            ",code:" << outcome.error().Code() <<
            ",message:" << outcome.error().Message() <<
            ",requestId:" << outcome.error().RequestId() << std::endl;
            return false;
        }
        return true;
    }

    ~OssUploader() 
    {
        AlibabaCloud::OSS::ShutdownSdk();
    }

private:
    OssInfo _ossInfo;
    AlibabaCloud::OSS::ClientConfiguration _conf;
    AlibabaCloud::OSS::OssClient _ossClient;

};

#endif

