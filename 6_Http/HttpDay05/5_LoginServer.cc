#include <signal.h>

#include <iostream>
using std::cout;
using std::endl;
using std::string;

#include <workflow/WFFacilities.h>
#include <workflow/WFHttpServer.h>
#include <workflow/HttpUtil.h>
#include <workflow/HttpMessage.h>
#include <workflow/RedisMessage.h>


//通过构造函数初始化计数器的值 
static WFFacilities::WaitGroup waitGroup(1);

struct SeriesContext
{
    protocol::HttpResponse * resp;
    string password;
};

void sighandler(int num)
{
    printf("sig %d is coming\n", num);
    //将计数器的值减1
    waitGroup.done();
}

void httpCallback(WFHttpTask * )
{
    printf("httpCallback is running.\n");
}

void redisCallback(WFRedisTask * redistask)
{
    printf("redisCallback is running.\n");
    //0. 对任务的状态进行检测
    int state = redistask->get_state();
    printf("state: %d\n", state);
    int error = redistask->get_error();
    if(state != WFT_STATE_SUCCESS) {
        printf("error ocurrs: %s\n", WFGlobal::get_error_string(state, error));
        return;
    }
    protocol::RedisValue result;
    redistask->get_resp()->get_result(result);
    if(result.is_string()) {
        string password = result.string_value();
        cout << "redis resp password:" << password << endl;
        //获取共享数据，进行验证
        SeriesContext * data = (SeriesContext*)series_of(redistask)->get_context();
        if(password == data->password) {
            //登录成功
            data->resp->append_output_body("Login SUCESS");
        } else {
            //登录失败
            data->resp->append_output_body("Login Failed");
        }
    }
}

void process(WFHttpTask * serverTask)
{
    printf("process is running.\n");
    //0. 设置任务的回调函数
    serverTask->set_callback(httpCallback);
    //1. 对任务的状态进行检测
    int state = serverTask->get_state();
    printf("state: %d\n", state);
    int error = serverTask->get_error();
    if(state != WFT_STATE_TOREPLY) {
        printf("error ocurrs: %s\n", WFGlobal::get_error_string(state, error));
        return;
    }
    //2. 解析请求
    auto req = serverTask->get_req();
    string uri = req->get_request_uri();
    //  /login?username=liubei&password=123
    cout << "uri: "  << uri << endl;  
    //  /login?username=liubei
    string usernameKV = uri.substr(0, uri.find("&"));
    //  password=123
    string passwordKV = uri.substr(uri.find("&") + 1);
    string username = usernameKV.substr(usernameKV.find("=") + 1);
    string password = passwordKV.substr(passwordKV.find("=") + 1);
    cout << "username: " << username << endl;
    cout << "password: " << password << endl;
    //2.1 设置共享数据
    SeriesContext * data = new SeriesContext;
    data->resp = serverTask->get_resp();
    data->password = password;
    series_of(serverTask)->set_context(data);

    //3. 进行登录的验证
    string redisURL("redis://127.0.0.1:6379");
    auto redisTask = WFTaskFactory::create_redis_task(redisURL, 1, redisCallback);
    //3.1 设置任务的属性
    string command("GET");
    std::vector<string> params{username};
    redisTask->get_req()->set_request(command, params);
    //3.2 将Redis任务添加到序列之中
    series_of(serverTask)->push_back(redisTask);

}

void test0()
{
    signal(SIGINT, sighandler);

    WFHttpServer httpServer(process);
    
    if(httpServer.start(8888) == 0) {
        waitGroup.wait();
        httpServer.stop();
    }

    waitGroup.wait();
}


int main()
{
    test0();
    printf("exit main\n");
    return 0;
}

