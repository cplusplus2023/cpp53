// List.h
// 定义结点类型
typedef struct node {
	char val;
	struct node* next;
} Node;

// 存放整条链表的信息
typedef struct {
	Node* head;
	Node* tail;
	int size;
} List;

// API
List* create_list();
void destroy_list(List* list);

void add_before_head(List* list, char val);
void add_behind_tail(List* list, char val);
void add_node(List* list, int idx, char val);

void delete_node(List* list, char val);

Node* find_by_index(List* list, int idx);
Node* search_for_value(List* list, char val);
