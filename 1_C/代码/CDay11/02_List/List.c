#include "List.h"
#include <stdlib.h>
#include <stdio.h>

/* 创建一条空链表 */
List* create_list(void) {
	return calloc(1, sizeof(List));
}

void destroy_list(List* list) {
	// 释放所有结点
	Node* curr = list->head;
	while (curr) {
		Node* next = curr->next;
		free(curr);
		curr = next;
	}
	// 释放List结构体
	free(list);
}

/* 头插法 */
void add_before_head(List* list, char val) {
	// 创建结点
	Node* new_node = malloc(sizeof(Node));
	if (!new_node) {
		printf("Error: malloc failed in add_before_head.\n");
		exit(1);
	}
	// 初始化结点, 并且链接结点
	new_node->val = val;
	new_node->next = list->head;
	// 更改整条链表的信息
	list->head = new_node;
	if (!list->tail) {
		list->tail = new_node;
	}
	list->size++;
}

/*  尾插法  */
void add_behind_tail(List* list, char val) {
	// 创建结点
	Node* new_node = malloc(sizeof(Node));
	if (!new_node) {
		printf("Error: malloc failed in add_before_head.\n");
		exit(1);
	}
	// 初始化结点
	new_node->val = val;
	new_node->next = NULL;
	// 链接结点，并更改整条链表的信息
	if (list->size == 0) {
		list->head = new_node;
		list->tail = new_node;
		list->size++;
	} else {
		// 链接
		list->tail->next = new_node;
		list->tail = new_node;
		list->size++;
	}
}

void add_node(List* list, int idx, char val) {
	// idx: [0, list->size]
	// 参数校验
	if (idx < 0 || idx > list->size) {
		printf("Illegal Argument: idx = %d, size = %d\n", idx, list->size);
		return;
	}
	// 头插法
	if (idx == 0) {
		add_before_head(list, val);
		return;
	}
	// 尾插法
	if (idx == list->size) {
		add_behind_tail(list, val);
		return;
	}
	// 在中间插入
	// 创建结点
	Node* new_node = malloc(sizeof(Node));
	if (!new_node) {
		printf("Error: malloc failed in add_before_head.\n");
		exit(1);
	}
	// 初始化结点
	new_node->val = val;
	// 查找索引为 idx-1 的结点
	// 循环不变式：curr指向的结点索引为i
	Node* curr = list->head;
	for (int i = 0; i < idx - 1; i++) {
		curr = curr->next;
	} // i == idx-1
	// 循环不变式：curr指向的结点索引为i
	// 链接结点
	new_node->next = curr->next;
	curr->next = new_node;

	list->size++;
}

/* 在list中删除第一个与 val 相等的结点 */
void delete_node(List* list, char val) {
	Node* prev = NULL;
	Node* curr = list->head;
	// 遍历链表，查找要删除的结点
	while (curr) {
		if (curr->val == val) {
			// 删除curr指向的结点
			if (!prev) {
				// 删除第一个结点
				list->head = curr->next;
				if (!list->head) {
					list->tail = NULL;
				}
				list->size--;
				free(curr);
			} else {
				prev->next = curr->next;
				if (curr->next == NULL) {
					list->tail = prev;
				}
				list->size--;
				free(curr);
			}
			
			return;
		}
		prev = curr;
		curr = curr->next;
	} // curr == NULL
}

Node* find_by_index(List* list, int idx) {
	// 参数校验：[0, list->size - 1]
	if (idx < 0 || idx >= list->size) {
		printf("Illegal Argument: idx = %d, size = %d\n", idx, list->size);
		return NULL;
	}
	// 找到索引为idx的结点
	Node* curr = list->head;
	for (int i = 0; i < idx; i++) {
		curr = curr->next;
	} // i == idx;

	return curr;
}

/* 在list中查找第一个与val值相等的结点 */
Node* search_for_value(List* list, char val) {
	Node* curr = list->head;
	// 遍历链表
	while (curr) {
		if (curr->val == val) {
			return curr;
		}
		curr = curr->next;
	} // curr == NULL

	return NULL;
}