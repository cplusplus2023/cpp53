#include "List.h"

int main(void) {
	List* list = create_list();

	/*add_before_head(list, 'A');
	add_before_head(list, 'B');
	add_before_head(list, 'C');
	add_before_head(list, 'D');	*/	// D --> C --> B --> A

	/*add_behind_tail(list, 'A');
	add_behind_tail(list, 'B');
	add_behind_tail(list, 'C');
	add_behind_tail(list, 'D');	*/// A --> B --> C --> D

	add_node(list, 0, 'A');
	add_node(list, 0, 'B');
	add_node(list, 2, 'C');
	add_node(list, 1, 'D');		// B --> D --> A --> C


	//Node* node1 = find_by_index(list, 2);
	//Node* node2 = find_by_index(list, 4);

	//Node* node1 = search_for_value(list, 'B');
	//Node* node2 = search_for_value(list, 'X');

	delete_node(list, 'A');
	delete_node(list, 'X');

	return 0;
}