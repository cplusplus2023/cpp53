/*
函数指针 (了解)
    要求：只要会使用函数指针

声明：void (*p)(void)
函数指针：
    foo, &foo
和函数相关的符号：
    函数的声明：void foo(void); 
    函数的定义 (包含了声明)：
        void foo(void) {
            for (int i = 0; i < 3; i++) {
                printf("I love xixi.\n");
        }
    函数的调用：
        foo();
    函数指针：
        foo, &foo

作用：
    1. 编写非常通用的函数 (功能非常强大)
    2. 解耦合

编程范式：
    面向过程
    面向对象：结构体
    函数式编程：函数指针
}
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

//void foo(void) {
//    for (int i = 0; i < 3; i++) {
//        printf("I love xixi.\n");
//    }
//}

typedef struct {
    int id;
    char name[25];
    int chinese;
    int math;
    int english;
} Student;

int cmp(const void* p1, const void* p2) {
    Student* s1 = p1;
    Student* s2 = p2;

    int total1 = s1->chinese + s1->math + s1->chinese;
    int total2 = s2->chinese + s2->math + s2->chinese;

    if (total1 != total2) {
        return total2 - total1;
    }
    if (s1->chinese != s2->chinese) {
        return s2->chinese - s1->chinese;
    }
    if (s1->math != s2->math) {
        return s2->math - s1->math;
    }
    if (s1->english != s2->english) {
        return s2->english - s1->english;
    }

    return strcmp(s1->name, s2->name);
}

int main(void) {
    //void (*p1) (void) = foo;
    //void (*p2) (void) = &foo;

    //p1();   // 通过函数指针，调用它指向的函数
    //printf("--------------------------\n");
    //p2();

    Student stus[5];

    for (int i = 0; i < 5; i++) {
        scanf("%d%s%d%d%d",
            &stus[i].id,
            stus[i].name,
            &stus[i].chinese,
            &stus[i].math,
            &stus[i].english);
    }

    qsort(stus, 5, sizeof(Student), cmp);

    for (int i = 0; i < 5; i++) {
        printf("%d %s %d %d %d\n",
            stus[i].id,
            stus[i].name,
            stus[i].chinese,
            stus[i].math,
            stus[i].english);
    }

    return 0;
}