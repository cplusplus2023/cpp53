#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE 1024
/*
打开/关闭文件
	File* fopen(filename, mode);
		filename: 文件的路径
			绝对路径：D:\Class\C\a.txt
			相对路径：a.txt, ./a.txt
		mode: 打开模式
			r: 
				1. 文件不存在 --> 报错
				2. 文件存在
			w: 
				1. 文件不存在 --> 创建
				2. 文件存在   --> 清空原文件的内容，再写入
			a: 
				1. 文件不存在 --> 创建
				2. 文件存在   --> 在原文件的末尾追加
*/
int main(int argc, char* argv[]) {
	// argv[0]: 可执行程序的路径
	//for (int i = 0; i < argc; i++) {
	//	puts(argv[i]);
	//}

	
	// 打开文件流
	FILE* src = fopen(argv[1], "rb");	// argv[1]: "a.txt"
	if (!src) {
		fprintf(stderr, "Open %s failed.\n", argv[1]);
		exit(1);
	}

	FILE* dst = fopen(argv[2], "wb");
	if (!dst) {
		fclose(src);
		fprintf(stderr, "Open %s failed.\n", argv[2]);
		exit(1);
	}

/*********************************************************************/
/*                       读写文本文件                                 */
/*********************************************************************/
	// 1. fgetc / fputc    (getchar / putchar)
	//char c1 = fgetc(src); 
	//char c2 = fgetc(src); 
	//char c3 = fgetc(src); 

	//fputc(c1, dst);
	//fputc(c2, dst);
	//fputc(c3, dst);

	// 惯用法
	//int c;
	//while ((c = fgetc(src)) != EOF) {
	//	fputc(c, dst);
	//}

	// 2. fgets / fputs (gets / puts)
	//char line[MAX_LINE];
	//fgets(line, MAX_LINE, src);
	//fputs(line, dst);
	//fgets(line, MAX_LINE, src);
	//fputs(line, dst);
	//fgets(line, MAX_LINE, src);
	//fputs(line, dst);

	// 惯用法
	//char line[MAX_LINE];
	//while (fgets(line, MAX_LINE, src) != NULL) {
	//	fputs(line, dst);
	//}

/*********************************************************************/
/*                       读写二进制文件                               */
/*********************************************************************/
	// fwrite / fread： 以块为单位进行读和写

	// 惯用法
	char buffer[4096];  // 4k, buffer的就是块的大小
	int n;
	while ((n = fread(buffer, 1, 4096, src)) > 0) {	// 实际读取了n个元素
		fwrite(buffer, 1, n, dst);
	}

	// 关闭文件流
	fclose(src);
	fclose(dst);
	return 0;
}