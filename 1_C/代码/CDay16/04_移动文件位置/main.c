#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

char* readFile(const char* path) {
	FILE* fp = fopen(path, "rb");
	if (!fp) {
		fprintf(stderr, "Open %s failed.\n", path);
		exit(1);
	}

	// 确定文件的大小
	fseek(fp, 0, SEEK_END);
	long size = ftell(fp);

	// 申请内存空间
	char* content = malloc(size + 1); // 1 for '\0'
	if (!content) {
		fprintf(stderr, "malloc failed in readFile\n");
		fclose(fp);
		exit(1);
	}

	// 把文件里面的内容读取到content指向的数组中
	rewind(fp);
	int n = fread(content, 1, size + 1, fp); // 实际读取的字符数目
	content[n] = '\0';

	fclose(fp);
	return content;
}

int main(int argc, char* argv[]) {
	// 命令行程序
	// 参数个数校验
	if (argc != 2) {
		fprintf(stderr, "Arguments Error\n");
		exit(1);
	}

	char* content = readFile(argv[1]);
	// 处理content

	free(content);
	return 0;
}