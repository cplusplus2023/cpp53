#include <stdio.h>

#define SIZE(a) (sizeof(a)/sizeof(a[0]))

/*
二分查找的前提：
	a. 能够随机访问 (数组)
	b. 有序        (每一次比较，可以丢掉另一半区间)
*/

////int b_search(int arr[], int left, int right, int key) {
////	// [left, right] 查找与key值相等的元素
////	// 递归
////	// 边界条件
////	if (left > right)   return -1;
////	// 递归公式
////	int mid = left + (right - left >> 1);
////	int cmp = key - arr[mid];
////	if (cmp < 0) {
////		return b_search(arr, left, mid - 1, key);
////	}
////	if (cmp > 0) {
////		return b_search(arr, mid + 1, right, key);
////	}
////	return mid;
////}
////
////int binary_search1(int arr[], int n, int key) {
////	return b_search(arr, 0, n - 1, key);   //[0, n-1]


/* 循环方式 */
int binary_search2(int arr[], int n, int key) {
	int left = 0, right = n - 1;
	while (left <= right) {
		// 求中间元素的索引
		int mid = left + (right - left >> 1);
		int cmp = key - arr[mid];

		if (cmp < 0) {
			right = mid - 1;
		} else if (cmp > 0) {
			left = mid + 1;
		} else {
			return mid;
		}
	} // left > right
	return -1;
}

/*  查找第一个与key值相等的元素 */
int binary_search3(int arr[], int n, int key) {
	int left = 0, right = n - 1;
	// [left, right]
	while (left <= right) {
		// 求中间元素的索引
		int mid = left + (right - left >> 1);
		int cmp = key - arr[mid];

		if (cmp < 0) {
			right = mid - 1;
		} else if (cmp > 0) {
			left = mid + 1;
		} else {
			// 如果是第一个与key值相等的元素
			if (mid == left || arr[mid - 1] < key) {
				return mid;
			}
			right = mid - 1;
		}
	} // left > right
	return -1;
}

/*  查找第一个大于等于key值的元素 */
int binary_search4(int arr[], int n, int key) {
	int left = 0, right = n - 1;
	// [left, right]
	while (left <= right) {
		int mid = left + (right - left >> 1);

		if (arr[mid] >= key) {
			// 判断是否是第一个
			if (mid == left || arr[mid - 1] < key) {
				return mid;
			}
			right = mid - 1;
		} else {
			left = mid + 1;
		}
	} // left > right
	return -1;
}

int main(void) {
	// 
	// int arr[] = { 0, 1, 2, 3, 3, 3, 3,3,3,3,3,3,3,3,3,3,3,3, 4, 5, 6, 7, 8, 9 };
	// int idx1 = binary_search3(arr, SIZE(arr), 3);	// 3
	// int idx2 = binary_search2(arr, 10, 30);	// -1

	int arr[] = { 0, 1, 2, 3, 4, 5, 6, 7,8,9, 10, 50, 50, 50, 50, 100 };
	int idx1 = binary_search4(arr, SIZE(arr), 50);		// 11
	int idx2 = binary_search4(arr, SIZE(arr), 70);		// 15
	int idx3 = binary_search4(arr, SIZE(arr), 200);    // -1

	return 0;
}