#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

/*
序列化：
	把程序里面的数据，持久化到数据库或者网络上
反序列化：
	把持久化到数据库或者网络上的数据，恢复成程序里面的数据

文本方式 / 二进制方式
*/

typedef struct {
	int id;
	char name[25];
	char gender;
	int chinese;
	int math;
	int english;
} Student;

int main(void) {
/***************************************************************************/
/*                   文本方式的序列化和发序列化                              */
/***************************************************************************/
	// 1. fprintf / fscanf
	// Student s1 = { 1, "xixi", 'f', 100, 100, 100 };
	//FILE* fp = fopen("students.txt", "w");
	//if (!fp) {
	//	fprintf(stderr, "Open students.txt failed.\n");
	//	exit(1);
	//}

	//// {1, xixi, f, 100, 100, 100}
	//fprintf(fp, "%d %s %c %d %d %d\n",
	//	s1.id,
	//	s1.name,
	//	s1.gender,
	//	s1.chinese,
	//	s1.math,
	//	s1.english);

	//fclose(fp);

	// 反序列化
	//FILE* fp = fopen("students.txt", "r");
	//if (!fp) {
	//	fprintf(stderr, "Open students.txt failed.\n");
	//	exit(1);
	//}

	//Student s1;
	//// {1, xixi, f, 100, 100, 100}
	//fscanf(fp, "%d%s %c%d%d%d",
	//	&s1.id,
	//	s1.name,
	//	&s1.gender,
	//	&s1.chinese,
	//	&s1.math,
	//	&s1.english);

	//fclose(fp);

/***************************************************************************/
/*                   二进制方式的序列化和发序列化                             */
/***************************************************************************/
	// 2. fread / fwrite
	//Student s1 = { 1, "liuyifei", 'f', 100, 100, 100 };
	//FILE* fp = fopen("students.dat", "wb");
	//if (!fp) {
	//	fprintf(stderr, "Open students.dat failed.\n");
	//	exit(1);
	//}

	//fwrite(&s1, sizeof(s1), 1, fp);

	//fclose(fp);

	// 反序列化
	FILE* fp = fopen("students.dat", "rb");
	if (!fp) {
		fprintf(stderr, "Open students.dat failed.\n");
		exit(1);
	}

	Student s1;
	fread(&s1, sizeof(s1), 1, fp);

	fclose(fp);

	return 0;
}