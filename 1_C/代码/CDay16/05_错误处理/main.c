#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <errno.h>
#include <string.h>
/*
发现错误
	1. 返回值
	2. errno
		0表示没有错误, 非0表示发生了某种类型的错误
报告错误：
	perror(msg)
处理错误：
	在函数的末尾，进行错误处理
	goto
*/

int main(void) {
	printf("%d\n", errno);
	FILE* fp = fopen("not_exist.txt", "r");
	printf("%d\n", errno);
	puts(strerror(errno));

	// msg: strerror(errno)
	perror("not_exist.txt");
	return 0;
}