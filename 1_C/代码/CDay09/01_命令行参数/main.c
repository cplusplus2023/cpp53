#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

/*
命令行参数：
	
	命令行参数：操作系统   -->    main
	状态码：    main      -->    操作系统

VS设置：属性 --> 调试 --> 命令参数

作用：
	1. 写一些通用程序。
	2. 可以改变程序的默认行为。
*/

int main(int argc, char* argv[]) {
	// argc: argument count,  argv字符串数组的长度
	// argv: argument vector, 这是一个字符串数组
	// 注意事项：argv的第一个元素永远是可执行程序的路径。

	// 参数转换
	int n1;
	sscanf(argv[1], "%d", &n1);
	printf("%d\n", n1);

	//for (int i = 0; i < argc; i++) {
	//	puts(argv[i]);
	//}

	return 0;
}
