#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

/*
结构体：
	1. 内存布局
	2. 字面值 (无)
	3. 支持的操作
		. （访问成员运算符）
		= (赋值)
	4. 读写

面向过程：
	结构体(类似C++中类), 只有属性，没有行为

面向对象：
	属性(静态特征)
	行为(动态特征)

例子：学生结构体
	属性：id, name, gender, chinese, math, english ...

填充的目的是为了对齐， 对齐的目的是为了更快的访问数据项。

*/

// 定义结构体类型
typedef struct student_s {	// student_s 是结构体的标签
	int id;
	char name[25];
	char gender;	// 'm' for male, 'f' for female
	int chinese;
	int math;
	int english;
} Student;


//typedef struct {	// student_s 是结构体的标签
//	int id;
//	char name[25];
//	char gender;	// 'm' for male, 'f' for female
//	int chinese;
//	int math;
//	int english;
//} Student;

//typedef struct {	
//	int id;
//	char name[25];
//	char gender;	// 'm' for male, 'f' for female
//	int chinese;
//	int math;
//	int english;
//} Student, *pStudent;

// 注意事项：不要给结构体指针类型定义别名。


void print_stu_info(Student* s) {
	//printf("{%d, %s, %c, %d, %d, %d}\n",
	//	(*s).id,
	//	(*s).name,
	//	(*s).gender,
	//	(*s).chinese,
	//	(*s).math,
	//	(*s).english);

	printf("{%d, %s, %c, %d, %d, %d}\n",
		s->id,	// s->id 等价于 (*s).id
		s->name,
		s->gender,
		s->chinese,
		s->math,
		s->english);
}

int main(void) {
	// 定义变量和初始化
	Student s1 = {1, "peanut", 'm'};
	Student s2 = {2, "liuyifei", 'f', 100, 100, 100};
	
	// s1 = s2;
	// print_stu_info(&s1);
	// print_stu_info(&s2);

	// 读结构体数据
	Student s3;
	scanf("%d%s %c%d%d%d",
		&s3.id,
		s3.name,
		&s3.gender,
		&s3.chinese,
		&s3.math,
		&s3.english);

	print_stu_info(&s3);

	pStudent s1, s2;

	return 0;
}