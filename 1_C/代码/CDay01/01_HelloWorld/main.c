// #include <stdio.h>

// stdio.h
// int printf(const char* fmt, ...);

// #define N 5;

#define FOO(x) (1 + (x) * (x))

/*
宏函数
	注意事项：
		1. 左括号应该紧贴宏函数的名称
		2. 参数应该用括号括起来
		3. 如果是表达式，整个表达式也应该用括号括起来；
	原理：文本替换

	为什么需要宏函数？
		1. 避免函数调用的开销
		2. 提供一定的宏编程能力

	应用场景：
		经常被调用的，比较小巧玲珑的函数
*/

int main(void) {
	// printf("FOO(3) = %d\n", FOO(3));
	// printf("FOO(3) = %d\n", FOO(1 + 2));
	printf("100 / FOO(3) = %d\n", 100 / FOO(3));
	return 0;
}