/*
变量：在程序的执行过程中可以发生改变的量
	声明：int i = 10;
	作用：给变量名绑定一个值
	三要素：
		变量名：通过变量名引用值
		类型:   值的类型 （a.确定值的范围；b. 确定值能够进行的操作）
		值

	为什么定义变量的时候都需要指定类型？
		1. 确定变量所占内存空间的大小
		2. 编码   
		3. 值能够进行的操作
*/

#include <stdio.h>

int main(void) {
	int i = 10;
	printf("i = %d\n", i);
	return 0;
}