#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define SIZE(a) (sizeof(a)/sizeof(a[0]))
/*
数组是一种数据类型：
	1. 内存布局
	2. 能够进行的操作
		[]: 取下标
	3. 数组没有字面值
	4. 数组的读和写
		和for循环配合使用

问题：
1. 为什么数组的元素必须是同一种数据类型？
	随机访问元素
	arr[i];
	i_addr = base_addr + i * sizeof(elem)  寻址公式

2. 为什么在大多数语言中，数组的索引是从0开始的?
	i_addr = base_addr + i * sizeof(elem)  寻址公式 （索引从0开始）
	i_addr = base_addr + (i - 1) * sizeof(elem)  寻址公式 （索引从1开始）

*/

int main(void) {
	// 数组的声明
	// int arr[10];

	//const char* p1;	// pointer to const : 常量指针
	//char* const p2;	// constant pointer : 指针常量

	// 数组的初始化
	// int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };		// {...} 数组的初始化式
	// int arr[10] = { 1, 2, 3, 4, 5, };	
	// int arr[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 10};	// 编译不通过
	// int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0,};	

	// 取下标运算
	//int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, };
	//printf("arr[3] = %d\n", arr[3]); // 读
	//arr[3] = 30;					 // 写


	// 读和写
	//int arr[10];
	//for (int i = 0; i < SIZE(arr); i++) {
	//	scanf("%d", &arr[i]);
	//}

	//printf("--------------------------------------------\n");

	//for (int i = 0; i < SIZE(arr); i++) {
	//	printf("%d ", arr[i]);
	//}
	//printf("\n");

/**********************************************************************************/
/*                              课堂小练习                                         */
/**********************************************************************************/
	double init_balance;
	printf("Enter initial balance: ");
	scanf("%lf", &init_balance);

	int rate;
	printf("Enter interest rate: ");
	scanf("%d", &rate);

	int years;
	printf("Enter number of years: ");
	scanf("%d", &years);

	double balance[5];

	printf("\nYears   ");
	for (int i = 0; i < 5; i++) {
		printf("%4d%%  ", rate + i);
		balance[i] = init_balance;
	}
	printf("\n");

	for (int i = 1; i <= years; i++) {
		printf("%3d     ", i);
		for (int i = 0; i < 5; i++) {
			balance[i] += balance[i] * ((rate + i) / 100.0);
			printf("%7.2lf", balance[i]);
		}
		printf("\n");
	}

	return 0;
}