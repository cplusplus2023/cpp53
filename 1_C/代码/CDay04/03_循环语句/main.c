#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

/*

while: 在循环体的前面设置退出点
do...while: 在循环体的后面设置退出点
for: 在循环体的前面设置退出点

*/

int main(void) {
	int n;
	printf("Enter an integer: ");
	scanf("%d", &n);

	int digits = 0;

	do {
		n /= 10;
		digits++;
	} while (n);

	printf("The number has %d digit(s).\n", digits);

	return 0;
}
