#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

/*
break: 
	1. 跳出switch语句
	2. 跳出循环语句
continue:
    跳转到循环体的末尾
goto:
    应用场景
    1. 跳出外层嵌套
    2. 进行错误处理

*/

int main(void) {

    //int n;
    //while (1) {
    //    printf("Enter a number (enter 0 to stop): ");
    //    scanf("%d", &n);
    //    if (n == 0)
    //        break;
    //    printf("%d squared is %d\n", n, n * n);
    //}


    //int count = 0, i;
    //int sum = 0;
    //while (count < 10) {
    //    scanf("%d", &i);
    //    if (i == 0)
    //        continue;
    //    sum += i;
    //    count++;
    //    /* continue jumps to here */
    //}

    //printf("sum = %d\n", sum);

    //int sum = 0, i;
    //for (int count = 0; count < 10; count++) {
    //    scanf("%d", &i);
    //    if (i == 0)
    //        continue;
    //    sum += i;

    //}

    //printf("sum = %d\n", sum);

/***************************************************************************/
/*                                 课堂小练习                               */
/***************************************************************************/
    printf("*** checkbok-balancing program ***\n");
    printf("Commands: 0=clear, 1=credit, 2=debit, 3=balance, 4=exit\n\n");

    double balance = 0.0, amount;
    int cmd;
    for (;;) {
        printf("Enter command: ");
        scanf("%d", &cmd);
        switch (cmd) {
        case 0: 
            balance = 0.0; 
            break;
        case 1: 
            printf("Enter amount of credit: ");
            scanf("%lf", &amount);
            balance += amount;
            break;
        case 2:
            printf("Enter amount of dedit: ");
            scanf("%lf", &amount);
            balance -= amount;
            break;
        case 3:
            printf("Current balance: $%.2lf\n", balance);
            break;
        case 4:
            return 0;
        default:
            printf("Commands: 0=clear, 1=credit, 2=debit, 3=balance, 4=exit\n\n");
            break;
        }
    }

	return 0;
}
