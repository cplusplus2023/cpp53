#include <stdio.h>

int main(void) {
	int nums[] = { 1,2,1,3,2,5 };

	int xor = 0;
	for (int i = 0; i < 6; i++) {
		xor ^= nums[i];
	}
	// xor = a ^ b; （xor肯定不等于0, 至少有一位为1, a和b在那一位上的值不一样)
	int lsb = xor &(-xor);

	int a = 0, b = 0;
	for (int i = 0; i < 6; i++) {
		if (nums[i] & lsb) {
			a ^= nums[i];
		} else {
			b ^= nums[i];
		}
	}

	printf("a = %d, b = %d\n", a, b);

	return 0;
}