#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

/*
语句分类：
	表达式语句
	选择语句：if, switch
	循环语句：while, do...while, for
	跳转语句：break, continue, goto, return
	复合语句：{...}
	空语句：  ;
*/
int main(void) {
	// switch 语句的特性：
    // 1. case后的标签必须是常量表达式
    // 2. 多个case标签可以共用一组语句
    // 3. case穿透现象

    //int grade;
    //scanf("%d", &grade);

    //switch (grade) {
    //case 4:
    //    printf("Excellent\n");
    //    break;
    //case 3:
    //    printf("Good\n");
    //    break;
    //case 2:
    //    printf("Average\n");
    //    /* case 穿透 */
    //case 1:
    //    printf("Poor\n");
    //    break;
    //case 0:
    //    printf("Failing\n");
    //    break;
    //default:
    //    printf("Illegal grade\n");
    //    break;
    //}

    //switch (grade) {
    //case 4: case 3: case 2: case 1:
    //    printf("Passed\n");
    //    break;
    //case 0:
    //    printf("Failing\n");
    //    break;
    //default:
    //    printf("Illegal grade\n");
    //    break;
    //}

/********************************************************************************/
/*                                课堂小练习                                     */
/********************************************************************************/

    int grade;
    printf("Enter numerical grade: ");
    scanf("%d", &grade);

    if (grade > 100 || grade < 0) {
        printf("Illegal grade\n");
    } else {
        grade /= 10;
        switch (grade) {
        case 10: case 9:
            printf("Letter grade: A\n");
            break;
        case 8:
            printf("Letter grade: B\n");
            break;
        case 7:
            printf("Letter grade: C\n");
            break;
        case 6:
            printf("Letter grade: D\n");
            break;
        default:
            printf("Letter grade: F\n");
            break;
        }
    }
   
    return 0;
}