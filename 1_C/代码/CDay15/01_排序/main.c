#include <stdio.h>

#define SIZE(a) (sizeof(a)/sizeof(a[0]))
#define SWAP(arr, i, j) {	\
	int tmp = arr[i];		\
	arr[i] = arr[j];		\
	arr[j] = tmp;			\
}			

void print_array(int arr[], int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void shell_sort(int arr[], int n);
void merge_sort(int arr[], int n);
void quick_sort(int arr[], int n);
void heap_sort(int arr[], int n);

int main(void) {
	int arr[] = { 9, 5, 2, 7, 1, 3, 4, 6, 8, 0 };

	print_array(arr, SIZE(arr));

	// shell_sort(arr, SIZE(arr));
	// merge_sort(arr, SIZE(arr));
	// quick_sort(arr, SIZE(arr));
	heap_sort(arr, SIZE(arr));
	return 0;
}

void shell_sort(int arr[], int n) {
	int gap = n / 2; 
	while (gap) {
		// 组间插入排序
		for (int i = gap; i < n; i++) {
			int value = arr[i]; // 待插入的元素
			int j = i - gap;
			while (j >= 0 && arr[j] > value) {
				arr[j + gap] = arr[j];
				j -= gap;
			} // j < 0 || arr[j] <= value
			arr[j + gap] = value;
		}
		print_array(arr, n);
		// 缩小gap
		gap >>= 1;
	} // gap == 0
}

/**********************************************************************/
/*                         归并排序                                    */
/**********************************************************************/

int tmp[10];

void merge(int arr[], int left, int mid, int right) {
	int i = left, j = left, k = mid + 1;
	// 两个区间都有元素
	while (j <= mid && k <= right) {
		if (arr[j] <= arr[k]) {		// Caution: 不能写成 arr[j] < arr[k], 就不稳定了
			tmp[i++] = arr[j++];
		} else {
			tmp[i++] = arr[k++];
		}
	} // j > mid || k > right
	// 左边区间有元素
	while (j <= mid) {
		tmp[i++] = arr[j++];
	}
	// 右边区间有元素
	while (k <= right) {
		tmp[i++] = arr[k++];
	}
	// 将tmp数组中的元素，复制到原数组的对应区间
	for (int i = left; i <= right; i++) {
		arr[i] = tmp[i];
	}
}

void m_sort(int arr[], int left, int right) {
	// [left, right] 归并排序
	// 边界条件
	if (left >= right) return;
	// 递归公式
	int mid = left + (right - left >> 1);
	m_sort(arr, left, mid);
	m_sort(arr, mid + 1, right);
	merge(arr, left, mid, right);

	print_array(arr, 10);
}

void merge_sort(int arr[], int n) {
	// 委托
	m_sort(arr, 0, n - 1);  // [0, n-1]
}

/**********************************************************************/
/*                         快速排序                                    */
/**********************************************************************/

int partition(int arr[], int left, int right) {
	// 选取基准值
	int pivot = arr[left];
	// 双向分区
	int i = left, j = right;
	while (i < j) {
		// 移动j, 找第一个比pivot小的元素
		while (i < j && arr[j] >= pivot) {
			j--;
		} // i == j || arr[j] < pivot
		arr[i] = arr[j];

		// 移动i，找第一个比pivot大的元素
		while (i < j && arr[i] <= pivot) {
			i++;
		} // i == j || arr[i] > pivot
		arr[j] = arr[i];

	} // i == j
	arr[i] = pivot;
	return i;
}

void q_sort(int arr[], int left, int right) {
	// [left, right]
	// 边界条件
	if (left >= right) return;
	// 递归公式
	// 对[left, right]区间分区, idx是分区后基准值所在的位置
	int idx = partition(arr, left, right);

	print_array(arr, 10);

	q_sort(arr, left, idx - 1);
	q_sort(arr, idx + 1, right);
}

void quick_sort(int arr[], int n) {
	q_sort(arr, 0, n - 1);	// [0, n-1]
}

/**********************************************************************/
/*                         堆排序                                      */
/**********************************************************************/

// i: 可能违反大顶堆规则的结点，并且它的左右子树都是大顶堆
// n: 逻辑上堆的长度
void heapify(int arr[], int i, int n) {
	while (i < n) {
		int lchild = 2 * i + 1;
		int rchild = 2 * i + 2;
		// 求i, lchild, rchild的最大值
		int maxIdx = i;
		if (lchild < n && arr[lchild] > arr[maxIdx]) {
			maxIdx = lchild;
		}
		if (rchild < n && arr[rchild] > arr[maxIdx]) {
			maxIdx = rchild;
		}

		if (maxIdx == i) break;

		SWAP(arr, i, maxIdx);
		i = maxIdx;
	}
}

void build_heap(int arr[], int n) {
	// 从后往前构建, 找第一个非叶子结点
	// lchild(i) = 2i+1 <= n-1
	// i <= (n-2)/2
	for (int i = n - 2 >> 1 ; i >= 0; i--) {
		heapify(arr, i, n);
	}
}

void heap_sort(int arr[], int n) {
	build_heap(arr, n);

	print_array(arr, n);

	int len = n;	// 无序区的长度
	while (len > 1) {
		// 交换堆顶元素和无序区的最后一个元素
		SWAP(arr, 0, len - 1);
		len--;
		heapify(arr, 0, len); // 0: 可能违反堆规则的结点，这个结点的左右子树都是大顶堆
		                      // len: 逻辑上堆的大小

		print_array(arr, n);
	} // len == 1
}