#include <stdio.h>

int main(void) {
	//char c = 'A';
	//short s = 10;
	//int i = 100;
	//long l = 1000;
	//long long ll = 100000;

	//float f = 3.14f;
	//double d = 6.78;

	//int i = -10;
	//unsigned int u = 10;

	//// 发生了隐式类型转换， int --> unsigned int
	//if (i < u)
	//	printf("i is less than u\n");
	//else if (i > u)
	//	printf("i is bigger than u\n");
	//else
	//	printf("is equals with u\n");

	//char c = 10000;		/* WRONG */
	//int i = 1.0e20;		/* WRONG */
	//float f = 1.0e100;	/* WRONG */

	//double fraction = 6.78;
	//double fraction_part = fraction - (int)fraction;


	//float f = 3.14f;
	//// ...
	//int i = (int)f;	// 注意：这里发生了类型转换，可能导致问题
	
	//float quotient;
	//int dividend = 4, divisor = 3;

	///* What's the difference between next two expressions? */
	//quotient = dividend / divisor;
	//quotient = (float)dividend / divisor;

	//long long millisPerDay = 24 * 60 * 60 * 1000;
	//long long nanosPerDay = (long long)24 * 60 * 60 * 1000 * 1000 * 1000;	

	//printf("%lld\n", nanosPerDay / millisPerDay);

	int i = 10;
	long long j = 20LL;

	return 0;
}