#define  _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#define SCALA_FACTOR (5.0f / 9.0f)
#define FREEZING_POINT 32

// ���뼴ע��
int main(void) {
	float fahrenheit;
	printf("Enter Fahrenheit temperature: ");
	scanf("%f", &fahrenheit);

	float celsius = SCALA_FACTOR * (fahrenheit - FREEZING_POINT);
	printf("Celsius equivalent: %.1f\n", celsius);

	return 0;
}