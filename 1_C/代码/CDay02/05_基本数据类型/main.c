/*
类型：值的划分
	a. 确定值的取值范围 (编码, 内存大小)
	b. 确定值能够进行的操作

学习哪些内容：
	1. 编码
	2. 能够进行的操作
	3. 字面值如何书写
	4. 值的读写

基本数据类型：
	整数，浮点数，字符

整数：
	1. 无符号整数：二进制编码
	   有符号整数：补码
	2. ...
	3. (十进制，八进制，十六进制)， U, L, LL 
	4. (d,u,o,x)   h(d,u,o,x)  l(d,u,o,x)  ll(d,u,o,x)

浮点数：
	1. IEEE-754标准
	2. 浮点数不能够进行取余运算，也不支持位运算
	3. 要么包含字母E(e)，要么包含小数点。 F(f), L(l)
	4. %f, %lf, %Lf

字符类型：
	1. ASCII
	2. 整数能支持的运算，字符都支持。<ctype.h>头文件中定义了字符函数，字符函数可以扩展字符的运算。
	3. 'A', 转义序列(字符转义序列，数字转义序列)
	4. scanf + %d, printf + %d;   getchar(), putchar()
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main(void) {
	// int --> unsign int --> long --> unsigned long --> long long --> unsigned long long --> 报错
	// 42ULL;

	// printf("%d\n", 12345 + 5432L);

	//unsigned int n;

	//scanf("%x", &n);		

	//printf("%u\n", n);			
	//printf("%o\n", n);		
	//printf("%x\n", n);

	// 570.e-1F;

	//printf("%c\n", 'A');	// A: 65
	//printf("%c\n", '\a');
	//printf("%c\n", '\101');
	//printf("%c\n", '\x41');

	//char ch;
	//int i;

	//i = 'a';				/* i is now 97 */
	//ch = 65;				/* ch is now 'A' */
	//ch = ch + 1;			/* ch is now 'B' */
	//ch++;					/* ch is now 'C' */

	//char c1, c2, c3;
	//scanf("%c%c%c", &c1, &c2, &c3);

	//char c;
	//c = getchar();	// read a character from stdin.
	//putchar(c);		// write a character to stdout.


	/*
	1. 语法特性
	2. 惯用法
	3. 编程范式，设计模式...
	*/

	printf("Enter a message: ");
	int count = 0;
	while (getchar() != '\n') {
		count++;
	} 
	printf("Your message was %d character(s) long.\n", count);

	return 0;
}