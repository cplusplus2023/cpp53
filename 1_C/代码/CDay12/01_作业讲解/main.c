#include <stdio.h>
#include <stdbool.h>

typedef struct node {
    int val;
    struct node* next;
} Node;

int middleElement(Node* list) {
    Node* slow = list;
    Node* fast = list;

    while (fast != NULL && fast->next != NULL) {
        slow = slow->next;
        fast = fast->next->next;
    }   // fast == NULL || fast->next == NULL

    return slow->val;
}

bool hasCircle(Node* list) {
    Node* fast = list;
    Node* slow = list;
    do {
        // 如果 fast 到达了链表的末尾
        if (fast == NULL || fast->next == NULL) {
            return false;
        }
        fast = fast->next->next;
        slow = slow->next;
    } while (fast != slow);
    // 循环结束: fast == slow, 说明链表有环
    return true;
}

// 递归方式
Node* reverse(Node* list) {
    // 边界条件
    if (list == NULL || list->next == NULL) {
        return list;
    }
    // 递归公式
    Node* result = reverse(list->next);
    list->next->next = list;
    list->next = NULL;

    return result;
}

int main(void) {
    Node node4 = { 4, NULL };
    Node node3 = { 3, &node4 };
    Node node2 = { 2, &node3 };
    Node node1 = { 1, &node2 };  // 1 --> 2 --> 3 -->4

    // node4.next = &node1;
    // node4.next = &node2;
    // node4.next = &node4;
    
    // printf("middle element: %d\n", middleElement(&node1));

    // printf("%s\n", hasCircle(&node1) ? "YES" : "NO");
    Node* head = reverse(&node1);

    return 0;
}