#include "Stack.h"
#include <stdio.h>

int main(void) {
	Node* stack = NULL;  // ��ջ

	stack_push(&stack, 1);
	stack_push(&stack, 2);
	stack_push(&stack, 3);
	stack_push(&stack, 4);

	// ����ջ
	while (!stack_empty(stack)) {
		int val = stack_peek(stack);
		printf("%d ", val);
		stack_pop(&stack);
	}
	printf("\n");
	// 4 3 2 1
	return 0;
}