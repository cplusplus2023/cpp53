#include "Stack.h"
#include <stdio.h>
#include <stdlib.h>

void stack_push(Node** ptop, int val) {
	// 创建结点
	Node* new_node = malloc(sizeof(Node));
	if (!new_node) {
		printf("Error: malloc failed in stack_push.\n");
		exit(1);
	}
	// 初始化结点并且链接
	new_node->data = val;
	new_node->next = *ptop;
	*ptop = new_node;
}

int stack_pop(Node** ptop) {
	if (stack_empty(*ptop)) {
		printf("Error: Stack EMPTY!\n");
		exit(1);
	}
	// 删除第一个结点
	Node* top = *ptop;
	int ret = top->data;
	*ptop = top->next;
	free(top);
	return ret;
}

int stack_peek(const Node* top) {
	if (stack_empty(top)) {
		printf("Error: Stack EMPTY!\n");
		exit(1);
	}
	return top->data;
}

bool stack_empty(const Node* top) {
	return top == NULL;
}