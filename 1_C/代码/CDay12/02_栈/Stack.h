#include <stdbool.h>

typedef struct node_s {
	int data;
	struct node_s* next;
} Node;

void stack_push(Node** top, int val);

int stack_pop(Node** top);

int stack_peek(const Node* top);

bool stack_empty(const Node* top);
