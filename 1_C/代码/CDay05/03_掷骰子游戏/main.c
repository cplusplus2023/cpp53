#include <stdio.h>
#include <stdbool.h>

int roll_dice(void);
bool play_game(void);

int main(void) {
	int wins = 0, losses = 0;
	char again;

	do {
		play_game() ? wins++ : losses++;
		printf("\nPlay again? ");
		// 读取用户的输入...
	} while (again == 'y' || again == 'Y');

	printf("Wins: %d, Losses: %d\n", wins, losses);

	return 0;
}