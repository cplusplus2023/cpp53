/*
局部变量： 定义在函数里面的变量
	作用域： 块作用域(从定义变量开始，到块的末尾)
	存储期限：
		一般是自动存储期限，不过可以通过static关键字改为静态存储期限。

外部变量(全局变量)：定义在所有函数外面的变量
	作用域： 文件作用域 (从定义变量开始，到文件的末尾)
	存储期限：静态存储期限
*/

#include <stdio.h>

//void foo(void) {
//	static int i = 1;	// 静态局部变量
//	printf("&i: %p, i: %d\n", &i, i++);
//	foo();
//}

long long next_fib(void) {
	static long long a = 0;
	static long long b = 1;

	long long tmp = a + b;
	a = b;
	b = tmp;

	return a;
}

int main(void) {
	// fibnacci: 0, 1, 1, 2, 3, 5, 8, 13, 21, ...
	printf("%lld\n", next_fib());	// 1
	printf("%lld\n", next_fib());	// 1
	printf("%lld\n", next_fib());	// 2
	printf("%lld\n", next_fib());	// 3
	printf("%lld\n", next_fib());	// 5
	printf("%lld\n", next_fib());	// 8

	return 0;
}

// int g_value = 1;
//
//void foo(void) {
//	printf("g_value = %d\n", g_value);
//}
//
//void bar(void) {
//	printf("g_value = %d\n", g_value);
//}