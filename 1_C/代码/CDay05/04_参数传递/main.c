#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

#define SIZE(a) (sizeof(a) / sizeof(a[0]))
/*
参数传递：
	1. 值传递 (复制)
	2. 传递数组时，数组会退化成指向它第一个元素的指针。
		好处：
			a. 避免大量复制数据
			b. 修改形参的值可以影响实参
			c. 函数调用会更加灵活
		坏处：
			a. 丢失了数组的长度信息
*/

int sum_array(int arr[]) {
	int sum = 0;
	for (int i = 0; i < SIZE(arr); i++) {
		sum += arr[i];
	}
	return sum;
}

void clear_array(int arr[], int n) {
	for (int i = 0; i < n; i++) {
		arr[i] = 0;
	}
}

int main(void) {
	int arr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	//int sum = sum_array(arr);
	//printf("sum = %d\n", sum);

	clear_array(arr, 5);

	return 0;
}