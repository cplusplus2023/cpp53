/*
函数：
宗旨：
	函数的设计：函数的功能应该越单一越好。
	函数的实现：函数的实现应该越高效越好。

易混淆的概念：
	函数的声明: double average(double a, double b);
	函数的定义：
		 double average(double a, double b) {
  			 return (a + b) / 2;
		 }
    函数的调用：average(3, 4); 
	函数指针: average;
*/
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdbool.h>

bool isPrime(int n) {
	for (int i = 2; i * i < n; i++) {
		if (n % i == 0) {
			return false;
		}
	}
	return true;
}

int main(void) {
	int n;
	printf("Enter a number: ");
	scanf("%d", &n);

	if (isPrime(n)) {
		printf("Prime\n");
	} else {
		printf("Not prime\n");
	}

	return 0;
}


