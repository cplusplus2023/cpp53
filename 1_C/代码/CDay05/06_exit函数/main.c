#include <stdlib.h>
#include <stdio.h>

void foo(void) {
	printf("foo begin\n");

	for (int i = 0; i < 3; i++) {
		printf("I love xixi\n");
	}
	// return ;
	exit(0);

	printf("foo end\n");
}

int main(void) {
	printf("main begin\n");
	foo();
	printf("main end\n");
	return 0;
}