#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

long long fib1(int n) {
	if (n == 0 || n == 1) return n;
	return fib1(n - 1) + fib1(n - 2);
}

long long fib2(int n) {
	if (n == 0 || n == 1) return n;
	// n >= 2;
	long long a = 0;
	long long b = 1;

	/*   循环不变式   */
	// fib(i)未求解
	for (int i = 2; i <= n; i++) {
		long long tmp = a + b;
		a = b;
		b = tmp;
	} // i == n+1

	return b;
}

void hanoi(int n, char start, char middle, char target);

int main(void) {
	// 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55
	//int n = 60;
	//printf("%lld\n", fib2(n));

/*****************************************************************************/
/*                              汉诺塔                                        */
/*****************************************************************************/
	
	int n;
	scanf("%d", &n);

	long long steps = (1 << n) - 1;
	printf("total step(s): %lld\n", steps);

	hanoi(n, 'A', 'B', 'C');

	return 0;
}

void hanoi(int n, char start, char middle, char target) {
	// 边界条件：n=1
	// 递归公式：
	//		a. 将上面的n-1个盘子，从start杆移动到middle杆上
	//		b. 将最下面的盘子, 从start杆移动到target杆上
	//      c. 将middle杆子上的n-1个盘子移动到target杆子上

	// 移动次数：S(1) = 1, S(n) = 2S(n-1) + 1
	//          S(n) + 1 = 2 (S(n-1) + 1) = ... = 2^n
	//          S(n) = 2^n - 1

	if (n == 1) {
		printf("%c --> %c\n", start, target);
		return;
	}
	// 将上面的n-1个盘子，从start杆移动到middle杆上
	hanoi(n - 1, start, target, middle);
	// 将最下面的盘子, 从start杆移动到target杆上
	printf("%c --> %c\n", start, target);
	// 将middle杆子上的n - 1个盘子移动到target杆子上
	hanoi(n - 1, middle, start, target);
}