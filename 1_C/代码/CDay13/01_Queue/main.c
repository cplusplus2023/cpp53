#include "Queue.h"
#include <stdio.h>

int main(void) {
	Queue* q = queue_create(); 

	queue_push(q, 1);
	queue_push(q, 2);
	queue_push(q, 3);
	queue_push(q, 4);

	while (!queue_empty(q)) {
		E val = queue_peek(q);
		printf("%d ", val);
		queue_pop(q);
	}
	printf("\n");

	// 1 2 3 4
	queue_destroy(q);
	return 0;
}