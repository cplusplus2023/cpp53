#include "BST.h"
#include "Queue.h"
#include <stdlib.h>
#include <stdio.h>

/* 创建一颗空树 */
BST* bst_create(void) {
	return calloc(1, sizeof(BST));
}

void bst_destroy(BST* tree);

void bst_insert(BST* tree, K key) {
	// 找到添加的位置
	TreeNode* parent = NULL;
	TreeNode* curr = tree->root;
	int cmp;
	while (curr) {
		cmp = key - curr->key;

		if (cmp < 0) {
			parent = curr;
			curr = curr->left;
		} else if (cmp > 0) {
			parent = curr;
			curr = curr->right;
		} else {
			return;
		}
	} // curr == NULL

	// 创建结点，并初始化结点
	TreeNode* new_node = calloc(1, sizeof(TreeNode));
	if (!new_node) {
		printf("Error: calloc failed in bst_insert\n");
		exit(1);
	}
	new_node->key = key;
	// 链接 (尾插法: 添加到prev的后面)
	if (parent == NULL) {
		// 添加第一个结点
		tree->root = new_node;
	} else if (cmp < 0) {
		parent->left = new_node;
	} else {
		parent->right = new_node;
	}
}

TreeNode* bst_search(BST* tree, K key) {
	TreeNode* curr = tree->root;

	while (curr) {
		int cmp = key - curr->key;

		if (cmp < 0) {
			curr = curr->left;
		} else if (cmp > 0) {
			curr = curr->right;
		} else {
			return curr;
		}
	} // curr == NULL
	return NULL;
}

void bst_delete(BST* tree, K key) {
	// 查找要删除的结点
	TreeNode* parent = NULL;
	TreeNode* curr = tree->root;
	while (curr) {
		int cmp = key - curr->key;

		if (cmp < 0) {
			parent = curr;
			curr = curr->left;
		} else if (cmp > 0) {
			parent = curr;
			curr = curr->right;
		} else {
			break;
		}
	} // curr == NULL || curr->key == key

	if (curr == NULL) return;

	// 退化度为2的情况
	if (curr->left && curr->right) {
		// 先将度为2的情况退化成度为0或者度为1的情况。
		TreeNode* minOfParent = curr;
		TreeNode* minOfRight = curr->right;

		while (minOfRight->left) {
			minOfParent = minOfRight;
			minOfRight = minOfRight->left;
		} // minOfRight->left == NULL

		// 退化
		curr->key = minOfRight->key;
		parent = minOfParent;
		curr = minOfRight;

	}

	// 删除curr结点
	// 处理度为0或者度为1的情况。
	TreeNode* child = curr->left ? curr->left : curr->right; // 度为0, child == NULL; 

	if (parent == NULL) {
		tree->root = child;
	}
	else {
		int cmp = curr->key - parent->key;
		if (cmp < 0) {
			parent->left = child;
		}
		else if (cmp > 0) {
			parent->right = child;
		}
		else {
			// 右子树的最小结点就是右子树的根结点
			parent->right = child;
		}
	}

	free(curr);
}

void bst_preorder(BST* tree);

void inorder(TreeNode* root) {
	// 边界条件
	if (root == NULL) return;
	// 递归公式
	// 遍历左子树
	inorder(root->left);
	// 遍历根结点
	printf("%d ", root->key);
	// 遍历右子树
	inorder(root->right);
}

void bst_inorder(BST* tree) {
	// 委托
	inorder(tree->root);
	printf("\n");
}

void bst_postorder(BST* tree);

void bst_levelorder(BST* tree) {
	// 将根结点入队列
	Queue* q = queue_create();
	queue_push(q, tree->root);
	// 判断队列是否为空
	while (!queue_empty(q)) {
		// 这一层所有结点的数目
		int n = q->size;
		// 遍历这一层所有的结点
		for (int i = 0; i < n; i++) {
			TreeNode* node = queue_pop(q);
			printf("%d ", node->key);
			if (node->left) {
				queue_push(q, node->left);
			}
			if (node->right) {
				queue_push(q, node->right);
			}
		}
		printf("\n");
	} // queue_empty(q)
	printf("\n");
}
