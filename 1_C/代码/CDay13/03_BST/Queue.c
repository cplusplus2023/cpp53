#include "Queue.h"
#include <stdlib.h>
#include <stdio.h>

// API
Queue* queue_create(void) {
	// 创建一个空的队列
	return calloc(1, sizeof(Queue));
}

void queue_destroy(Queue* q) {
	free(q);
}

void queue_push(Queue* q, E val) {
	if (queue_full(q)) {
		printf("Error: QUEUE FULL\n");
		exit(1);
	}
	q->elements[q->rear] = val;
	q->rear = (q->rear + 1) % N;
	q->size++;
}

E queue_pop(Queue* q) {
	if (queue_empty(q)) {
		printf("Error: EMPTY QUEUE!\n");
		exit(1);
	}
	E ret = q->elements[q->front];
	q->front = (q->front + 1) % N;
	q->size--;
	return ret;
}

E queue_peek(Queue* q) {
	if (queue_empty(q)) {
		printf("Error: EMPTY QUEUE!\n");
		exit(1);
	}
	return q->elements[q->front];
}

bool queue_empty(Queue* q) {
	return q->size == 0;
}

bool queue_full(Queue* q) {
	return q->size == N;
}
