#include "BST.h"

int main(void) {
	BST* tree = bst_create();

	bst_insert(tree, 9);
	bst_insert(tree, 42);
	bst_insert(tree, 13);
	bst_insert(tree, 5);
	bst_insert(tree, 57);
	bst_insert(tree, 3);

	// bst_insert(tree, 13);
	// bst_insert(tree, 42);
	// bst_insert(tree, 9);

	// TreeNode* node1 = bst_search(tree, 13);
	// TreeNode* node2 = bst_search(tree, 9527);

	// bst_delete(tree, 3);
	// bst_delete(tree, 5);
	// bst_delete(tree, 9);

	// bst_inorder(tree);

	bst_levelorder(tree);

	return 0;
}