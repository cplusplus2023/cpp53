#include "HashMap.h"
#include <stdio.h>

int main(void) {
	HashMap* map = hashmap_create();

	hashmap_put(map, "liuqiangdong", "zhangzetian");
	hashmap_put(map, "wenzhang", "mayili");
	hashmap_put(map, "jianailiang", "lixiaolu");
	hashmap_put(map, "wangbaoqiang", "marong");

	hashmap_put(map, "wenzhang", "");
	hashmap_put(map, "wangbaoqiang", "");

	// V val1 = hashmap_get(map, "liuqiangdong");
	// V val2 = hashmap_get(map, "peanut");

	hashmap_delete(map, "wangbaoqiang");
	hashmap_delete(map, "peanut");

	hashmap_destroy(map);
	return 0;
}