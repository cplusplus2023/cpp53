#include "HashMap.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

/* 创建一个空的哈希表 */
HashMap* hashmap_create(void) {
    HashMap* map = calloc(1, sizeof(HashMap));
    if (!map) {
        printf("Error: calloc failed in hashmap_create\n");
        exit(1);
    }
    map->hashseed = time(NULL);
    return map;
}

void hashmap_destroy(HashMap* map) {
    // 遍历哈希表
    // 释放一个一个结点
    for (int i = 0; i < N; i++) {
        Node* curr = map->table[i];
        while (curr) {
            Node* next = curr->next;
            free(curr);
            curr = next;
        } // curr == NULL
    }
    // 释放HashMap结构体
    free(map);
}

/* murmurhash2 */
static uint32_t hash(const void* key, int len, uint32_t seed) {
    const uint32_t m = 0x5bd1e995;
    const int r = 24;
    uint32_t h = seed ^ len;
    const unsigned char* data = (const unsigned char*)key;

    while (len >= 4) {
        uint32_t k = *(uint32_t*)data;

        k *= m;
        k ^= k >> r;
        k *= m;

        h *= m;
        h ^= k;

        data += 4;
        len -= 4;
    }

    switch (len)
    {
    case 3: h ^= data[2] << 16;
    case 2: h ^= data[1] << 8;
    case 1: h ^= data[0];
        h *= m;
    };

    h ^= h >> 13;
    h *= m;
    h ^= h >> 15;

    return h;
}

/*
a. 如果key不存在, 则添加键值对(key, val)
b. 如果key存在，则更新key关联的值，并把原来的值返回
*/
V hashmap_put(HashMap* map, K key, V val) {
    int idx = hash(key, strlen(key), map->hashseed) % N;
    // 遍历链表
    Node* curr = map->table[idx];
    while (curr) {
        if (strcmp(curr->key, key) == 0) {
            // 更新curr结点的值
            V oldVal = curr->val;
            curr->val = val;
            return oldVal;
        }
        curr = curr->next;
    } // curr == NULL
    // 创建结点
    Node* new_node = malloc(sizeof(Node));
    if (!new_node) {
        printf("Error: malloc failed in hashmap_put\n");
        exit(1);
    }
    // 初始化结点
    new_node->key = key;
    new_node->val = val;
    new_node->next = map->table[idx];

    map->table[idx] = new_node;
}

V hashmap_get(HashMap* map, K key) {
    int idx = hash(key, strlen(key), map->hashseed) % N;
    // 遍历链表
    Node* curr = map->table[idx];
    while (curr) {
        if (strcmp(curr->key, key) == 0) {
            return curr->val;
        }
        curr = curr->next;
    } // curr == NULL
    return NULL;
}

void hashmap_delete(HashMap* map, K key) {
    int idx = hash(key, strlen(key), map->hashseed) % N;
    // 遍历链表
    Node* prev = NULL;
    Node* curr = map->table[idx];
    while (curr) {
        if (strcmp(curr->key, key) == 0) {
            // 删除结点
            if (prev == NULL) {
                // 删除第一个结点
                map->table[idx] = curr->next;
                free(curr);
            } else {
                prev->next = curr->next;
                free(curr);
            }
            return;
        }
        prev = curr;
        curr = curr->next;
    } // curr == NULL
}