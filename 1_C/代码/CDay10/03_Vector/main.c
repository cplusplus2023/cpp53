#include <stdio.h>
#include "Vector.h"
// 单元测试
// main.c: Vector的使用者
int main(void) {
	// TDD: Test-Driven Development
	Vector* v = vector_create();

	for (int i = 1; i < 10000; i++) {
		push_back(v, i);
	}

	vector_destroy(v);

	return 0;
}