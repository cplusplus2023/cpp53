// .h文件
// 内容: 类型的定义和函数的声明，对外的公共的API
// 
// 作用：类似其它语言的接口

typedef struct {
	int* elements;
	int size;
	int capacity;
} Vector;

// 无参构造函数
Vector* vector_create();
// 析构函数
void vector_destroy(Vector* v);

void push_back(Vector* v, int val);
