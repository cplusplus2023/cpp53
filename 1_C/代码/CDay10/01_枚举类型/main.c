/*
枚举类型：表示一些离散值的。

应用场景：表示状态
    
类型：
    值的取值范围
    值能够进行的操作
*/

typedef enum { // Suit是枚举类型的标签
    // 罗列枚举值
    DIAMONDS,   // 0
    HEARTS = 10086,     // 1
    SPADES,     // 2
    CLUBS       // 3
} Suit;


int main(void) {
     Suit s1 = DIAMONDS;
     Suit s2 = HEARTS;
     Suit s3 = SPADES;
     Suit s4 = CLUBS;

    return 0;
}