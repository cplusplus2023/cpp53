#include <stdlib.h>

/*
free的注意事项：
	1. 只能释放堆内存空间。
	2. 不能够double free
	3. 悬空指针问题 (悬空指针是野指针的一种)
*/
int main(void) {
	// int* p = malloc(100 * sizeof(int));

	// free(p); // free(p)只会把p指向的堆内存空间释放掉，不会改变p的值。

	/*int i = 10;
	int* p = &i;
	free(p);*/

	int* p = malloc(100 * sizeof(int));
	free(p);
	free(p);

	return 0;
}