#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define N 10000

typedef struct {
	int id;
	char name[25];
	char gender;
	int chinese;
	int math;
	int english;
} Student;


char* my_strcat(const char* s1, const char* s2) {
	int n = strlen(s1) + strlen(s2);

	// malloc申请内存空间，标识变量生命周期的开始
	char* result = malloc(n + 1); // 1 for '\0'
	//if (result == NULL) {
	//	printf("Error: malloc failed in my_strcat.\n");
	//	return NULL;
	//}

	// !result: result 指向的对象不存在
	// result: result指向的对象存在
	if (!result) {
		printf("Error: malloc failed in my_strcat.\n");
		return NULL;
	}

	strcpy(result, s1);
	strcat(result, s2);

	return result;
}

int main(void) {
	//Student* s1 = (Student*)malloc(sizeof(Student));
	//Student* s2 = calloc(1, sizeof(Student));

	//Student* p = NULL;
	//// p->name; //(*p).name
	//printf("%s\n", p->name);

/*	char s1[] = "Hello world; ";
	char s2[] = "Hello kitty.\n";

	char* result = my_strcat(s1, s2);
	puts(s1);
	puts(s2);
	puts(result);

	free(result)*/;		// free释放申请的内存空间，标识变量生命周期的结束


/***********************************************************/
/*                      realloc                            */
/***********************************************************/
	int* ptr = malloc(100 * sizeof(int));
	int new_size = 200 * sizeof(int);

	// ptr = realloc(ptr, new_size);	// 可能发生什么问题? 申请内存不成功，ptr = NULL;
	
	// 惯用法 
	int* p = realloc(ptr, new_size);
	if (!p) {
		// handle error
	}
	ptr = p;	// 可能会移动内存块，ptr指向新的内存块

	return 0;
}