#include <stdlib.h>
#include <stdio.h>

// one-pass 
// 结点
typedef struct node_s {
	int data;
	struct node_s* next;
} Node;

void addNode(Node* list, int data);

int main(void) {
	Node* head = NULL;
	
	/*head = addNode(head, 1);
	head = addNode(head, 2);
	head = addNode(head, 3);
	head = addNode(head, 4);*/	// 4 --> 3 --> 2 --> 1

	addNode(&head, 1);
	addNode(&head, 2);
	addNode(&head, 3);
	addNode(&head, 4);	// 4 --> 3 --> 2 --> 1

	return 0;
}

/*  头插法 */
//Node* addNode(Node* list, int val) {
//	// 创建结点
//	Node* new_node = malloc(sizeof(Node));
//	if (!new_node) {
//		printf("Error: malloc failed in addNode.\n");
//		exit(1);
//	}
//	// 初始化结点并且链接结点
//	new_node->data = val;
//	new_node->next = list;
//
//	return new_node;
//}


/*  头插法 */
void addNode(Node** plist, int val) {
	// 创建结点
	Node* new_node = malloc(sizeof(Node));
	if (!new_node) {
		printf("Error: malloc failed in addNode.\n");
		exit(1);
	}
	// 初始化结点并且链接结点
	new_node->data = val;
	new_node->next = *plist;
	// 更新list的值
	*plist = new_node;
}