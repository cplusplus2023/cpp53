#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define LEN 100
#define SIZE(a) (sizeof(a)/sizeof(a[0]))

char* my_strcpy(char* s1, const char* s2);
void separate(char* str);

int main(void) {
	 char s1[LEN + 1];
	 char s2[LEN + 1];
	 my_strcpy(s1, "I love xixi.");
	 my_strcpy(s2, strcpy(s1, "Hello Kitty."));
	// strcpy(s1, "Hello Kitty");
	// strcpy(s2, s1);

	// 注意事项：strcpy不会检查数组是否越界
	// char str[5];
	// strcpy(str, "Hello world");

	// strncpy
	//char str[5];
	//strncpy(str, "Hello world", 4);
	//str[4] = '\0';

/**************************************************************/
/*                      strcat                                */
/**************************************************************/
	 // strcat: string + concatenate
	 //char str1[LEN + 1] = "hello ";
	 //char str2[LEN + 1] = "world.";

	 //strcat(str1, str2);
	 //puts(str1);
	 //puts(str2);

	 // 注意事项：不会检查数组是否越界
	 //char str1[8] = "Hello ";
	 //strcat(str1, "world.");

	 // strncat(str1, str2, n);		n: 最多能够复制的字符数目
	 //char str1[8] = "Hello ";
	 //int n = SIZE(str1) - strlen(str1) - 1;
	 //strncat(str1, "world.", n);

/**************************************************************/
/*                      strcmp                                */
/**************************************************************/
	 //int cmp;
	 //cmp = strcmp("ABC", "abc");		// 负数
	 //cmp = strcmp("abc", "abcd");		// 负数
	 //cmp = strcmp("abc", "abd");		// 负数


/**************************************************************/
/*                      课堂小练习                             */
/**************************************************************/
	 char str[LEN + 1] = "AaBbCcDd";
	 separate(str);
	 puts(str);

	return 0;
}

//void separate(char* str) {
//	char lower[LEN + 1];
//	char upper[LEN + 1];
//
//	int i = 0, j = 0;
//	char* p = str;
//	// 遍历字符串
//	while (*p) {
//		if (islower(*p)) {
//			lower[i++] = *p;
//		} else {
//			upper[j++] = *p;
//		}
//		p++;
//	}
//
//	lower[i] = '\0';
//	upper[j] = '\0';
//
//	strcpy(str, lower);
//	strcat(str, upper);
//}


void separate(char* str) {
	char* pstore = str;
	// 遍历字符串
	while (*str) {
		if (islower(*str)) {
			// 满足条件
			char c = *pstore;
			*pstore = *str;
			*str = c;
			pstore++;
		}
		str++;
	}
}

char* my_strcpy(char* s1, const char* s2) {
	//char* p = s1;
	//while (*s2 != '\0') {
	//	*s1 = *s2;
	//	s1++;
	//	s2++;
	//}
	//*s1 = '\0';

	//return p;

	char* p = s1;

	// 惯用法：复制字符串
	while (*s1++ = *s2++)
		;

	return p;
}

/*
n: 表示最大能够复制的字符数目
*/
char* strncpy(char* s1, const char* s2, int n) {
	char* p = s1;
	
	while (n-- && (*s1++ = *s2++))
		;
	// n == 0 或者 *s2 == '\0'
	return p;
}