#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define SIZE(a) (sizeof(a)/sizeof(a[0]))

// 比较容易混淆的符号：
// int* p;		声明：定义指针变量
// p * q;		二元运算符：乘法
// *p;			单目运算符：解引用操作

// int& ref;	在C语言中不存在
// a & b;		二元运算符：按位与
// &i;			单目运算符：取地址

// 注意事项：指针类型不是整数类型!

void swap(int*, int*);
void max_min(int arr[], int n, int* pmax, int* pmin);
int* foo(void);

int main(void) {
	// int* p;	// 未初始化的指针
	// int* q = 0xCAFEBABE;	// 指向未知区域的指针

/*           指针变量的赋值                       */
	//int i = 3;
	//int* p = &i;		// Ok

	//int* q = p;			// Ok
	//int* q1 = NULL;		// Ok
	//int* q2 = 0xABCD;	// Error: 野指针
	//int j = q;			// Error: 不要混淆指针类型和整数类型。


// 例子
	//int i = 3, j = 4;
	//int* p = &i;
	//int* q = &j;

	//*p = *q;

	//printf("*p = %d, *q = %d\n", *p, *q);	// 4， 4
	//printf("i = %d, j = %d\n", i, j);		// 4， 4

/***************************************************************************/
/*                       指针作为参数                                       */
/***************************************************************************/
	//int a = 3, b = 4;
	//printf("a = %d, b = %d\n", a, b);
	//swap(&a, &b);
	//printf("a = %d, b = %d\n", a, b);


/***************************************************************************/
/*                       课堂小练习                                         */
/***************************************************************************/

	//int arr[] = { 9, 5, 2, 7, 1, 3, 4, 6, 8, 0 };
	//int max, min;
	//max_min(arr, SIZE(arr), &max, &min);

	//printf("max = %d, min = %d\n", max, min);
	//return 0;

/***************************************************************************/
/*                       指针作为返回值                                     */
/***************************************************************************/
	// 注意事项：不要返回指向当前栈帧区域的指针
	int* p = foo();
	printf("*p = %d\n", *p);
	printf("*p = %d\n", *p);

	return 0;
}

int* foo(void) {
	int i = 3;
	return &i;
}



void max_min(int arr[], int n,  int* pmax,  int* pmin) {
	*pmax = arr[0];
	*pmin = arr[0];
	for (int i = 1; i < n; i++) {
		if (arr[i] > *pmax) {
			*pmax = arr[i];
		} else if (arr[i] < *pmin) {
			*pmin = arr[i];
		}
	}
}

void swap(int* pa, int* pb) {
	int tmp = *pa;
	*pa = *pb;
	*pb = tmp;
}