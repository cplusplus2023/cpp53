#include <string.h>
#include <stdio.h>

int main(void) {
	// 1. strlen: string + length
	printf("strlen(\"abc\") = %d\n", my_strlen("abc"));
	printf("strlen(\"\") = %d\n", my_strlen(""));

	return 0;
}

size_t my_strlen(const char* s) {
	/*size_t len = 0;
	while (*s) {
		len++;
		s++;
	}  
	return len;*/

	char* p = s;
	while (*s) {
		s++;
	} 
	return s - p;
}