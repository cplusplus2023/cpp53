#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

/*
C语言中的字符串：
	总纲：C语言中的字符串依赖字符数组存在，它是一种逻辑类型。

	1. 内存布局
	2. 字面值
	3. 操作
		数组能够进行的操作, 字符串变量都可以。(数组+指针)
		字符串库

	4. 读写
		写：
			1. printf + %s
			2. puts
		读：
			1. scanf + %s
			2. gets
			3. fgets(str, n, stdin);

*/

#define LEN 80

int main(void) {
	// printf("I love xixi.\n");

	//printf("I love xixi. \
	//	--From peanut\n");		// C语言支持跨行的字符串字面值

	//printf("I love xixi."
	//	"	--From peanut\n");

	//printf(
	//	"   *\n"
	//	"  * *\n"
	//	" *   *\n"
	//	"*     *\n"
	//	" *   *\n"
	//	"  * *\n"
	//	"   *\n");
	
	/*char* p;
	p = "abc";*/

	// char c = "abc"[1];

	//char* p = "abc" + 2;
	//printf("%c\n", *p);


/*******************************************************/
/*                 字符串变量的初始化                    */
/*******************************************************/
	// char str1[10] = { 'H', 'e', 'l', 'l', 'o' }; // 太繁琐，而且可读性不强
	// char str2[10] = "Hello";    // 数组初始化式的简写形式 (语法糖)
	// char str3[5] = "Hello";     // Caution: str3不表示字符串
	// char str4[] = "Hello";     // str4表示字符串, 字符数组的长度为6
	// char str5[] = { 'H', 'e', 'l', 'l', 'o' };	// str5不表示字符串, 字符数组的长度为5

	// 注意事项：字符数组和字符指针的区别!
	// char str[] = "Hello";
	// char* p = "Hello";

	// str[0] = 'h';
	// p[0] = 'h';   /* Error  */


/*******************************************************/
/*                 读写字符串                           */
/*******************************************************/
	// 写：
	// 1. printf + %s
	// 2. puts
	// char str[] = "liuyifei";
	// printf("%s\n", str);	
	// puts(str);			// 效果等价于 printf("%s\n", str).


	// 读：
	// 1. scanf + %s
	// %s: 跳过前置的空白字符，读取字符填充数组，遇到空白字符结束
	// 弊端：
	//     a. 不能读取空白字符
	//     b. 不安全, 可能发生数组越界
	// char str[LEN + 1];
	// char str[4];
	// scanf("%s", str);	// scanf压根儿就不知道str数组的长度

	// 2. gets
	// 原理：读取一行数据，并将换行符'\n'替换成空字符'\0'
	// 弊端：
	//		a. 不安全, 可能发生数组越界
	// char str[LEN + 1];
	// char str[4];
	// gets(str);		// gets压根儿就不知道str数组的长度

	// 3. fgets
	char str[10 + 1];
	fgets(str, 11, stdin);	
	return 0;
}