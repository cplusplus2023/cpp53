#include <stdio.h>

#define SIZE(a) (sizeof(a)/sizeof(a[0]))
#define SWAP(arr, i, j) {	\
	int tmp = arr[i];		\
	arr[i] = arr[j];		\
	arr[j] = tmp;			\
}			

void insertion_sort(int arr[], int n);

void print_array(int arr[], int n) {
	for (int i = 0; i < n; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int main(void) {
	int arr[] = { 9, 5, 2, 7, 1, 3, 4, 6, 8, 0 };

	print_array(arr, SIZE(arr));

	insertion_sort(arr, SIZE(arr));

	return 0;
}

//void insertion_sort(int arr[], int n) {
//	for (int i = 1; i < n; i++) {
//		// i：下一个待插入元素的索引
//		for (int j = i - 1; j >= 0; j--) {
//			if (arr[j] > arr[j + 1]) {
//				// 逆序对，交换
//				SWAP(arr, j, j + 1);
//			} else {
//				break;
//			}
//		}
//		print_array(arr, n);
//	}
//}

void insertion_sort(int arr[], int n) {
	for (int i = 1; i < n; i++) {
		// i：下一个待插入元素的索引
		int value = arr[i];	// value：待插入的元素

		int j = i - 1;
		while (j >= 0 && arr[j] > value) {
			arr[j + 1] = arr[j];
			j--;
		} // j == -1 || arr[j] <= value
		arr[j + 1] = value;

		print_array(arr, n);
	}
}