#include <stdio.h>
#include <stdbool.h>

bool isOdd(int n) {
	return n % 2 != 0;
}

int main(void) {
	// int a = -3;
	// bool result = isOdd(a);
	// int i, j, k;

	// i = (j = (k = 0));

	//int i;
	//float f;

	//f = (i = 33.3f);	// i = 33, f = 33.0f;

	//int i = 1;
	//printf("i is %d\n", ++i);	// 2
	//printf("i is %d\n", i);		// 2

	//int i = 1;
	//printf("i is %d\n", i++);	// 1
	//printf("i is %d\n", i);		// 2

	//int i = 1;
	//int j = 2;
	//int k = ++i + j++;

	// i=2, j=3, k=4


	//unsigned short i, j;

	//i = 13;			// 0000_0000_0000_1101
	//j = i << 2;		// 0000_0000_0011_0100	52

	//j = i >> 2;		// 0000_0000_0000_0011	3

	unsigned short i, j, k;

	i = 21;		/* 0000_0000_0001_0101 */
	j = 56;     /* 0000_0000_0011_1000 */

	k = ~i;		/* 1111_1111_1110_1010*/
	k = i & j;  /* 0000_0000_0001_0000*/	// 16
	k = i | j;	/* 0000_0000_0011_1101*/	// 61
	k = i ^ j;	/* 0000_0000_0010_1101*/    // 61- 16 = 45
	return 0;
}