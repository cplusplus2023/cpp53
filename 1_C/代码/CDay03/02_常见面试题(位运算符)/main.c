#include <stdio.h>
#include <stdbool.h>

bool isOdd(int n) {
	return n & 0x1;
}

bool isPowerOf2(int n) {
	// ˼·1
	//while (n % 2 == 0) {
	//	  n >>= 1;
	//}
	//return n == 1;

	// ˼·2
	//int i = 1;
	//while (i < n) {
	//	i <<= 1;	// 1, 2, 4, 8, 16, ...
	//} // i >= n
	//return n == i;

	return (n & n-1) == 0;
}

int lastSetBit(int n) {
	/*
	x: 0101 1000
   -x: 1010 1000
       0000 1000
	*/

	return n & -n;
}

int main(void) {
	int lsb = lastSetBit(24);
	printf("lsb: %d\n", lsb);
	return 0;
}
