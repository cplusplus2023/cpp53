#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

bool play_game(void);
int roll_dice(void);

int main(void) {
	int wins = 0, losses = 0;
	char again;
	do {
		play_game() ? wins++ : losses++;
		// 询问玩家是否继续
		printf("\nPlay agiain? ");
		// 读取第一个非空白字符
		scanf(" %c", &again);
		// 跳过这一行剩余字符
		while (getchar() != '\n')
			;
	} while (again == 'Y' || again == 'y');

	printf("\nWins: %d Losses: %d\n", wins, losses);

	return 0;
}

bool play_game(void) {
	// 设置随机种子
	srand(time(NULL));

	int point = roll_dice();
	if (point == 7 || point == 11) {
		printf("You win!\n");
		return true;
	}
	if (point == 2 || point == 3 || point == 12) {
		printf("You lose!\n");
		return false;
	}

	printf("Your point is %d\n", point);

	for (;;) {
		int tally = roll_dice();
		if (tally == point) {
			printf("You win!\n");
			return true;
		}
		if (tally == 7) {
			printf("You lose!\n");
			return false;
		}
	}
}

int roll_dice() {
	int a = rand() % 6 + 1;	
	int b = rand() % 6 + 1;
	printf("You rolled: %d\n", a + b);

	return a + b;
}