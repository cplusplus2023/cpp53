#include <stdio.h>

int main(void) {
	// int *p, *q;
	// int* p, q;
	
	// int i, j, a[10], b[20], *p, *q;

	//int i = 3;
	//int* p = &i;

	//printf("*p = %d\n", *p);
	//*p = 4;
	//printf("i = %d\n", i);

	// 野指针：未初始化的指针变量，或者是指向未知区域的指针变量
	// int* p;	// 野指针
	int* q = 0xCAFEBABE;	// 野指针

	// 对野指针进行解引用操作，会发生未定义的行为。
	// printf("*p = %d\n", *p);
	*q = 100;

	return 0;
}