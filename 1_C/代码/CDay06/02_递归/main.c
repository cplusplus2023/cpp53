#include <stdio.h>

/*
时间：O(logn)
空间：O(logn) -- 栈的深度
*/
int joseph(int n) {
	// 边界条件
	if (n == 1 || n == 2) return 1;
	// 递归公式
	if (n & 0x1) {
		return 2 * joseph(n >> 1) + 1;
	}
	return 2 * joseph(n >> 1) - 1;
}

int main(void) {
	printf("joseph(12) = %d\n", joseph(12));	// 9
	printf("joseph(7) = %d\n", joseph(7));		// 7

	return 0;
}