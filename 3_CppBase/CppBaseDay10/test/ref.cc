#include <iostream>
using std::cout;
using std::endl;

int * p = new int(4);

class Test{
public:
    void change(){
        ref = 100;
    }

    void print(){
        cout << &ref << endl;
        cout << p << endl;
    }
private:
    int & ref = *p;
};

void test0(){
    Test t;
    t.change();
    cout << *p << endl;
    t.print();
}

void test1(){
    Test * tp = new Test();
    tp->print();
}

int main(void){
    test1();
    return 0;
}
