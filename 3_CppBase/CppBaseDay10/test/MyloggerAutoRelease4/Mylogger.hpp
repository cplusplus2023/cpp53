#ifndef __Mylogger_HPP__
#define __Mylogger_HPP__

#include <pthread.h>
#include <log4cpp/Category.hh>
#include <string>
using std::string;

class Mylogger
{
public:
    static Mylogger * getInstance();
	void warn(const char *msg);
	void error(const char *msg);
	void debug(const char *msg);
	void info(const char *msg);
	
private:
	Mylogger();
	~Mylogger();
    static void destroy();
    static void init_r();
    
private:
    log4cpp::Category & _mycat;
    static Mylogger * _pInstance;
    static pthread_once_t _once;
};

#define addprefix(msg) string("[").append(__FILE__)\
            .append(":").append(__func__)\
            .append(":").append(std::to_string(__LINE__))\
            .append("]").append(msg).c_str()

#define LogWarn(msg) Mylogger::getInstance()->warn(addprefix(msg))




#endif
