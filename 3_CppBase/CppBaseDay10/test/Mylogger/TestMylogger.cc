#include "Mylogger.hpp"
#include <iostream>
using std::cout;
using std::endl;
using std::string;


void test0(){
    Mylogger::getInstance()->warn("this is a warn msg");
    Mylogger::getInstance()->error("this is an error msg");
    Mylogger::getInstance()->debug("this is a debug msg");
    Mylogger::getInstance()->info("this is a info msg");
}

void test1(){
    cout << __FILE__ << endl;
    cout << __func__ << endl;
    cout << __LINE__ << endl;
}

void test2(){
    /* cout << addprefix("this is an error msg") << endl; */
    /* Mylogger::getInstance()->warn(addprefix("this is a warn msg")); */
    LogWarn("this is a warn msg");
    Mylogger::destroy();
}

int main(void){
    test2();
    return 0;
}
