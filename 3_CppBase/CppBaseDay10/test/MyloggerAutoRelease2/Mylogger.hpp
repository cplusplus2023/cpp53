#ifndef __Mylogger_HPP__
#define __Mylogger_HPP__

#include <log4cpp/Category.hh>
#include <string>
using std::string;

class Mylogger
{
public:
    static Mylogger * getInstance();
    static void destroy();
	void warn(const char *msg);
	void error(const char *msg);
	void debug(const char *msg);
	void info(const char *msg);
	
private:
    class AutoRelease;
	Mylogger();
	~Mylogger();
    
private:
    log4cpp::Category & _mycat;
    static Mylogger * _pInstance;
    static AutoRelease _ar;
};

#define addprefix(msg) string("[").append(__FILE__)\
            .append(":").append(__func__)\
            .append(":").append(std::to_string(__LINE__))\
            .append("]").append(msg).c_str()

#define LogWarn(msg) Mylogger::getInstance()->warn(addprefix(msg))




#endif
