#include <string.h>
#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

class CowString
{
public:
    CowString();

    CowString(const char * pstr);

    CowString(const CowString& rhs);

    CowString& operator=(const CowString & rhs);

    ~CowString();

    int size() const{ return strlen(_pstr); }
    const char * c_str() const{ return _pstr; }

    //获取引用计数
    int use_count(){ 
        return *(int*)(_pstr - kRefcountLength);
    }

    char & operator[](int idx);

    friend
    ostream & operator<<(ostream & os, const CowString & rhs);
private:

    char * malloc(const char * pstr = nullptr){
        if(pstr){
            return new char[strlen(pstr) + kRefcountLength + 1]() + kRefcountLength;
        }else{
            return new char[1 + kRefcountLength]() + kRefcountLength;
        }
    }

    void release(){
        decreaseRefcount();
        if(use_count() == 0){
            delete [] (_pstr - kRefcountLength);
            _pstr = nullptr;
            cout << ">> delete heap" << endl;
        }
    }

    void initRefcount(){
        *(int*)(_pstr - kRefcountLength) = 1;
    }

    void increaseRefcount(){
        ++*(int*)(_pstr - kRefcountLength);
    }

    void decreaseRefcount(){
        --*(int*)(_pstr - kRefcountLength);
    }

    static const int kRefcountLength = 4;
    char * _pstr;
};

ostream & operator<<(ostream & os, const CowString & rhs){
    os << rhs._pstr;
    return os;
}

#if 0
char & CowString::operator[](int idx){
    if(idx >= 0 && idx < size()){
        return _pstr[idx];
    }else{
        cout << "访问不合法" << endl;
        static char nullchar = '\0';
        return nullchar;
    }
}
#endif

#if 1
char & CowString::operator[](int idx){
    if(idx >= 0 && idx < size()){
        if(use_count() > 1){
            //原来的引用计数-1
            decreaseRefcount();
            //进行深拷贝
            char * ptmp = malloc(_pstr);
            strcpy(ptmp,_pstr);
            //改变指向
            _pstr = ptmp;
            //初始化引用计数
            initRefcount();
        }
        return _pstr[idx];
    }else{
        cout << "访问不合法" << endl;
        static char nullchar = '\0';
        return nullchar;
    }
}
#endif

CowString::CowString()
: _pstr(malloc())
{
    cout << "CowString()" << endl;
    initRefcount();
}

CowString::CowString(const char * pstr)
: _pstr(malloc(pstr))
{
    cout << "CowString(const char *)" << endl;
    strcpy(_pstr,pstr);
    initRefcount();
}

CowString& CowString::operator=(const CowString & rhs){
    if(this != &rhs){//考虑自复制情况
        release();//这里不一定回收左操作数申请的空间
        _pstr = rhs._pstr; //浅拷贝
        increaseRefcount(); //引用计数+1
    }
    return *this;
}

CowString::~CowString(){
    cout << "~CowString()" << endl;
    release();
}

CowString::CowString(const CowString & rhs)
: _pstr(rhs._pstr)
{
    increaseRefcount();
    cout << "CowString(const CowString &)" << endl;
}


void test0(){
    CowString str1;
    cout << str1.use_count() << endl;
    CowString str2 = str1;
    cout << "str1:" << str1 << endl;
    cout << "str2:" << str2 << endl;
    cout << str1.use_count() << endl;
    cout << str2.use_count() << endl;

    cout << endl;
    CowString str3("hello");
    CowString str4 = str3;
    cout << "str3:" << str3 << endl;
    cout << "str4:" << str4 << endl;
    cout << str3.use_count() << endl;
    cout << str4.use_count() << endl;

    cout << endl;
    str2 = str3;
    cout << "str1:" << str1 << endl;
    cout << "str2:" << str2 << endl;
    cout << "str3:" << str3 << endl;
    cout << "str4:" << str4 << endl;
    cout << str1.use_count() << endl;
    cout << str2.use_count() << endl;
    cout << str3.use_count() << endl;
    cout << str4.use_count() << endl;

    cout << endl;
    str2[0] = 'X';
    cout << "str2:" << str2 << endl;
    cout << "str3:" << str3 << endl;
    cout << "str4:" << str4 << endl;
    cout << str1.use_count() << endl;
    cout << str2.use_count() << endl;
    cout << str3.use_count() << endl;
    cout << str4.use_count() << endl;

    cout << endl;
    cout << str3[0] << endl;
    cout << str1.use_count() << endl;
    cout << str2.use_count() << endl;
    cout << str3.use_count() << endl;
    cout << str4.use_count() << endl;
}



int main(void){
    test0();
    return 0;
}
