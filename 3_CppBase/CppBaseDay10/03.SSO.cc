#include <iostream>
using std::cout;
using std::endl;
using std::string;


void test0(){
    cout << sizeof(string) << endl;
    string s1 = "helloworldaaaaa";
    string s2 = "helloworldaaaaaa";
    //打印字符串内容
    cout << "s1:" << s1 << endl;
    //打印字符串对象的首地址
    cout << "&s1:" << &s1 << endl;
    //打印字符串内容的首地址
    printf("s1's content address: %p\n",s1.c_str());

    cout << endl;
    cout << "s2:" << s2 << endl;
    cout << "&s2:" << &s2 << endl;
    printf("s2's content address: %p\n",s2.c_str());



}

int main(void){
    test0();
    return 0;
}
