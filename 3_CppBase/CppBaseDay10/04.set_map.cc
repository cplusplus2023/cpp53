#include <iostream>
#include <set>
#include <map>
#include <utility>
using std::cout;
using std::endl;
using std::set;
using std::pair;
using std::map;
using std::string;
using std::make_pair;

void test0(){
    set<int> number = {1,3,9,8,9,6,0,1,4,3,2,10,11,12,14,15,90};

    //增强for循环遍历
    for(auto & nu : number){
        cout << nu << " ";
    }
    cout << endl;

    //迭代器方式遍历容器
    /* auto it = number.begin(); */
    set<int>::iterator it = number.begin();
    while(it != number.end()){
        cout << *it << " ";
        ++it;
    }
    cout << endl;

    cout << "进行set的查找操作" << endl;
    cout << number.count(10) << endl;
    cout << number.count(9) << endl;
    auto it2 = number.find(89);
    set<int>::iterator it3 = number.find(6);
    cout << "*it2:" << *it2 << endl;
    cout << "*it3:" << *it3 << endl;

    cout << "进行set的插入操作" << endl;
    pair<set<int>::iterator,bool> ret = number.insert(8);
    if(ret.second){
        cout << "该元素插入成功:"
            << *(ret.first) << endl;
    }else{
        cout << "该元素插入失败，表明该元素已存在" << endl;
    }

    cout << endl;
    cout << "再次插入" << endl;
    int arr[5] = {18,41,35,2,99};
    //如果arr + 5改成arr + 4就无法插入最后一个元素99
    //传给insert的两个迭代器参数（左闭右开）
    number.insert(arr,arr + 5);
    for(auto & nu : number){
        cout << nu << " ";
    }
    cout << endl;

    cout << endl;
    number.insert({100,111,120});
    for(auto & nu : number){
        cout << nu << " ";
    }
    cout << endl;

    /* auto it4 = number.begin(); */
    /* cout << *it4 << endl; */
    /* *it = 300; */
}

void test1(){
    pair<int,string> num = {1,"wangdao"};
    cout << num.first << ":" << num.second << endl;
}

void checkfind(map<int,string> & rhs, int key){
    auto it = rhs.find(key);
    if(it == rhs.end()){
        cout << "该元素不在map中" << endl;
    }else{
        cout << "该元素在map中" << endl;
        cout << "*it:" << it->first << " : " << it->second << endl;
    }
}

void test2(){
    //map的初始化
    map<int,string> number = {
        {1,"hello"},
        {2,"world"},
        {3,"wangdao"},
        pair<int,string>(4,"hubei"),
        pair<int,string>(5,"wangdao"),
        make_pair(9,"shenzhen"),
        make_pair(3,"beijing"),
        make_pair(6,"shanghai")
    }; 

    cout << "迭代器方式遍历map" << endl;
    map<int,string>::iterator it = number.begin();
    while(it != number.end()){
        cout << (*it).first << " " << it->second << endl;
        ++it;
    }
    cout << endl;

    cout << endl;
    cout << "map的查找操作" << endl;
    cout << number.count(10) << endl;
    cout << number.count(9) << endl;
    /* auto it2 = number.find(1); */
    /* map<int,string>::iterator it3 = number.find(60); */
    /* cout << "*it2:" << it2->first << " : " << it2->second << endl; */
    /* cout << "*it3:" << it3->first << " : " << it3->second << endl; */
    checkfind(number,1);
    cout << endl;
    checkfind(number,60);

    cout << endl;
    cout << "map进行插入操作" << endl;

    pair<map<int,string>::iterator,bool> ret =
    number.insert(pair<int,string>(3,"nanjing"));

    if(ret.second){
        cout << "该元素插入成功" << endl;
        cout << ret.first->first << " : " << ret.first->second << endl;
    }else{
        cout << "该元素插入失败" << endl;
    }
    cout << endl;

    for(auto & nu : number){
        cout << nu.first << " " << nu.second  << endl;
    }
}

int main(void){
    test2();
    return 0;
}
