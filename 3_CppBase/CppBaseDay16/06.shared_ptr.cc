#include <iostream>
#include <memory>
#include <vector>
using std::cout;
using std::endl;
using std::vector;
using std::shared_ptr;

class Point
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    shared_ptr<int> sp(new int(10));
    /* cout << "*sp:" << *sp << endl; */
    /* cout << "sp.get(): " << sp.get() << endl; */
    cout << "sp.use_count(): " << sp.use_count() << endl;

    cout << endl;
    cout << "执行复制操作" << endl;
    shared_ptr<int> sp2 = sp;
    /* cout << "*sp:" << *sp << endl; */
    /* cout << "sp.get(): " << sp.get() << endl; */
    /* cout << "*sp2:" << *sp2 << endl; */
    /* cout << "sp2.get(): " << sp2.get() << endl; */
    cout << "sp.use_count(): " << sp.use_count() << endl;
    cout << "sp2.use_count(): " << sp2.use_count() << endl;

    cout << endl;
    cout << "再创建一个对象sp3" << endl;
    shared_ptr<int> sp3(new int(30));
    /* cout << "*sp3:" << *sp3 << endl; */
    /* cout << "sp3.get(): " << sp3.get() << endl; */
    cout << "sp.use_count(): " << sp.use_count() << endl;
    cout << "sp2.use_count(): " << sp2.use_count() << endl;
    cout << "sp3.use_count(): " << sp3.use_count() << endl;

    cout << endl;
    cout << "执行赋值操作" << endl;
    sp3 = sp;
    /* cout << "*sp:" << *sp << endl; */
    /* cout << "sp.get(): " << sp.get() << endl; */
    /* cout << "*sp2:" << *sp2 << endl; */
    /* cout << "sp2.get(): " << sp2.get() << endl; */
    /* cout << "*sp3:" << *sp3 << endl; */
    /* cout << "sp3.get(): " << sp3.get() << endl; */
    cout << "sp.use_count(): " << sp.use_count() << endl;
    cout << "sp2.use_count(): " << sp2.use_count() << endl;
    cout << "sp3.use_count(): " << sp3.use_count() << endl;





}

int main(void){
    test0();
    return 0;
}
