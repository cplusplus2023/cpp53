#include <iostream>
#include <memory>
using std::cout;
using std::endl;
using std::shared_ptr;
using std::weak_ptr;

class Child;

class Parent
{
public:
    Parent()
    { cout << "Parent()" << endl; }
    ~Parent()
    { cout << "~Parent()" << endl; }
    //只需要Child类型的指针，不需要类的完整定义
    weak_ptr<Child> wpChild;
};

class Child
{
public:
    Child()
    { cout << "child()" << endl; }
    ~Child()
    { cout << "~child()" << endl; }
    shared_ptr<Parent> spParent;
};

void test0(){
    shared_ptr<Parent> parentPtr(new Parent());
    shared_ptr<Child> childPtr(new Child());
    //1,1
    cout << "parentPtr.use_count():" << parentPtr.use_count() << endl;
    cout << "childPtr.use_count():" << childPtr.use_count() << endl;

    cout << endl; 
    parentPtr->wpChild = childPtr;
    childPtr->spParent = parentPtr;
    //2,1
    cout << "parentPtr.use_count():" << parentPtr.use_count() << endl;
    cout << "childPtr.use_count():" << childPtr.use_count() << endl;
    


}

int main(void){
    test0();
    return 0;
}
