#include <iostream>
using std::cout;
using std::endl;


int sum(int x){
    return x;
}

template <class T,class ...Args>
double sum(T x, Args ...args)
{
    return x + sum(args...);
}

void test0(){
    cout << sum(0.1,1.1,2.2,5,8.7,0.4,10) << endl;
}

int main(void){
    test0();
    return 0;
}
