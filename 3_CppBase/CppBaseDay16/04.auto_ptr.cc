#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::auto_ptr;

void test0(){
    int * pInt = new int(10);
    //创建auto_ptr对象接管资源
    auto_ptr<int> ap(pInt);
    cout << "*pInt:" << *pInt << endl;
    cout << "*ap:" << *ap << endl;

    cout << endl;
    auto_ptr<int> ap2(ap);
    cout << "*ap2:" << *ap2 << endl;
    cout << "*ap:" << *ap << endl;
}

int main(void){
    test0();
    return 0;
}
