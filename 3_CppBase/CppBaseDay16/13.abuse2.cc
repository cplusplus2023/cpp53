#include <iostream>
#include <memory>
using std::cout;
using std::endl;
using std::shared_ptr;

class Point
: public std::enable_shared_from_this<Point>
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    shared_ptr<Point> addPoint(Point * pt){
        _ix += pt->_ix;
        _iy += pt->_iy;

        //std::enable_shared_from_this<Point>();

        /* return this; */
        /* return shared_ptr<Point>(this); */
        return shared_from_this();
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    shared_ptr<Point> sp(new Point(1,2));
    cout << "sp = ";
    sp->print();

    cout << endl;
    shared_ptr<Point> sp2(new Point(3,4));
    cout << "sp2 = ";
    sp2->print();

    cout << endl;
    //创建sp3的参数实际上是sp所对应的裸指针
    //效果还是多个智能指针托管了同一块空间
    shared_ptr<Point> sp3(sp->addPoint(sp2.get()));
    cout << "sp3 = ";
    sp3->print();
}

int main(void){
    test0();
    return 0;
}
