#include <iostream>
#include <memory>
#include <vector>
using std::cout;
using std::endl;
using std::vector;
using std::unique_ptr;

class Point
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    unique_ptr<int> up(new int(10));
    cout << "*up:" << *up << endl;
    cout << "up.get(): " << up.get() << endl;

    cout << endl;
    //独享所有权的智能指针，对托管的空间独立拥有
    //拷贝构造已经被删除
    //unique_ptr<int> up2 = up;//复制操作 error

    //赋值运算符函数也被删除
    unique_ptr<int> up3(new int(20));
    //up3 = up;//赋值操作 error
    
    cout << endl;
    vector<unique_ptr<Point>> vec;
    unique_ptr<Point> up4(new Point(10,20));
    //&up4;//左值
    //将up4这个对象作为参数传给了函数，会调用拷贝构造
    //但是unique_ptr的拷贝构造已经删除了
    //所以这样写会报错
    //vec.push_back(up4);
    
    vec.push_back(std::move(up4));
    //调用构造函数创建一个匿名对象，也是右值
    //unique_ptr<Point> 理解为类名
    //括号里的new Point(1,3)这个表达式返回一个Point*
    //作为unique_ptr<Point>的构造函数的参数
    vec.push_back(unique_ptr<Point>(new Point(1,3)));





}

int main(void){
    test0();
    return 0;
}
