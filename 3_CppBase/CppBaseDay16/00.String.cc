#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class String
{
public:
	String();
	String(const char *pstr);
	String(const String &rhs);

    String(String && rhs)
    : _pstr(rhs._pstr)
    {
        cout << "String(String&&)" << endl;
        rhs._pstr = nullptr;
    }

    String & operator=(String && rhs){
        if(this != &rhs){
            delete [] _pstr;
            //浅拷贝
            _pstr = rhs._pstr;
            rhs._pstr = nullptr;
            cout << "String& operator=(String&&)" << endl;
        }
        return *this;
    }

	String &operator=(const String &rhs);
	~String();
	void print();
    size_t length() const;
    const char * c_str() const;

private:
	char * _pstr;
};

String::String()
: _pstr(new char[1]())
{
    cout << "String()" << endl;
}

//还是需要将原本的拷贝构造留下了，
//处理复制左值内容的情况
String::String(const char * pstr)
: _pstr(new char[strlen(pstr) + 1]())
{
    cout << "String(const char *) " << endl;
    strcpy(_pstr,pstr);
}

String::String(const String & rhs)
: _pstr(new char[strlen(rhs._pstr) + 1]())
{
    cout << "String(const String &)" << endl;
    strcpy(_pstr,rhs._pstr);
}

String & String::operator=(const String &rhs){
    if(this != &rhs){
        delete [] _pstr;
        _pstr = new char[strlen(rhs._pstr)]();
        strcpy(_pstr,rhs._pstr);
        cout << "String& operator=(const String&)" << endl;
    }
    return *this;
}

String::~String(){
    if(_pstr){
        delete [] _pstr;
        _pstr = nullptr;
    }
    cout << "~String()" << endl;
}

void String::print(){
    cout << _pstr << endl;
}
    
size_t String::length() const{
    cout << strlen(_pstr) << endl;
}
    
const char * String::c_str() const{
    return _pstr;
}


void test0(){
    //构造
    String s1("world");
    //拷贝构造
    String s2 = s1;
    //先构造，再拷贝构造
    //利用"hello"这个字符串创建了一个匿名对象
    //并复制给了s3
    //这一步实际上new了两次
    String s3 = "hello";
    cout << endl;
    s3 = s1;//赋值运算符函数
    cout << endl;
    s3 = String("wangdao");
}

void test1(){
    String s1("hello");
    //右值复制给左值，肯定不是同一个对象
    s1 = String("world");
    //创建了两个内容相同的临时对象，也不是同一对象
    String("wangdao") = String("wangdao");

    cout << endl;
    cout << "执行std::move函数" << endl;
    s1 = std::move(s1);
    s1.print();
}

void test2(){
    int a = 1;
    //&(std::move(a));//把左值转成右值
}

String && func(){
    return String("wuhan");
}

void test3(){
    func();
    //&func();//error，右值引用本身是右值的情况
    String && ref = func();
    &ref;//ok，右值引用本身是左值的情况
}

String func2(){
    String str1("wangdao");
    str1.print();
    return str1;
}

void test4(){
    func2();
    //&func2();//error,右值
    String && ref = func2();
    &ref;//右值引用本身是左值
}

String s10("beijing");
String func3(){
    s10.print();
    return s10;
}

int main()
{
    /* test4(); */
    func3();
	return 0;
}

