#include <iostream>
#include <memory>
using std::cout;
using std::endl;
using std::unique_ptr;
using std::shared_ptr;
using std::string;

void test0(){
    string msg = "hello,world\n";
    FILE * fp = fopen("res1.txt","a+");
    fwrite(msg.c_str(),1,msg.size(),fp);
    fclose(fp);
}

struct FILECloser{
    void operator()(FILE * fp){
        if(fp){
            fclose(fp);
            cout << "fclose(fp)" << endl;
        }
    }
};

void test1(){
    string msg = "hello,world\n";
    //在模板参数列表中加入删除器类型
    unique_ptr<FILE,FILECloser> up(fopen("res2.txt","a+"));
    //get函数可以从智能指针中获取到裸指针
    fwrite(msg.c_str(),1,msg.size(),up.get());
    //不写这一行，内容写不进去
    //写了这一行，可以写入内容，但是出现double free
    /* fclose(up.get()); */
}

void test2(){
    string msg = "hello,world\n";
    FILECloser fc;
    //在shared_ptr的构造函数参数中加入删除器对象
    shared_ptr<FILE> sp(fopen("res3.txt","a+"),fc);
    /* shared_ptr<FILE> sp(fopen("res3.txt","a+"),FILECloser()); */
    //get函数可以从智能指针中获取到裸指针
    fwrite(msg.c_str(),1,msg.size(),sp.get());
    /* fclose(sp.get()); */

}

int main(void){
    test2();
    return 0;
}
