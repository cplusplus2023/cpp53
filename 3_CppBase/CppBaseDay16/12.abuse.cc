#include <iostream>
#include <memory>
using std::cout;
using std::endl;
using std::unique_ptr;
using std::shared_ptr;

//test0~test3留下的是错误的使用方式
class Point
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    //需要人为注意避免
    Point * pt = new Point(1,2);
    unique_ptr<Point> up(pt);
    unique_ptr<Point> up2(pt);
}

void test1(){
    //这种错误比较隐晦，最终的效果也是
    //让两个unique_ptr对象托管了同一片空间
    unique_ptr<Point> up(new Point(1,2));
    unique_ptr<Point> up2(new Point(1,2));
    up.reset(up2.get());

    //这样不会有问题
    /* Point * pt = new Point(10,20); */
    /* up.reset(pt); */
}


void test2(){
    //使用不同的智能指针托管同一片堆空间
    //即使是shared_ptr也是不行的
    //shared_ptr的复制、赋值的参数都是shared_ptr的对象
    //不能直接多次把同一个裸指针传给它的构造
    Point * pt = new Point(10,20);
    shared_ptr<Point> sp(pt);
    shared_ptr<Point> sp2(pt);
    //
    //这样写是可以的
    //shared_ptr<Point> sp2(sp);
}

void test3(){
    //使用不同的智能指针托管同一片堆空间
    shared_ptr<Point> sp(new Point(1,2));
    shared_ptr<Point> sp2(new Point(1,2));
    sp.reset(sp2.get());

    //这样写是可以的
    //sp = sp2;
}

int main(void){
    test3();
    return 0;
}
