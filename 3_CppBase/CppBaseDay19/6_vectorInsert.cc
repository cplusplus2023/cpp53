#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    vector<int> number = {1, 4, 6, 8, 9, 5, 2};
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector的尾部进行插入与删除" << endl;
    number.push_back(10);
    number.push_back(50);
    display(number);
    number.pop_back();
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector的任意位置进行插入" << endl;
    vector<int>::iterator it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 100);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    //为什么vector的insert扩容不能按照2倍？
    //因为push_back每次插入元素的个数是一个（固定的），所以
    //按照两倍的扩容是没有问题的；但是对于insert而言，每次
    //插入元素的个数都不一定，所以不能按照固定的2倍进行扩容
    //
    //元素的个数size() = m = 9,容量的大小capacity() = n = 14
    //待插入的元素的个数t
    //1、t < n - m的时候，此时就不会发生扩容
    //2、n - m < t < m的时候;按照2 * m进行扩容
    //3、n - m < t, m < t < n的时候;按照t + m进行扩容
    //3、n - m < t, t > n的时候;按照t + m进行扩容
    cout << endl << endl;
    number.insert(it, 50, 200);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}
