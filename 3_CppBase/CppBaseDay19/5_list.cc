#include <iostream>
#include <list>
#include <vector>

using std::cout;
using std::endl;
using std::list;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

struct CompareList
{
    bool operator()(const int &lhs, const int &rhs) const
    {
        cout << "struct CompareList" << endl;
        return lhs < rhs;
    }
};

void test()
{
    list<int> number = {1, 3, 3, 3, 5, 9, 8, 6, 4, 3, 6};
    display(number);

    cout << endl << "list中的unique函数" << endl;
    number.unique();
    display(number);

    cout << endl << "list中的reverse函数" << endl;
    number.reverse();
    display(number);

    cout << endl << "list中的sort函数" << endl;
    /* number.sort();//按照升序排列 */
    /* std::less<int>  lt; */
    /* number.sort(std::less<int>());//按照升序排列 */
    /* number.sort(std::greater<int>());//按照降序排列 */
    number.sort(CompareList());//函数对象的形式
    display(number);

    cout << endl << "list中的unique函数" << endl;
    number.unique();
    display(number);

    cout << endl << "list中的merge函数" << endl;
    list<int> number2 = {11, 7, 10, 30};
    number2.sort();//先将number2排序
    /* number.sort(std::greater<int>()); */
    /* number2.sort(std::greater<int>()); */
    number.merge(number2);
    display(number);
    display(number2);
}

void test2()
{
    list<int> number = {1, 3, 5, 9, 8, 6, 4, 6};
    list<int> number2 = {7, 10, 30, 20, 80};

    auto it = number.begin();
    ++it;
    number.splice(it, number2);
    display(number);
    display(number2);

    cout << endl << endl;
    it = number.begin();
    ++it;
    list<int> number3 = {111, 888, 999, 444, 333, 222};
    auto it22 = number3.end();
    --it22;
    number.splice(it, number3, it22);
    display(number);
    display(number3);

    cout << endl << endl;
    it = number.begin();
    ++it;
    auto it33 = number3.begin();
    ++it33;
    ++it33;
    it22 = number3.end();
    --it22;
    number.splice(it, number3, it33, it22);
    display(number);
    display(number3);

    cout << endl << "splice在同一个链表中进行操作" << endl;
    auto it2 = number.begin();
    ++it2;
    ++it2;//444

    auto it3 = number.end();
    --it3;
    --it3;//4

    number.splice(it2, number, it3);
    display(number);
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

