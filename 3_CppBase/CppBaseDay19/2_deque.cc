#include <iostream>
#include <deque>
#include <vector>

using std::cout;
using std::endl;
using std::deque;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    deque<int> number = {1, 4, 6, 8, 9, 5, 2};
    display(number);

    cout << endl << "在deque的尾部进行插入与删除" << endl;
    number.push_back(10);
    number.push_back(50);
    display(number);
    number.pop_back();
    display(number);

    cout << endl << "在deque的头部进行插入与删除" << endl;
    number.push_front(11);
    number.push_front(55);
    display(number);
    number.pop_front();
    display(number);

    //y + n(目标行号) + G
    cout << endl << "在deque的任意位置进行插入" << endl;
    deque<int>::iterator it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 100);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl << endl;
    number.insert(it, 3, 200);
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    vector<int> vec = {111, 333, 888, 777};
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl;
    number.insert(it, {30, 50, 90, 60});
    display(number);
    cout << "*it = " << *it << endl;

    cout << endl << "清空deque中的所有元素" << endl;
    number.clear();//清空元素
    number.shrink_to_fit();//回收多余的空间
    cout << "number.size() = " << number.size() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

