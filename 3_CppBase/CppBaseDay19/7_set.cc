#include <iostream>
#include <set>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::set;
using std::vector;
using std::pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //set的基本特征
    //1、存放的是key值，key值是唯一的，不能重复
    //2、默认会按照key值进行升序排列
    //3、set的底层使用的是红黑树
    set<int>  number = {1, 3, 6, 3, 9, 8, 4, 2};
    /* set<int, std::greater<int>>  number = {1, 3, 6, 3, 9, 8, 4, 2}; */
    display(number);

    cout << endl << "set的查找操作" << endl;
    size_t cnt = number.count(4);
    cout << "cnt = " << cnt << endl;

    cout << endl;
    /* set<int>::iterator it = number.find(5); */
    set<int>::iterator it = number.find(6);
    if(it == number.end())
    {
        cout << "该元素是不存在set中的" << endl;
    }
    else
    {
        cout << "该元素存在set中 " << *it << endl;
    }

    cout << endl << "set的insert操作" << endl;
    /* pair<set<int>::iterator, bool> ret = number.insert(7); */
    auto ret = number.insert(7);
    if(ret.second)
    {
        cout << "该元素插入成功 " << *ret.first << endl;
    }
    else
    {
        cout << "该元素插入失败,因为该元素存在set中" << endl;
    }
    display(number);

    cout << endl;
    vector<int> vec = {5, 10, 60, 20, 8};
    number.insert(vec.begin(), vec.end());
    display(number);

    cout << endl;
    number.insert({100, 11, 33, 44, 7});
    display(number);

    //下标
    /* cout << endl << "set的下标" << endl; */
    /* cout << "number[1] = " << number[1] << endl;//error */

    //修改
    auto it2 = number.begin();
    cout << "*it2 = " << *it2 << endl;
    /* *it2 = 200;//error,不支持修改 */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

