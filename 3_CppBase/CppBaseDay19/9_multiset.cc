#include <iostream>
#include <set>
#include <vector>
#include <utility>

using std::cout;
using std::endl;
using std::multiset;
using std::vector;
using std::pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test()
{
    //multiset的基本特征
    //1、存放的是key值，key值是不唯一的，可以重复
    //2、默认会按照key值进行升序排列
    //3、multiset的底层使用的是红黑树
    multiset<int>  number = {1, 3, 6, 3, 9, 3, 8, 4, 2, 3};
    /* multiset<int, std::greater<int>>  number = {1, 3, 6, 3, 9, 8, 4, 2}; */
    display(number);

    cout << endl << "multiset的查找操作" << endl;
    size_t cnt = number.count(3);
    cout << "cnt = " << cnt << endl;

    cout << endl;
    /* multiset<int>::iterator it = number.find(5); */
    multiset<int>::iterator it = number.find(6);
    if(it == number.end())
    {
        cout << "该元素是不存在multiset中的" << endl;
    }
    else
    {
        cout << "该元素存在multiset中 " << *it << endl;
    }

    cout << endl << "测试xxx_bound函数" << endl;
    auto it3 = number.lower_bound(3);//不小于给定的key
    auto it4 = number.upper_bound(3);//大于给定的key
    cout << "*it3 = " << *it3 << endl;
    cout << "*it4 = " << *it4 << endl;
    while(it3 != it4)
    {
        cout << *it3 << "  ";
        ++it3;
    }
    cout << endl;

    //返回的是大于等于key以及大于key的一个范围
    auto ret = number.equal_range(3);
    while(ret.first != ret.second)
    {
        cout <<  *ret.first << "  ";
        ++ret.first;
    }
    cout << endl;

    cout << endl << "multiset的insert操作" << endl;
    number.insert(7);
    display(number);

    cout << endl;
    vector<int> vec = {5, 10, 60, 20, 8};
    number.insert(vec.begin(), vec.end());
    display(number);

    cout << endl;
    number.insert({100, 11, 33, 44, 7});
    display(number);

    //下标
    /* cout << endl << "multiset的下标" << endl; */
    /* cout << "number[1] = " << number[1] << endl;//error */

    //修改
    auto it2 = number.begin();
    cout << "*it2 = " << *it2 << endl;
    /* *it2 = 200;//error,不支持修改 */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

