#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test()
{
    vector<int>  vec = {1, 3, 5, 5, 5, 7, 9, 5, 8};
    /* auto it2 = vec.end(); */
    //需求：将vector所有等于5的元素删除掉
    //该操作不能删除连续重复的元素
    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        if(5 == *it)
        {
            vec.erase(it);
        }
    }

    //删除完毕之后，进行遍历
    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

}

void test2()
{
    vector<int>  vec = {1, 3, 5, 5, 5, 7, 9, 5, 8};
    //需求：将vector所有等于5的元素删除掉
    for(auto it = vec.begin(); it != vec.end(); )
    {
        if(5 == *it)
        {
            vec.erase(it);
        }
        else
        {
            ++it;
        }
    }

    //删除完毕之后，进行遍历
    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

}
int main(int argc, char *argv[])
{
    test2();
    return 0;
}

