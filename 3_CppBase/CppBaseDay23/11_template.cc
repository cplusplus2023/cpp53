#include <string.h>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

template <typename T>
T add(T x, T y)
{
    return x + y;
}

template <>
const char *add<const char *>(const char *x, const char *y)
{
    char *ptmp = new char[strlen(x) + strlen(y) + 1]();
    strcpy(ptmp, x);
    strcat(ptmp, y);

    return ptmp;
}

void test()
{
    int ia = 1, ib = 2;
    double da = 1.1, db = 2.2;
    string s1 = "hello", s2 = "world";
    const char *pstr1 = "hello";
    const char *pstr2 = "world";
    add(ia, ib);
    add(da, db);
    add(s1, s2);
    add(pstr1, pstr2);

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

