#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    virtual void print()
    {

    }

    void func1()
    {
        this->print();
    }

    void func2()
    {
        Base::print();
    }
};

class Derived
: public Base
{
public:
    virtual void print()
    {

    }

};

void test()
{
    Base  base;
    Derived derived;

    Base *pbase = &base;
    pbase->func1();
    pbase->func2();

    Base *pbase2 = &derived;
    pbase2->func1();
    pbase2->func2();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

