#include <string.h>
#include <iostream>

using std::cout;
using std::endl;

void *operator new(size_t sz)
{
    cout << "void *operator new(size_t)" << endl;
    /* printf("%p\n", this); */
    void *ret = malloc(sz);
    return ret;
}

void operator delete(void *pointer)
{
    cout << "void operator delete(void *)" << endl;
    free(pointer);
} 

class Student
{ 
public:
    Student(int id, const char *name)
    : _id(id)
    , _name(new char[strlen(name) + 1]())
    {
        cout << "Student()" << endl;
        strcpy(_name, name);
    } 

    ~Student()
    {
        cout << "~Student()" << endl;
        delete [] _name;
    } 

#if 0
    static void *operator new(size_t sz)
    {
        cout << "void *operator new(size_t)" << endl;
        /* printf("%p\n", this); */
        void *ret = malloc(sz);
        return ret;
    }

    static void operator delete(void *pointer)
    {
        cout << "void operator delete(void *)" << endl;
        free(pointer);
    } 
#endif
    
    void print() const
    {
        cout << "id:" << _id << endl
            << "name:" << _name << endl;
    }
private:
    int _id;
    char *_name;
};

int main(void)
{
    Student *pstu = new Student(100, "Jackie");
    pstu->print();
    delete pstu;

    return 0;
}
