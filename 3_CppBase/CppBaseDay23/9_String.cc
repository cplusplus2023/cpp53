#include <iostream>

using std::cout;
using std::endl;

class String 
{
public:
    String(){}
    String &operator=(String &&rhs)
    {
        if(this != &rhs)
        {
            delete [] _pstr;

            _pstr = rhs._pstr;
            rhs._pstr = nullptr;
        }

        return  *this;
    }

private:
    char *_pstr;
};

void test()
{

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

