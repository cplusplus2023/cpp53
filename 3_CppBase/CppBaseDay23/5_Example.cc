#include <iostream>

using std::cout;
using std::endl;

class NonCopyable
{
public:
    NonCopyable(){}
    NonCopyable(const NonCopyable &rhs) = delete;
    NonCopyable &operator=(const NonCopyable &rhs) = delete;
};

class Example
: NonCopyable
{
public:
    Example() = default;
    /* Example(const Example &rhs) = delete; */
    /* Example &operator=(const Example &rhs) = delete; */
private:
    /* Example(const Example &rhs); */
    /* Example &operator=(const Example &rhs); */
    
};

void test()
{
    Example ex1;
    Example ex2 = ex1;//error

    Example ex3;
    ex3 = ex1;//error
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

