#include <iostream>

using std::cout;
using std::endl;

class Test
{
public:
    Test(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    , _ref(_ix)
    {

    }
private:
    int _ix;
    int _iy;
    int &_ref;
    static int cnt;
};

int Test::cnt = 0;

void test()
{
    cout << "sizeof(Test) = " << sizeof(Test) << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

