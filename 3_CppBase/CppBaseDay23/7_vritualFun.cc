#include <iostream>

using std::cout;
using std::endl;

class A
{ 
public:
    virtual
    void a()
    {
        cout << "a() in A" << endl;
    } 

    virtual
    void b()
    {
        cout << "b() in A" << endl;
    } 
    
    virtual
    void c()
    {
        cout << "c() in A" << endl;
    }
};
class B
{ 
public:
    virtual
    void a()
    {
        cout << "a() in B" << endl;
    } 

    virtual
    void b()
    {
        cout << "b() in B" << endl;
    } 
    
    void c()
    {
        cout << "c() in B" << endl;
    } 
    
    void d()
    {
        cout << "d() in B" << endl;
    }
};

class C
: public A
, public B
{ 
public:
    virtual
    void a()
    {
        cout << "a() in C" << endl;
    } 
    
    //是虚函数还是非虚函数
    void c()
    {
        cout << "c() in C" << endl;
    } 
    
    void d()
    {
        cout << "d() in C" << endl;
    }
};

class D
: public C
{
public:
    void c()
    {
        cout << "c() in D" << endl;
    }
};

void test()
{
    D d;
    C *pc = &d;
    pc->c();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

