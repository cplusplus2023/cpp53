#include <iostream>

using std::cout;
using std::endl;

class Base
{
public:
    Base(int ix, int iy)
    : _ix(ix)
    , _iy(iy)
    {

    }
private:
    int _ix;
    int _iy;
};

class Dervied
: public Base
/* , public Base2 */
{
public:
    Dervied(int ix, int iy)
    /* : _ix(ix) */
    : Base(ix, iy)
    {

    }

};


int main(int argc, char *argv[])
{
    cout << "sizeof(Dervied) = " << sizeof(Dervied) << endl;
    return 0;
}

