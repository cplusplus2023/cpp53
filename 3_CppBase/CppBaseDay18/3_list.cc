#include <iostream>
#include <list>

using std::cout;
using std::endl;
using std::list;

void test()
{
    //初始化
    /* list<int> number1();//声明了函数number1//error */
    /* list<int> number;//1、无参对象 */
    /* list<int> number(10, 3);//2、count个value */
    /* int arr[10] = {1, 3, 5, 7, 9, 8, 6, 4, 2, 10}; */
    //3、迭代器范围
    /* list<int> number(arr, arr + 10);//[,),左闭右开区间 */
    //4、拷贝（移动）构造函数
    /* list<int>  number2(number); */
    /* list<int>  number3(std::move(number));//ok */
    //5、使用大括号的形式
    list<int> number = {1, 2, 3, 4, 6, 7, 8, 9, 10, 5};

    //遍历
#if 0
    //下标
    for(size_t idx = 0; idx != number.size(); ++idx)
    {
        cout << number[idx] << "  ";
    }
    cout << endl;
#endif

    //迭代器未初始化
    list<int>::iterator it;
    for(it = number.begin(); it != number.end(); ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;

    //初始化了迭代器
    list<int>::iterator it2 = number.begin();
    for(; it2 != number.end(); ++it2)
    {
        cout << *it2 << "  ";
    }
    cout << endl;

    //for循环与auto结合
    for(auto &elem : number)
    {
        cout << elem << "  ";
    }
    cout << endl;

#if 0
    for(auto it3 = number2.begin(); it3 != number2.end(); ++it3)
    {
        cout << *it3 << "  ";
    }
    cout << endl;
#endif


}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

