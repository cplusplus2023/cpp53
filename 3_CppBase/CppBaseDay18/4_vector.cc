#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

void test0()
{
    vector<int> number;
    number[0] = 10;
}

void test()
{
    cout << "sizeof(vector<int>) = " << sizeof(vector<int>) << endl;
    cout << "sizeof(vector<long>) = " << sizeof(vector<long>) << endl;

    cout << endl;
    vector<int> number = {1, 4, 6, 8, 9, 5, 2};
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << "在vector的尾部进行插入与删除" << endl;
    number.push_back(10);
    number.push_back(50);
    display(number);
    number.pop_back();
    display(number);
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    //为何vector不支持在头部进行插入与删除操作呢？
    //因为vector是一端开口的，如果在头部进行插入与删除的话，会
    //导致后面的元素都要进行移动，此时的时间复杂度是O(N)
    //
    //如何获取vector中的第一个元素的首地址?
    &number;//error，得不到第一个元素的首地址
    &number[0];//ok
    &*number.begin();//ok
    int *pdata = number.data();//ok

    cout << endl << "在vector的任意位置进行插入" << endl;
    vector<int>::iterator it = number.begin();
    ++it;
    ++it;
    cout << "*it = " << *it << endl;
    number.insert(it, 100);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl << endl;
    number.insert(it, 3, 200);
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    //vector在进行insert操作的时候，底层已经发生了扩容，导致
    //迭代器指向的是老的空间，而老的空间都已经被回收了,此时
    //迭代器已经失效了(重要的)
    cout << endl;
    vector<int> vec = {111, 333, 888, 777};
    number.insert(it, vec.begin(), vec.end());
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;

    cout << endl;
    it = number.begin();//重新置位
    ++it;
    ++it;
    number.insert(it, {30, 50, 90, 60});
    display(number);
    cout << "*it = " << *it << endl;
    cout << "number.size() = " << number.size() << endl;
    cout << "number.capacity() = " << number.capacity() << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

