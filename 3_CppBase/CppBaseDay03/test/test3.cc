#include <iostream>
using std::cout;
using std::endl;

int num = 100;//全局静态区
int num2 = 200;
const int num3 = 300;//文字常量区

void test0(){
    const int num4 = 30;
    const char * pstr = "hello";//hello 存在文字常量区
    cout << "&num:" << &num << endl;
    cout << "&num2:" << &num2 << endl;
    cout << "&num3:" << &num3 << endl;
    cout << "&num4:" << &num4 << endl;
    /* cout << pstr << endl; */
    printf("%p\n",pstr);

}

int main(void){
    test0();
    return 0;
}
