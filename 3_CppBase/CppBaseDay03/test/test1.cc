#include <iostream>
using std::cout;
using std::endl;

namespace wd
{
void print(){
    cout << "hello" << endl;
}

void print(int x){
    cout << x << endl;
}
}

using wd::print;
void test0(){
    print();
    print(4);
}

int main(void){
    test0();
    return 0;
}
