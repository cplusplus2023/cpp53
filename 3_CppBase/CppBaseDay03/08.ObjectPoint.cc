#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(){}

    Point(int x,int y)
    : _ix(x) //int _ix = x
    , _iy(y) //int _iy = y
    {
        cout << "Point(int,int)" << endl;
    }

#if 0
    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "const Point &" << endl;
    }
#endif

#if 0
    //在类中的所有成员函数中都有一个隐藏的this指针（第一个参数）
    //this指针代表的是当前对象
    //在编译之后，编译器会自动加上该指针
    //形式：Point * const this
    //不要在参数列表中显式写出this指针
    Point& operator=(const Point & rhs)
    {
        this->_ix = rhs._ix;
        this-> _iy = rhs._iy;
        cout << "Point& operator=(const Point &)" << endl;
        return *this;
    }
#endif
    void print(){
        cout << "(" << _ix
            << "," << _iy
            << ")" ;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};


class Line
{
public:
    Line(int x1, int y1, int x2, int y2)
    //: _pt1()
    //, _pt2()
    /* : _pt1(x1,y1) //Point pt1(x1,y1) */
    /* , _pt2(x2,y2) //Point pt2(x2,y2) */
    {
        cout << "Line(int * 4)" << endl;
    }

    void printLine(){
        _pt1.print();
        cout << " -----> ";
        _pt2.print();
        cout << endl;
    }
private:
    //对象成员
    Point _pt1;
    Point _pt2;
};

void test0(){
    Line ll(1,2,3,4);
    ll.printLine();
    cout << sizeof(Line) << endl;
}

int main(void){
    test0();
    return 0;
}
