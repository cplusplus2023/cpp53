#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{
public:
    Computer(const char * brand, double price)
    : _brand(new char[strlen(brand) + 1]())
    , _price(price) // _price = price
    {
        strcpy(_brand,brand);
        cout << "Computer(const char *, double)" << endl;
    }

    ~Computer(){
        if(_brand){//如果_brand申请了堆空间
            delete [] _brand;
            _brand = nullptr;
        }
        cout << "~Computer()" << endl;
    }


    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }
private:
    char * _brand;
    double _price;
};

void test0(){
    Computer pc("Apple",15000);
    pc.print();
    /* pc.~Computer(); */
    /* pc.print(); */
    /* cout << 1111 << endl; */
    Computer pc3("Apple",15000);
    pc3.print();

    static Computer pc4("Huawei",5900);
    pc4.print();

    int * p = new int(1);
    delete p;
    p = nullptr;

    Computer * p1 = new Computer("Asus",4900);
    delete p1;
    p1 = nullptr;

}

Computer pc2("Xiao",5600);

/* void test1(){ */
/*     cout << sizeof(Computer) << endl; */
/* } */


int main(void){
    test0();
    cout << "over" << endl;
    return 0;
}
