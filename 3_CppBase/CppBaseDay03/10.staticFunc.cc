#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{
public:
    Computer(const char * brand, double price)
        : _brand(new char[strlen(brand) + 1]())
          , _price(price) // _price = price
    {
        strcpy(_brand,brand);
        _totalPrice += _price;
        cout << "Computer(const char *, double)" << endl;
    }

    Computer(const Computer & rhs)
        : _brand(new char[strlen(rhs._brand)]())
          , _price(rhs._price)
    {
        strcpy(_brand,rhs._brand);
        _totalPrice += _price;
        cout << "Computer(const Computer &)" << endl;
    }

    Computer & operator=(const Computer & rhs)
    {
        if(this != &rhs){//1.考虑自复制
            delete [] _brand;//2.回收原本申请的空间
            _totalPrice -= _price;
            _brand = new char[strlen(rhs._brand) + 1]();//3.深拷贝
            strcpy(_brand,rhs._brand);
            _price = rhs._price;
            _totalPrice += _price;
        }
        cout << "Computer& operator=(const Computer&)" << endl;
        return *this;//4.返回*this
    }

    ~Computer(){
        if(_brand){
            delete [] _brand;
            _brand = nullptr;
            _totalPrice -= _price;
        }
        cout << "~Computer()" << endl;
    }

    void test(){
        this->print();
    }

    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
        cout << "totalPrice:" << _totalPrice << endl; 
    }

    static void printTotalprice(){
        cout << "totalPrice:" << _totalPrice << endl;
        /* cout << "brand:" << _brand << endl; */
        /* cout << "price:" << _price << endl; */
        //print();
    }

private:
    char * _brand;
    double _price;
    static double _totalPrice;
};

double Computer::_totalPrice = 0;

void test0(){
    Computer pc("Apple",15000);
    Computer pc2("Lenovo",6000);
    Computer pc3("Dell",8000);
    Computer pc4 = pc3;
    pc4 = pc;
    pc.print();
    cout << sizeof(Computer) << endl;

    pc2.printTotalprice();
    Computer::printTotalprice();

}





int main(void){
    test0();
    return 0;
}
