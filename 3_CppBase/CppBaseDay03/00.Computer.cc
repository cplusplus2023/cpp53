#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{
public:
    Computer(const char * brand, double price)
    /* : _brand(brand) // _brand = brand */
    /* : _brand("Xiaomi") */
    /* : _brand{'X','M'} */
    : _price(price) // _price = price
    {
        strcpy(_brand,brand);
        cout << "Computer(const char *, double)" << endl;
    }

    ~Computer(){
        cout << "~Computer()" << endl;
    }

    //动作:成员函数，命名遵循小驼峰规则
    void setBrand(const char * brand){
        strcpy(_brand,brand);
    }

    void setPrice(double price){
        _price = price;
    }

    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }
private:
    //属性:数据成员，命名通常以_作为开头
    char _brand[20];
    double _price;
};

void test0(){
    //内置类型
    int a;
    //自定义类型,类比内置类型去创建一个对象
    Computer pc("Apple",15000);
    pc.print();
}


int main(void){
    test0();
    return 0;
}
