#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

#if 0
    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "const Point &" << endl;
    }
#endif

#if 0
    //在类中的所有成员函数中都有一个隐藏的this指针（第一个参数）
    //this指针代表的是当前对象
    //在编译之后，编译器会自动加上该指针
    //形式：Point * const this
    //不要在参数列表中显式写出this指针
    Point& operator=(const Point & rhs)
    {
        this->_ix = rhs._ix;
        this-> _iy = rhs._iy;
        cout << "Point& operator=(const Point &)" << endl;
        return *this;
    }
#endif
    //const Point * const this
    void print() const{
        /* _ix = 100; */
        /* _iy = 200; */
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
        cout << "const版本" << endl;
    }

    //Point * const this
    /* void print() { */
    /*     cout << "(" << _ix */
    /*         << "," << _iy */
    /*         << ")" << endl; */
    /*     cout << "非const版本" << endl; */
    /* } */

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    Point pt(1,2);
    pt.print();

    const Point pt2(9,8);
    pt2.print();
}

int main(void){
    test0();
    return 0;
}
