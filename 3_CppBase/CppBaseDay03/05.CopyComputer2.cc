#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{
public:
    Computer(const char * brand, double price)
        : _brand(new char[strlen(brand) + 1]())
          , _price(price) // _price = price
    {
        strcpy(_brand,brand);
        cout << "Computer(const char *, double)" << endl;
    }

    Computer(const Computer & rhs)
        : _brand(new char[strlen(rhs._brand)]())
          , _price(rhs._price)
    {
        strcpy(_brand,rhs._brand);
        cout << "Computer(const Computer &)" << endl;
    }

    Computer & operator=(const Computer & rhs)
    {
        if(this != &rhs){//1.考虑自复制
            delete [] _brand;//2.回收原本申请的空间
            _brand = new char[strlen(rhs._brand) + 1]();//3.深拷贝
            strcpy(_brand,rhs._brand);
            _price = rhs._price;
        }
        cout << "Computer& operator=(const Computer&)" << endl;
        return *this;//4.返回*this
    }

    ~Computer(){
        if(_brand){
            delete [] _brand;
            _brand = nullptr;
        }
        cout << "~Computer()" << endl;
    }


    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }
private:
    char * _brand;
    double _price;
};

void test0(){
    Computer pc("Apple",15000);
    Computer pc2("Lenovo",6000);
    Computer pc3("Dell",8000);
    pc3 = pc;
    //pc3.operator=(pc);
    pc3.print();
    pc.print();

    //pc的值给pc2,再把pc2的值复制给pc3
    pc3 = pc2 = pc;
    pc3.print();
    int a = 1, b = 2, c = 3;
    a = b = c;
    cout << a << endl;

    pc3 = pc3;
    pc3.print();
}

void test1(){
    Computer pc = Computer("Apple",12000);
    pc = Computer("Asus",7000);
}




int main(void){
    test1();
    return 0;
}
