#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "const Point &" << endl;
    }

    void print(){
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    Point pt(1,2);
    pt.print();
    Point pt2 = pt;
    pt.print();
    pt2.print();
    cout << &pt << endl;
    cout << &pt2 << endl;

    //拷贝构造函数的参数为const引用，才能绑定右值
    Point pt3 = Point(1,20);
    //&Point(1,20); //创建的匿名对象是右值

    int num = 1;
    &num;//num是左值

    //1没有存在内存中，存在指令系统中
    //&1;//1是右值
    
    int & ref = num;
    const int & ref2 = 1;//原理右值引用&&

}

//调用拷贝构造的第二种时机
void func(Point p)
{
    p.print();
}

void test1(){
    Point pt(3,4);
    func(pt);
}

//调用拷贝构造的第三种时机
//加上去优化参数查看结果
Point func2(){
    Point pt2(1,8);
    return pt2;
}

void test2(){
    func2();
}


int main(void){
    test0();
    return 0;
}
