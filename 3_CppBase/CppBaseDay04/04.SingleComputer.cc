#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{
public:
    static Computer * getInstance(){
        if(_pInstance == nullptr){
            _pInstance = new Computer("apple",13000);
        }
        return _pInstance;
    }

    void init(const char * brand, double price)
    {
        if(_brand){
            delete [] _brand;
        }
        _brand = new char[strlen(brand) + 1]();
        strcpy(_brand,brand);
        _price = price;
    }


    Computer(const Computer & rhs) = delete;
    Computer & operator=(const Computer & rhs) = delete;

    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }

    static void destroy(){
        if(_pInstance){
            delete _pInstance;//回收堆对象整个的空间
            _pInstance = nullptr;
        }
        cout << "堆空间上的对象释放" << endl;
    }
private:
    Computer(const char * brand, double price)
        : _brand(new char[strlen(brand) + 1]())
          , _price(price) // _price = price
    {
        strcpy(_brand,brand);
        cout << "Computer(const char *, double)" << endl;
    }

    ~Computer(){
        if(_brand){
            delete [] _brand;//回收数据成员申请的空间
            _brand = nullptr;
        }
        cout << "~Computer()" << endl;
    }

    char * _brand;
    double _price;
    static Computer * _pInstance;
};
Computer* Computer::_pInstance = nullptr;

void test0(){
    Computer::getInstance()->print();
    Computer::getInstance()->init("XiaoMi",7800);
    Computer::getInstance()->print();
    Computer::destroy();

    cout << endl;
    Computer::getInstance()->init("XiaoMi",7800);
    Computer::getInstance()->print();
    Computer::destroy();
}




int main(void){
    test0();
    return 0;
}
