#include <iostream>
using std::cout;
using std::endl;


class Stack {
public:
    Stack(int capacity)
        : _top(-1)
          , _capacity(capacity)
          , _data(new int[_capacity]())
    {
        cout << "Stack(int)" << endl;
    }

    ~Stack(){
        if(_data){
            delete [] _data;
            _data = nullptr;
        }
        cout << "~Stack()" << endl;
    }

    bool empty();	//判断栈是否为空
    bool full();	//判断栈是否已满
    void push(int); //元素入栈
    void pop();     //元素出栈
    int  top();		//获取栈顶元素

private:
    int _top;
    int _capacity;
    int * _data;
};

bool Stack::empty(){	//判断栈是否为空
    return _top == -1;
}

bool Stack::full(){	//判断栈是否已满
    return _top == _capacity - 1;
}

void Stack::push(int x){ //元素入栈
    if(!full()){
        _data[++_top] = x;
    }else{
        cout << "Stack is full!" << endl; 
    }
}

void Stack::pop()    //元素出栈
{
    if(!empty()){
        --_top;
    }else{
        cout << "Stack is empty!" << endl;
    }
}

int Stack::top()	//获取栈顶元素
{
    return _data[_top];
}

void test0(){
    Stack stack(10);
    cout << "当前栈中是否为空：" << stack.empty() << endl;
    stack.push(10);
    cout << "当前栈中是否为空：" << stack.empty() << endl;
    for(int idx = 1; idx < 20; ++idx){
        stack.push(idx);
    }

    cout << "当前栈中是否为空：" << stack.empty() << endl;
    cout << "当前栈中是否已满:" << stack.full() << endl;

    while(!stack.empty()){
        cout << stack.top() << endl;
        stack.pop();
    }

    cout << "当前栈中是否为空：" << stack.empty() << endl;
}

int main(void){
    test0();
    return 0;
}
