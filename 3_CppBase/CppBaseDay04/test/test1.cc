#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "const Point &" << endl;
    }

    Point& operator=(const Point & rhs)
    {
        this->_ix = rhs._ix;
        this-> _iy = rhs._iy;
        cout << "Point& operator=(const Point &)" << endl;
        return *this;
    }

    void print();

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

    void Point::print(){
        cout << "(" << this->_ix
            << "," << this->_iy
            << ")" << endl;

        _ix = 100;
    }
void test0(){
    Point pt(1,2);
    pt.print();
}

int main(void){
    test0();
    return 0;
}
