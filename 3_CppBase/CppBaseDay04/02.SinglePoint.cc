#include <iostream>
using std::cout;
using std::endl;

class Point
{
    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

public:
    static Point & getInstance(){
        //当静态函数多次被调用
        //静态的局部对象只会被初始化一次
        //第一次调用时，静态对象会被初始化为一个对象实例
        //后续的调用中，静态局部对象已经存在，不会在初始化
        //而是直接返回已经初始化的对象实例
        static Point pt(1,2);
        return pt;
    }
private:
    Point(const Point & rhs)
    : _ix(rhs._ix)
    , _iy(rhs._iy)
    {
        cout << "const Point &" << endl;
    }

    Point& operator=(const Point & rhs)
    {
        this->_ix = rhs._ix;
        this-> _iy = rhs._iy;
        cout << "Point& operator=(const Point &)" << endl;
        return *this;
    }

public:
    void print() const{
        cout << "(" << this->_ix
            << "," << this->_iy
            << ")" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    Point & pt = Point::getInstance();
    pt.print();

    Point & pt2 = Point::getInstance();
    pt2.print();

    cout << &pt << endl;
    cout << &pt2 << endl;
}

int main(void){
    test0();
    return 0;
}
