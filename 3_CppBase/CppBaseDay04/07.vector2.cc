#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;

void test0(){
    vector<int> numbers;
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(1);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    cout << sizeof(numbers) << endl;

    //释放vector可以存放元素但没有存放的多余的空间
    numbers.shrink_to_fit();
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    //清空所有的元素，但是只删除元素，并不回收空间
    numbers.clear();
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;
    numbers.shrink_to_fit();
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

}

void test1(){
    vector<int> * p = new vector<int>();
    p->push_back(10);
    delete p;
    p = nullptr;
}

void test2(){
    vector<int> numbers;
    //只申请空间，没有存放元素
    //如果预估需要存放count个元素，可以使用reserve函数
    //开辟能够存放count个元素的空间，然后根据需求存放元素
    numbers.reserve(10);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    numbers.push_back(100);
    cout << "numbers'size:" << numbers.size() << endl;
    cout << "numbers'capacity:" << numbers.capacity() << endl;

    vector<int> numbers2(10);
    cout << "numbers2'size:" << numbers2.size() << endl;
    cout << "numbers2'capacity:" << numbers2.capacity() << endl;

}

int main(void){
    test2();
    return 0;
}
