#include <string>
#include <iostream>
using std::cout;
using std::endl;
using std::string;

void test0(){
    string str;
    cout << "str:" << str << endl;
    cout << str.empty() << endl;
    cout << str.size() << endl;

    string str1(5,'a');
    cout << "str1:" << str1 << endl;
    cout << str1.empty() << endl;
    cout << str1.size() << endl;

    string str2("hello");
    cout << "str2:" << str2 << endl;
    cout << str2.empty() << endl;
    cout << str2.size() << endl;

    string str3(str2);
    cout << "str3:" << str3 << endl;
    cout << str3.empty() << endl;
    cout << str3.size() << endl;

    string str4(",world!");
    //用+号对string对象进行拼接
    string str5 = str3 + str4;
    cout << "str5:" << str5 << endl;

    string str6(",yes");
    string str7 = str5 + "cpp" + str6;
    cout << "str7:" << str7 << endl;

    printf("str5:%s\n",str5.c_str());
    
    str5.push_back('a');
    cout << "str5:" << str5 << endl;
    
    str3.append(3,'b');
    cout << "str3:" << str3 << endl;
    str3.append(str1);
    cout << "str3:" << str3 << endl;
    //第一个参数是一个string对象str2
    //第二个参数pos代表从str2的下标1位置开始拼接
    //第三个参数count代表拼接str2的count个字符
    str3.append(str2,1,3);
    cout << "str3:" << str3 << endl;
    str2.append(",world");
    cout << "str2:" << str2 << endl;

    size_t pos = str5.find("world");
    cout << "pos:" << pos << endl;

    string str8("ell");
    pos = str5.find(str8);
    cout << "pos:" << pos << endl;

    pos = str5.find("ella",0,3); 
    cout << "pos:" << pos << endl;
}

void test1(){
    string str("hello,world!");
    //C++字符串也可以用下标访问单个字符
    for(size_t idx = 0; idx < str.size(); ++idx){
        cout << str[idx] << " ";
    }
    cout << endl;

    //增强for循环
    //通常和auto一起使用
    //auto关键字表示自动推导类型
    //&表示引用，直接操作元素本身
    //如果不加引用，就会进行复制
    //冒号左边表示容器中的元素，右边表示容器
    for(auto & ch : str){
        cout << ch << " ";
    }
    cout << endl;

    //迭代器方式遍历C++字符串
    //迭代器理解为是广义的指针
    //string::iterator it = str.begin();
    auto it = str.begin();
    while(it != str.end()){
        cout << *it << " ";
        ++it;
    }
    cout << endl;

    //字符串截取
    string str2 = str.substr(1,5);
    cout << str2 << endl;
    
    //字符串比较
    string str3("ello,");
    cout << (str2 == str3) << endl;
    
}

int main(void){
    test1();
    return 0;
}
