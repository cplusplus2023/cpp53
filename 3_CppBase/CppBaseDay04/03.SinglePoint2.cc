#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    static Point * getInstance(){
        if(_pInstance == nullptr){
            _pInstance = new Point(1,2);
        }
        return _pInstance;
    }

    void init(int x, int y){
        _ix = x;
        _iy = y;
    }
    
    //C++11
    //能够禁止一切形式的复制
    Point(const Point & rhs) = delete;
    Point& operator=(const Point & rhs) = delete;

    static void destroy(){
        if(_pInstance){
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

    void print() const{
        cout << "(" << this->_ix
            << "," << this->_iy
            << ")" << endl;
    }

private:
    Point(){}

    Point(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int,int)" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }

    int _ix;
    int _iy;
    static Point * _pInstance;
};

Point* Point::_pInstance = nullptr;

void test0(){
    Point * pt = Point::getInstance();
    pt->init(100,200);
    pt->print();

    Point * pt2 = Point::getInstance();
    pt2->init(300,400);
    pt2->print();

    cout << pt << endl;
    cout << pt2 << endl;

    /* delete pt; */
    /* pt = nullptr; */
    /* pt2->print(); */

}

void test1(){
    //实际工作中更常用这种方式去使用单例对象
    Point::getInstance()->print();
    Point::getInstance()->init(10,80);
    Point::getInstance()->print();
    Point::destroy();
}

int main(void){
    test1();
    return 0;
}
