#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;

void test0(){
    long arr[5] = {1,2,3,4,5};

    //表示vector中存放的是int类型的元素
    //number就是容器名（vector的名字）
    vector<int> number;
    cout << "is number empty? " << number.empty() << endl;
    cout << "number's size: " << number.size() << endl;

    //开辟了能够存放10个long型元素的空间，
    //并存放了10个0（有初始化）
    vector<long> number2(10);
    cout << "is number2 empty? " << number2.empty() << endl;
    cout << "number2's size: " << number2.size() << endl;

    for(auto & l : number2){
        cout << l << " ";
    }
    cout << endl;

    //迭代器方式创建vector
    //传入的两个迭代器，第一个是首元素的迭代器，
    //第二是是尾后的迭代器
    vector<long> number3(arr,arr + 5);
    for(auto & l : number3){
        cout << l << " ";
    }
    cout << endl;

    //迭代器方式遍历vector
    /* vector<long>::iterator it = number3.begin(); */ 
    auto it = number3.begin();
    while(it != number3.end()){
        cout << *it << " ";
        ++it;
    }
    cout << endl;

    cout << number3.size() << endl;
}

int main(void){
    test0();
    return 0;
}
