#include <iostream>
using std::cout;
using std::endl;

class A{
public:
    virtual void a(){ cout << "A::a()" << endl; } 
    virtual void b(){ cout << "A::b()" << endl; } 
    void c(){ cout << "A::c()" << endl; } 
};

class B{
public:
    virtual void a(){ cout << "B::a()" << endl; } 
    virtual void b(){ cout << "B::b()" << endl; } 
    void c(){ cout << "B::c()" << endl; } 
    void d(){ cout << "B::d()" << endl; } 
};

class C
: public A
, public B
{
public:
    virtual void a(){ cout << "C::a()" << endl; } 
    void c(){ cout << "C::c()" << endl; } 
    void d(){ cout << "C::d()" << endl; } 
};

class D
: public C
{
public:
    void c(){ cout << "D::c()" << endl; }
};

void test2(){
    cout << sizeof(C)  << endl;
    cout << sizeof(D)  << endl;
}

void test1(){
    //验证C类中的c函数是一个虚函数
    /* D d; */
    /* C* pc = &d; */
    /* pc->c();  //动态多态，覆盖 D::c() */

    D d;
    C* pc = &d;
    pc->c();  //只能操纵C类的部分  C::c()
}


void test0(){
    C c;
    c.a();//没有动态多态，隐藏 C::a()
    //c.b();//error，二义性    
    c.c();//没有动态多态，隐藏 C::c()
    c.d();//隐藏               C::d()


    cout << endl;
    A* pa = &c;//只能操纵A类基类子对象的部分
    pa->a();//动态多态，有覆盖 C::a()
    pa->b();//没有动态多态，无覆盖 A::b()
    pa->c();//动态多态，有覆盖 C::c()
    //pa->d();//error,A类中没有d函数
    
    cout << endl;
    B* pb = &c;
    pb->a();//没有动态多态，有覆盖 C::a()
    pb->b();//没有动态多态，因为无覆盖 B::b()
    pb->c();//只能访问到B类的实现，不是虚函数 B::c()
    pb->d();//同上  B::d()


    cout << endl;
    C * pc = &c;//不会触发动态多态
    pc->a();//隐藏   C::a()
    //pc->b();//二义性，C中没有b函数的实现
    pc->c();//隐藏  C::c()
    pc->d();//隐藏  C::d()
}

int main(void){
    test2();
    return 0;
}
