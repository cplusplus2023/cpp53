#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int x = 0, int y = 0)
    : _ix(x)
    , _iy(y)
    {
        cout << "Point(int=0,int=0)" << endl;
    }

    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }
    int getY() const{return _iy;}
protected:
    int _ix;
private:
    int _iy;
};

class Point3D
: protected Point
{
public:
    //派生类的构造中需要在初始化列表调用基类的构造
    //如果没有显式调用就会隐式调用基类的无参构造
    //
    Point3D(int x, int y, int z)
    : Point(x,y)
    , _iz(z)
    {
        cout << "Point3D(int*3)" << endl;
    }

    void display() const{
        cout << "(" << _ix
            /* << "," << _iy */
            << "," << getY()
            << "," << _iz
            << ")" << endl;
        print();
    }
private:
    int _iz;
};

class Point4D
: public Point3D
{
    void show() const{
        cout << _ix << endl;
        /* cout << _iy << endl; */
        /* cout << _iz << endl; */
        print();
    }

};

void test0(){
    /* Point pt; */
    /* pt._ix; */
    Point3D pt3d(3,4,5);
    pt3d.display();
    cout << sizeof(Point) << endl;
    cout << sizeof(Point3D) << endl;
}

int main(void){
    test0();
    return 0;
}
