#include <iostream>

using std::cout;
using std::endl;

template <typename T>
class Singleton
{
public:
    template <typename ...Args>
    static T *getInstance(Args ...args)
    {
        if(nullptr == _pInstance)
        {
            _pInstance = new T(args...);//堆对象
            _ar;//实例化一次
        }

        return _pInstance;
    }

    static void destroy()
    {
        if(_pInstance)
        {
            delete _pInstance;
            _pInstance = nullptr;
        }
    }
private:
    Singleton()
    {
        cout << "Singleton()" << endl;
    }
    ~Singleton()
    {
        cout << "~Singleton()" << endl;
    }

private:
    class AutoRelease
    {
    public:
        AutoRelease()
        {
            cout << "AutoRelease()" << endl;
        }
    
        ~AutoRelease()
        {
            cout << "~AutoRelease()" << endl;
            if(_pInstance)
            {
                delete _pInstance;
                _pInstance = nullptr;
            }
        }
    };
private:
    static T *_pInstance;
    static AutoRelease _ar;
};

template <typename T>
T *Singleton<T>::_pInstance = nullptr;

template <typename T>
typename Singleton<T>::AutoRelease Singleton<T>::_ar;

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int = 0, int = 0)" << endl;
    }

    ~Point()
    {
        cout << "~Point()" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test()
{
    Point *pt = Singleton<Point>::getInstance(1, 2);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

