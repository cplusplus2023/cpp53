#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::map;
using std::string;
using std::pair;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first << "  " << elem.second << endl;
    }
}

void test()
{
    //map的特征
    //1、存放的是key-value类型，key值是唯一的，不能重复，但是
    //value值是可以重复的
    //2、默认会按照key值进行升序排列
    //3、底层使用的是红黑树
    map<int, string> number = {
        pair<int, string>(2, "hello"),
        pair<int, string>(8, "wangdao"),
        {3, "beijing"},
        {2, "hello"},
        {4, "beijing"},
        make_pair(7, "nanjing"),
        make_pair(4, "tianjin")
    };
    display(number);

    cout << endl << "map的查找操作" << endl;
    size_t cnt = number.count(4);
    cout << "cnt = " << cnt << endl;

    cout << endl;
    auto it = number.find(5); 
    if(it == number.end())
    {
        cout << "该元素是不存在map中的" << endl;
    }
    else
    {
        cout << "该元素存在map中 " 
             << it->first << "  " << it->second << endl;
    }

    cout << endl << "map的插入操作" << endl;
    pair<map<int, string>::iterator, bool> ret = 
        /* number.insert(pair<int, string>(5, "wangdao")); */
        /* number.insert({5, "wangdao"}); */
        number.insert(make_pair(5, "wangdao"));
    if(ret.second)
    {
        cout << "插入成功 " << ret.first->first
             << "  " << ret.first->second << endl;
    }
    else
    {
        cout << "插入失败" << endl;
    }
    display(number);

    cout << endl << "map的下标" << endl;
    cout << "number[2] = " << number[2] << endl;//查找
    cout << "number[1] = " << number[1] << endl;//插入
    display(number);

    cout << endl << endl;
    /* number.operator[](1).operator=("jingdong");//ok */
    number[1] = "jingdong";//修改
    number[4] = "jingdong";
    display(number);

    const map<int, string> number2 = {
        {1, "hello"}
    };
    /* number2[1];//error,operator[]没有const版本 */
};


int main(int argc, char *argv[])
{
    test();
    return 0;
}

