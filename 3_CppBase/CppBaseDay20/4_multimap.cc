#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::multimap;
using std::string;
using std::pair;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first << "  " << elem.second << endl;
    }
}

void test()
{
    //multimap的特征
    //1、存放的是key-value类型，key值是不唯一的，可以重复，但是
    //value值是既可以重复也可以不重复
    //2、默认会按照key值进行升序排列
    //3、底层使用的是红黑树
    multimap<int, string> number = {
        pair<int, string>(2, "hello"),
        pair<int, string>(8, "wangdao"),
        {3, "beijing"},
        {2, "hello"},
        {4, "beijing"},
        make_pair(7, "nanjing"),
        make_pair(4, "tianjin")
    };
    display(number);

    cout << endl << "multimap的查找操作" << endl;
    size_t cnt = number.count(4);
    cout << "cnt = " << cnt << endl;

    cout << endl;
    auto it = number.find(5); 
    if(it == number.end())
    {
        cout << "该元素是不存在multimap中的" << endl;
    }
    else
    {
        cout << "该元素存在multimap中 " 
             << it->first << "  " << it->second << endl;
    }

    cout << endl << "multimap的插入操作" << endl;
    /* number.insert(pair<int, string>(5, "wangdao")); */
    /* number.insert({5, "wangdao"}); */
    number.insert(make_pair(5, "wangdao"));
    display(number);

#if 0
    cout << endl << "multimap的下标" << endl;
    cout << "number[2] = " << number[2] << endl;//查找
    cout << "number[1] = " << number[1] << endl;//插入
    display(number);

    cout << endl << endl;
    /* number.operator[](1).operator=("jingdong");//ok */
    number[1] = "jingdong";//修改
    number[4] = "jingdong";
    display(number);

    const multimap<int, string> number2 = {
        {1, "hello"}
    };
    /* number2[1];//error,operator[]没有const版本 */
#endif
};


int main(int argc, char *argv[])
{
    test();
    return 0;
}

