#include <iostream>
#include <algorithm>
#include <vector>

using std::cout;
using std::endl;
using std::for_each;
using std::remove_if;
using std::vector;

bool func(int value)
{
    return value > 5;
}

void test()
{
    vector<int> vec = {1, 3, 6, 7, 5, 6, 4, 2, 9};
    for_each(vec.begin(), vec.end(),[](int value) {
             cout << value << "  " ;
             });

    //删除大于5的所有元素
    cout << endl << endl;
    //remove_if不能将满足条件的元素一次删除，但是可以返回待删除的
    //元素的首迭代器，然后再配合erase进行删除
    //remove_if需要与erase结合使用
    auto it = remove_if(vec.begin(), vec.end(), func);
    vec.erase(it, vec.end());

    for_each(vec.begin(), vec.end(),[](int value) {
             cout << value << "  " ;
             });
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

