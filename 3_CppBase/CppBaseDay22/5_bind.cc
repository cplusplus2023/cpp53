#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

int add(int x, int y)
{
    cout << "int add(int, int)" << endl;
    return x + y;
}

int func(int x, int y, int z)
{
    cout << "int func(int, int, int)" << endl;
    return x * y * z;
}

class Example
{
public:
    //Example * this
    int add(int x, int y)
    {
        cout << "int Example::add(int, int)" << endl;
        return x + y;
    }
};

void test()
{
    //在C语言中，函数名是函数的入口地址；数组名是数组的入口地址
    //bind可以绑定n元函数，该函数既可以是普通函数（自由函数、全局函数）
    //也可以是成员函数
    auto f = bind(add, 1, 3);
    cout << "f() = " << f() << endl;

    cout << endl;
    auto f2 = bind(&func, 3, 5, 6);
    cout << "f2() = " << f2() << endl;

    cout << endl;
    Example ex;
    auto f3 = bind(&Example::add, &ex, 10, 30);
    cout << "f3() = " << f3() << endl;

    cout << endl;
    /* using namespace std::placeholders; */
    //占位符
    auto f4 = bind(func, std::placeholders::_1, std::placeholders::_1, 30);
    cout << "f4(10, 20) = " << f4(10, 20) << endl;
}

int main(int argc, char *argv[])
{
    /* printf("%p\n", main); */
    /* printf("%p\n", &main); */
    test();
    return 0;
}

