#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

void func(int x1, int x2, int x3, const int &x4, int x5)
{
    cout << "x1 = " << x1 << endl
         << "x2 = " << x2 << endl
         << "x3 = " << x3 << endl
         << "x4 = " << x4 << endl
         << "x5 = " << x5 << endl;
}

void test()
{
    //占位符本身代表的是形参的位置
    //占位符中的数字代表的是实参的位置
    //默认采用的是值传递
    //cref = const reference,引用的包装器
    //ref = reference,引用的包装器
    using namespace std::placeholders;
    int number = 100;
    auto f = bind(func, 10, _3, _1, std::cref(number), number);
    number = 300;
    f(12, 45, 67, 89, 230, 400, 200, 400);//多余的参数直接废弃
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

