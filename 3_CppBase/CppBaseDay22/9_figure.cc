#include <math.h>
#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;
using std::function;

//int number = 10;
//左值引用 int &ref = number;//ok
//右值引用 int &&rref = 10;//ok
//
//左值引用 int &ref = 10;//error
//右值引用 int &&rref = number;//error
//
//const左值引用 const int &ref1 = number;//ok
//const左值引用 const int &ref2 = 10;//ok
//
//&ref; &rref; &ref1; &ref2, 在此处都是左值

//体现多态的两种方法
//面向对象的思想：继承 + 虚函数
//基于对象的思想：std::bind + std::function(更加灵活，函数的名字，参数
//列表没有必要完全一样)

class Figure
{
public:
    using DisplayCallback = function<void()>;
    using AreaCallback = function<double()>;
    /* virtual void display() const = 0; */
    /* virtual double area() const = 0; */
    //回调函数的注册与执行
    //cb是右值引用,cb是左值
    //数据成员进行复制，注册回调函数
#if 0
    Figure(DisplayCallback &&cb1, AreaCallback &&cb2)
    : _displayCallback(std::move(cb1))
    , _areaCallback(std::move(cb2))
    {

    }
#endif
    void setDisplayCallback(DisplayCallback &&cb)
    {
        _displayCallback = std::move(cb);
    }

    void setAreaCallback(AreaCallback &&cb)
    {
        _areaCallback = std::move(cb);
    }

    //执行回调函数
    void handleDisplayCallback() const
    {
        if(_displayCallback)
        {
            _displayCallback();//回调函数进行了执行
        }
    }

    double handleAreaCallback() const
    {
        if(_areaCallback)
        {
            return _areaCallback();
        }
        else
        {
            return 0.0;
        }
    }

public:
    DisplayCallback _displayCallback;
    AreaCallback _areaCallback;
};

class Rectangle
{
public:
    Rectangle(double length = 0, double width = 0)
    : _length(length)
    , _width(width)
    {
        cout << "Rectangle(double = 0, double = 0)" << endl;
    }

    void display(int x) const
    {
        cout << "Rectangle";
    }

    double area() const
    {
        return _length * _width;
    }

    ~Rectangle()
    {
        cout << "~Rectangle()" << endl;
    }
private:
    double _length;
    double _width;
};

class Circle
{
public:
    Circle(double radius = 0)
    : _radius(radius)
    {
        cout << "Circle(double = 0)" << endl;
    }

    void print() const 
    {
        cout << "Circle";
    }

    double printArea() const 
    {
        return 3.14 * _radius *_radius;;
    }

    ~Circle()
    {
        cout << "~Circle()" << endl;
    }
private:
    double _radius;
};

class Triangle
{
public:
    Triangle(double a = 0, double b = 0, double c = 0)
    : _a(a)
    , _b(b)
    , _c(c)
    {
        cout << "Triangle(double = 0, double = 0, double = 0)" << endl;
    }

    void show() const 
    {
        cout << "Triangle";
    }

    double showArea() const 
    {
        //海伦公式
        double tmp = (_a + _b + _c)/2;

        return sqrt(tmp * (tmp - _a) * (tmp - _b) * (tmp - _c));
    }

    ~Triangle()
    {
        cout << "~Triangle()" << endl;
    }
private:
    double _a;
    double _b;
    double _c;
};

void func(const Figure &fig)
{
    fig.handleDisplayCallback();
    cout << "的面积 : " << fig.handleAreaCallback() << endl;
}

int main(int argc, char **argv)
{
    Rectangle rectangle(10, 20);
    Circle circle(10);
    Triangle triangle(3, 4, 5);

    cout << endl;
    Figure fig;
    fig.setDisplayCallback(bind(&Rectangle::display, &rectangle, 40));
    fig.setAreaCallback(bind(&Rectangle::area, &rectangle));
    func(fig);

    fig.setDisplayCallback(bind(&Circle::print, &circle));
    fig.setAreaCallback(bind(&Circle::printArea, &circle));
    func(fig);

    fig.setDisplayCallback(bind(&Triangle::show, &triangle));
    fig.setAreaCallback(bind(&Triangle::showArea, &triangle));
    func(fig);

    return 0;
}

