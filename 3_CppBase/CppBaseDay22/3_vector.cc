#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void test()
{
    vector<int> vec;
    vec.push_back(10);

    bool flag = true;
    for(auto it = vec.begin(); it != vec.end(); ++it)
    {
        cout << *it << "  ";//读r
        if(flag)
        {
            //底层已经发生了扩容，迭代器还指向老的空间，
            //但是已经扩容，就会导致迭代器失效
            vec.push_back(20);//写w
            flag = false;
            it = vec.begin();//迭代器重新置位
        }
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

