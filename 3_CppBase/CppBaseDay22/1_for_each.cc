#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::ostream_iterator;
using std::copy;

#if 0
void func(int &value)
{
    ++value;
    cout << value << "  ";
}
#endif
void test()
{
    int a = 10;
    int b = 20;
    vector<int> vec = {1, 5, 9, 8, 6, 4};
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl;
    //lambda表达式===匿名函数
    //[],捕获列表,可以捕捉lambda外面的变量，但是默认采用的是只读,
    //如果想进行写操作，可以在捕获列表中进行&，或者加上mutable
    //(),参数列表
    //{}，函数体
    for_each(vec.begin(), vec.end(), [a, b](int &value)mutable->void{
             ++a;
             cout << a << "  ";
             cout << b << "  ";
             ++value; 
             cout << value << "  "; 
             });
    cout << endl;

    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

