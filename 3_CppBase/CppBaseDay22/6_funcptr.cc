#include <iostream>

using std::cout;
using std::endl;

int func1()
{
    return 10;
}

int func2()
{
    return 20;
}

int add(int x, int y)
{
    return x + y;
}

void test()
{
    func1();
    /* typedef int A; */
    //函数指针的用法
    typedef int (*pFunc)();//C的方法
    /* typedef int (*)() pFunc; */
    /* using pFunc = int (*)(); //C++11的方法 */
    //回调函数：将注册与执行分开
    //思想：延迟调用的思想
    pFunc f = &func1;//回调函数的注册
    //....
    //...
    //...
    cout << "f() = " << f() << endl;//回调函数的执行

    f = &func2;
    cout << "f() = " << f() << endl;

    /* f = add;//error */
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

