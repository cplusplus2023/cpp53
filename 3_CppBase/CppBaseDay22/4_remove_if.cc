#include <iostream>
#include <algorithm>
#include <vector>
#include <functional>

using std::cout;
using std::endl;
using std::for_each;
using std::remove_if;
using std::vector;
using std::bind1st;
using std::bind2nd;
using std::less;
using std::greater;

void test()
{
    vector<int> vec = {1, 3, 6, 7, 5, 6, 4, 2, 9};
    for_each(vec.begin(), vec.end(),[](int value) {
             cout << value << "  " ;
             });

    cout << endl << endl;
    //删除小于5的
    /* auto it = remove_if(vec.begin(), vec.end(), bind1st(less<int>(), 5)); */
    auto it = remove_if(vec.begin(), vec.end(), bind2nd(greater<int>(), 5));
    
    //下面的是删除所有小于5的元素
    /* auto it = remove_if(vec.begin(), vec.end(), bind2nd(less<int>(), 5)); */
    vec.erase(it, vec.end());
#if 0
    vec.erase(remove_if(vec.begin(), vec.end(), [](int value){
                        return value > 5;
                        }), vec.end());
#endif

    for_each(vec.begin(), vec.end(),[](int value) {
             cout << value << "  " ;
             });
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

