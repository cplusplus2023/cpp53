#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
    Base(){
        cout << "Base()" << endl;
    }

    virtual void print() const{}

    ~Base(){
        cout << "~Base()" << endl;
    }
private:
    long _base = 10;
};


class Derived
: public Base
{
public:
    Derived()
    {
        cout << "Derived()" << endl;
    }

    void display() const{
        cout << "Derived::_derived:" << _derived << endl;
    }

    ~Derived(){
        cout << "~Derived()" << endl;
    }
private:
    long _derived = 12;
};


void test0(){
    Base base;
    Derived d1;

    /* Derived * p = &d1; */

    //会调用Base类的赋值运算符函数
    //Base& operator=(const Base & rhs)
    //const Base & rhs = d1;
    //base = d1;//ok
    //d1 = base;//error

    //Base * pbase = &base;
    Base * pbase = &d1;//ok
    //Derived * pderived = &base;//error
    //因为前面进行了一次赋值，base = d1
    //所以其实应该让Derived指针可以指向base对象
    //此处应该是一个合理的向下转型
    
    //向下转型
    Derived * pd = dynamic_cast<Derived*>(pbase);
    if(pd){
        cout << "转换成功" << endl;
        pd->display();
    }else{
        cout << "转换失败" << endl;
    }

    cout << endl;
    



    
    Base & rbase = d1;//ok
    //Derived & rderived = base;//error

}

int main(void){
    test0();
    return 0;
}
