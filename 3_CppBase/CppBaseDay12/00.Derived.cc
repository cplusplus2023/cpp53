#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
    Base(long x){
        cout << "Base()" << endl;
        _base = x;
    }

    ~Base(){
        cout << "~Base()" << endl;
    }
private:
    long _base;
};

class Test{
public:
    Test(long test)
    : _test(test)
    { cout << "Test()" << endl; }
    ~Test(){ cout << "~Test()" << endl; }
private:
    long _test;
};

class Derived
: public Base
{
public:
    Derived(long base,long test,long b2,long derived)
    : Base(base)//创建基类子对象
    , _t(test)//创建Test类的成员子对象
    , _b(b2)//创建Base类的成员子对象
    , _derived(derived)
    {
        cout << "Derived()" << endl;
    }

    ~Derived(){
        cout << "~Derived()" << endl;
    }
private:
    Test _t;
    Base _b;
    long _derived;

};


void test0(){
    Derived dd(1,2,3,4);
}

int main(void){
    test0();
    return 0;
}
