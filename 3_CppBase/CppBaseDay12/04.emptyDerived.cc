#include <iostream>
using std::cout;
using std::endl;

class A
{
};

class B
: public A
{};

class C
: public A
{};

class D
: public B
, public C
{
    double _d;
};

void test0(){
    cout << sizeof(A) << endl;
    cout << sizeof(B) << endl;
    cout << sizeof(C) << endl;
    cout << sizeof(D) << endl;
}

int main(void){
    test0();
    return 0;
}
