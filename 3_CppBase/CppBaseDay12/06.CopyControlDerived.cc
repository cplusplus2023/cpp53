#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
    Base(long base)
    : _base(base)
    {}

protected:
    long _base = 10;
};


class Derived
: public Base
{
public:
    Derived(long base, long derived)
    : Base(base)
    , _derived(derived)
    {}

    void display() const{
        cout << "Derived::_base:" << _base << endl;
        cout << "Derived::_derived:" << _derived << endl;
    }

    Derived(const Derived & rhs)
    //: Base(rhs._base)//调用Base的构造
    : Base(rhs)//调用Base的拷贝构造
    , _derived(rhs._derived)
    {
        cout << "Derived(const Derived & rhs)" << endl;
    }

    Derived &operator=(const Derived & rhs){
        //调用Base的赋值运算符函数
        Base::operator=(rhs);
        _derived = rhs._derived;
        cout << "Derived& operator=(const Derived &)" << endl;
        return *this;
    }

private:
    long _derived = 12;
};


void test0(){
    Derived d1(1,2);
    d1.display();
    Derived d2 = d1;
    d2.display();

    Derived d3(100,200);
    d2 = d3;
    d2.display();

}

int main(void){
    test0();
    return 0;
}
