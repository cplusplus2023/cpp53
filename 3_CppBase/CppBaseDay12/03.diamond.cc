#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
    void print() const{
        cout << "A::print()" << endl;
    }
    double _a;
};


class B
: virtual public A
{
public:
    double _b;
};

class C
: virtual public A
{
public:
    double _c;
};

class D
: public B
, public C
{
public:
    double _d;
};

void test0(){
    D d;
    /* d.print(); */
    cout << sizeof(d) << endl;
}

int main(void){
    test0();
    return 0;
}
