#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
    Base(long x){
        cout << "Base()" << endl;
        _base = x;
    }

    ~Base(){
        cout << "~Base()" << endl;
    }
    
    void print() const{
        cout << "Base::_base:" << _base << endl;
        cout << "Base::_data:" << _data  << endl;
    }

    long _data = 100;
private:
    long _base;
};


class Derived
: public Base
{
public:
    Derived(long base,long derived)
    : Base(base)//创建基类子对象
    , _derived(derived)
    {
        cout << "Derived()" << endl;
    }

    ~Derived(){
        cout << "~Derived()" << endl;
    }

    void print(int x) const{
        cout << "Derived::_derived:" << _derived << endl;
        cout << "Derived::_data:" << _data  << endl;
    }

    long _data = 19;
private:
    long _derived;

};


void test0(){
    Derived dd(1,2);
    /* dd.print(); */
    cout << dd.Base::_data << endl;
    cout << endl;
    cout << "sizeof(Derived):" << sizeof(dd) << endl;
}

void test1(){
    Base base(11);
    base.print();

    cout << endl;
    Derived derived(11,33);
    derived.Base::print();


}

int main(void){
    test1();
    return 0;
}
