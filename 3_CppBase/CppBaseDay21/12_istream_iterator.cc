#include <iostream>
#include <iterator>
#include <vector>

using std::cout;
using std::endl;
using std::istream_iterator;
using std::ostream_iterator;
using std::back_inserter;
using std::vector;

void test()
{
    vector<int> vec;
    /* vec.reserve(10); */
    istream_iterator<int> isi(std::cin);
    /* copy(isi, istream_iterator<int>(), vec.begin()); */
    //back_inserter的底层会调用push_back函数
    //isi与istream_iterator<int>()相等可以破坏流的状态
    copy(isi, istream_iterator<int>(), back_inserter(vec));

    copy(vec.begin(), vec.end(), 
         ostream_iterator<int>(cout, "  "));

}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

