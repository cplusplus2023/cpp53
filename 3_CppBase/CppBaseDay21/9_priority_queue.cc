#include <iostream>
#include <queue>
#include <vector>

using std::cout;
using std::endl;
using std::priority_queue;
using std::vector;

void test()
{
    vector<int> vec = {1, 5, 9, 7, 2, 5, 4};
    /* priority_queue<int> pque(vec.begin(), vec.end()); */

    //优先级队列的底层原理:使用的是堆排序，默认是大根堆
    //当有元素进行插入的时候，会将堆顶与新插入的元素进行比较,如果
    //堆顶比新插入的元素要小，就会满足std::less,就会将新插入的元素
    //作为新的堆顶；当继续插入元素的时候，将堆顶与新插入的元素进行
    //比较，如果堆顶比新插入的元素要大，那么就不满足std::less，那么
    //堆顶依旧是堆顶
    priority_queue<int> pque;
    for(size_t idx = 0; idx != vec.size(); ++idx)
    {
        pque.push(vec[idx]);
        cout << "优先级最高的元素 " << pque.top() << endl;
    }

    //优先级队列没有迭代器，所以就不能使用传统的迭代器的方法遍历
    cout << endl;
    while(!pque.empty())
    {
        /* cout << "优先级最高的元素 " << pque.top() << endl; */
        cout << pque.top() << "  ";
        pque.pop();
    }
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

