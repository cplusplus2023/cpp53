#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <iterator>
#include <algorithm>

using std::cout;
using std::endl;
using std::vector;
using std::list;
using std::set;
using std::back_inserter;
using std::back_insert_iterator;
using std::front_inserter;
using std::front_insert_iterator;
using std::inserter;
using std::insert_iterator;
using std::copy;
using std::ostream_iterator;

void test()
{
    vector<int> vecNumber = {1, 3, 9, 7};
    list<int> listNumber = {4, 8, 6, 2};
    //将list中的元素拷贝到vector的尾部
    //插入操作
    copy(listNumber.begin(), listNumber.end(), 
         back_inserter(vecNumber));

    //遍历操作
    copy(vecNumber.begin(), vecNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;
    
    //将vector中的元素拷贝到list的头部
    cout << endl;
    /* copy(vecNumber.begin(), vecNumber.end(), */ 
    /*      front_inserter(listNumber)); */
    copy(vecNumber.begin(), vecNumber.end(), 
         front_insert_iterator<list<int>>(listNumber));

    //遍历操作
    copy(listNumber.begin(), listNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl;
    set<int> setNumber = {1, 10, 40, 12, 7};
    //将vector中的元素插入到set中
    auto it = setNumber.begin();
    copy(vecNumber.begin(), vecNumber.end(), 
         inserter(setNumber, it));
    
    //遍历操作
    copy(setNumber.begin(), setNumber.end(), 
         ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

