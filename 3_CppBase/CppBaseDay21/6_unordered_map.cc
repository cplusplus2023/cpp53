#include <iostream>
#include <unordered_map>
#include <string>
#include <utility>

using std::cout;
using std::endl;
using std::unordered_map;
using std::string;
using std::pair;
using std::make_pair;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem.first << "  " << elem.second << endl;
    }
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    ~Point()
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
       << ", " << rhs._iy
       << ")";
    return os;
}

void test()
{
    unordered_map<string, Point> number = {
        {"wuhan", Point(1, 2)},
        {"beijing", Point(-1, 2)},
        pair<string, Point>("tianjin", Point(2, 3)),
        pair<string, Point>("tianjin", Point(2, 3)),
        make_pair("hainan", Point(2, 3)),
        make_pair("dongjing", Point(2, 3)),
    };
    display(number);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

