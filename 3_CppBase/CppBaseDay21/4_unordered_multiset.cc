#include <math.h>
#include <iostream>
#include <unordered_set>

using std::cout;
using std::endl;
using std::unordered_multiset;

template <typename Container>
void display(const Container &con)
{
    for(auto &elem : con)
    {
        cout << elem << "  ";
    }
    cout << endl;
}

class Point
{
public:
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
    }

    double getDistance() const
    {
        return hypot(_ix, _iy);
    }

    int getX() const
    {
        return _ix;
    }

    int getY() const
    {
        return _iy;
    }

    ~Point()
    {
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs);
private:
    int _ix;
    int _iy;
};

std::ostream &operator<<(std::ostream &os, const Point &rhs)
{
    os << "(" << rhs._ix
       << ", " << rhs._iy
       << ")";

    return os;
}

//1、hash针对Point进行的特化
namespace std
{

//模板的特化
template <>
struct hash<Point>
{
    size_t operator()(const Point &rhs) const
    {
        return (rhs.getX() << 1)^(rhs.getY() << 2);
    }
};//end of struct hash

}//end of namespace std

struct HashPoint
{
    size_t operator()(const Point &rhs) const
    {
        cout << "struct HashPoint" << endl;
        return (rhs.getX() << 1)^(rhs.getY() << 2);
    }

};


//1、针对于equal_to而言，比较两个点是不是相等的
//运算符的重载
bool operator==(const Point &lhs, const Point &rhs)
{
    return (lhs.getX() == rhs.getX()) && (lhs.getY() == rhs.getY());
}

//2、equal_to的特化形式
namespace  std
{

template <>
struct equal_to<Point>
{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        cout << "struct equal_to<Point>" << endl;
        return (lhs.getX() == rhs.getX()) && (lhs.getY() == rhs.getY());
    }
};

}//end of namespace std

//3、函数对象的形式
struct EqualToPoint
{
    bool operator()(const Point &lhs, const Point &rhs) const
    {
        cout << "struct EqualToPoint" << endl;
        return (lhs.getX() == rhs.getX()) && (lhs.getY() == rhs.getY());
    }

};

void test()
{
    unordered_multiset<Point> number = {
    /* unordered_multiset<Point, HashPoint> number = { */
    /* unordered_multiset<Point, HashPoint, EqualToPoint> number = { */
        Point(1, 2),
        Point(-1, 2),
        Point(4, 5),
        Point(1, 2),
        Point(3, 2),
        Point(1, -2),
    };
    display(number);
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

