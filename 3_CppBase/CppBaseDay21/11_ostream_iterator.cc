#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <algorithm>

using std::cout;
using std::endl;
using std::ostream_iterator;
using std::vector;
using std::list;
using std::copy;

void test0()
{
    vector<int> vec = {10, 50, 60, 12};
    ostream_iterator<int> osi(cout, "  ");
    copy(vec.begin(), vec.end(), osi);
}

void test()
{
    list<int> number = {1, 5, 6, 10};
    /* ostream_iterator<int> osi(cout, "  "); */
    copy(number.begin(), number.end(), 
         ostream_iterator<int>(cout, "  "));
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

