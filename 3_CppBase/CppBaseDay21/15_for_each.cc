#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>

using std::cout;
using std::endl;
using std::for_each;
using std::vector;
using std::ostream_iterator;
using std::copy;

void func(int &value)
{
    ++value;
    cout << value << "  ";
}

void test()
{
    vector<int> vec = {1, 5, 9, 8, 6, 4};
    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, "  "));
    cout << endl;

    cout << endl;
    for_each(vec.begin(), vec.end(), func);
    cout << endl;

    copy(vec.begin(), vec.end(), ostream_iterator<int>(cout, "  "));
    cout << endl;
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

