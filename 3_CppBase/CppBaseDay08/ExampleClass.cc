#include <iostream>
using std::cout;
using std::endl;

int num = 1;
int x = 2;

namespace wd
{

int num = 2;

class Example {
public:
    void print(int num)
    {
        //形参和数据成员同名，直接用成员名进行访问时会发生屏蔽效果
        //对变量的访问遵循就近原则
        cout << "形参num = " << num << endl;
        cout << "数据成员num = " << this->num << endl;
        cout << "数据成员num = " << Example::num << endl;
        cout << "wd::num = " << wd::num << endl;
        cout << "全局变量num = " << ::num << endl;
    }
private:
    int num = 10;
};
}

typedef void (wd::Example::*MemberFunction)(int);

void test0(){
    wd::Example ex;
    ex.print(12);

    cout << endl;
    MemberFunction mf = &wd::Example::print;
    (ex.*mf)(7);
}

int main(void){
    test0();
    return 0;
}
