#include <vector>
#include <iostream>
using std::cout;
using std::endl;
using std::vector;
using std::string;

struct Test{
    int _ix = 12;
    int _iy = 10;
};

void test0(){
    Test t1;
    Test t2;
    Test t3;
    vector<Test> num1;
    num1.push_back(t1);
    num1.push_back(t2);
    num1.push_back(t3);

    vector<Test> num2{t1,t2,t3};
    cout << num2[1]._iy << endl;
}

void test1(){
    string str("hello");
    string str2("world");
    vector<string> String{str,str2};
    for(auto & st : String){
        cout << st << " ";
    }
    cout << endl;
}

void test2(){
    vector<int> nums(5);
    vector<int> nums2{1,2,3,4,5};

    vector<vector<int>> Num{nums,nums2};
    for(auto & nu : Num){
        for(auto & ix : nu){
            cout << ix << " ";
        }
        cout << endl;
    }

    //vector<vector<int>>::iterator it
    auto it = Num.begin();
}

int main(void){
    test2();
    return 0;
}
