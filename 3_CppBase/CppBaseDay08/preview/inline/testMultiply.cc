#include "multiply.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int a = 2, b = 3;
    cout << multiply(a,b) << endl;
}

int main(void){
    test0();
    return 0;
}
