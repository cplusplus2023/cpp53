#include <iostream>
using std::cout;
using std::endl;

class Base
{
public:
    Base(int x)
    : _ix(x)
    {}


    friend Base operator+(const Base & lhs, const Base & rhs);
    friend bool operator==(const Base & lhs,int x);
private:
    int _ix;
};

Base operator+(const Base & lhs, const Base & rhs){
    return rhs._ix - lhs._ix;
}

bool operator==(const Base & lhs,int x){
    return lhs._ix == x;
}

void test0(){
    int i=2;
    int j=7;

    Base x(i);
    Base y(j);

    x + y;
    cout << (x+y == j - i) << endl;
}

int main(void){
    test0();
    return 0;
}
