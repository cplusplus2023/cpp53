#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class CharArray{
public:
    CharArray(const char * pstr)
    : _capacity(strlen(pstr) + 1)
    , _data(new char[_capacity]())
    {
        strcpy(_data,pstr);
    }

    ~CharArray(){
        if(_data){
            delete [] _data;
            _data = nullptr;
        }
    }

    char & operator[](size_t idx){
        if(idx < _capacity - 1){
            return _data[idx];
        }else{
            cout << "out of range" << endl;
            static char nullchar = '\0';
            return nullchar;
        }
    }

    void print() const{
        cout << _data << endl;
    }
private:
    size_t _capacity;
    char * _data;
};

void test0(){
    char arr[] = "hello";
    cout << arr[2] << endl;

    CharArray ca("world");
    cout << ca[0] << endl;
    ca[0] = 'W';
    ca.print();
}

int main(void){
    test0();
    return 0;
}
