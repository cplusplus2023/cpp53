#include <iostream>
using std::cout;
using std::endl;

class Complex
{
public:
    Complex(int real, int image)
    : _real(real)
    , _image(image)
    {
        cout << "Complex(int,int)" << endl;
    }

    void print() const{
        cout << _real << "+" << _image << "i" << endl;
    }

    Complex & operator+=(const Complex & rhs){
        cout << "Complex & operator+=" << endl;
        _real += rhs._real;
        _image += rhs._image;
        return *this;
    }

    //前置++的形式
    Complex& operator++(){
        cout << "Complex & operator++()" << endl;
        ++_real;
        ++_image;
        return *this;
    }

    //后置++的形式
    //参数列表中要多加一个int
    //与前置形式进行区分
    Complex operator++(int){
        cout << "Complex operator++(int)" << endl;
        Complex tmp(*this);
        ++_real;
        ++_image;
        return tmp;
    }


    friend 
    Complex operator+(const Complex & lhs, const Complex & rhs);
private:
    int _real;
    int _image;
};

Complex operator+(const Complex & lhs, const Complex & rhs){
    return Complex(lhs._real + rhs._real, lhs._image + rhs._image);
}

/* int operator+(int x, int y){ */
/*     return x - y; */
/* } */

void test0(){
    int a = 1;
    int b = 2;
    int c = a + b;
    c += a;
    //后置++返回一个临时变量
    /* cout << &(a++) << endl; */
    /* cout << &(++a) << endl; */
    cout << endl;

    Complex c1(1,2);
    Complex c2(3,4);
    Complex c3 = c1 + c2;
    c3.print();//4,6

    c3 += c1;
    c3.print();//5,8

    ++c3;
    c3.print();//6,9

    (c3++).print();
    c3.print();

}

int main(void){
    test0();
    return 0;
}
