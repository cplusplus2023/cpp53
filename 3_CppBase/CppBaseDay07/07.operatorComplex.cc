#include <limits>
#include <iostream>
using std::cout;
using std::cin;
using std::endl;
using std::ostream;
using std::istream;

void readInteger(istream & is,int & num){
    cout << "please input a int number:" << endl;
    while(is >> num, !is.eof()){
        if(is.bad()){
            cout << "istream has broken!" << endl;
            return;
        }else if(is.fail()){
            is.clear();
            is.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout << "please input a int number:" << endl;
        }else{
            break;
        }
    }
}


class Complex
{
public:
    Complex(int real, int image)
        : _real(real)
          , _image(image)
    {
        cout << "Complex(int,int)" << endl;
    }

    void print() const{
        cout << _real << "+" << _image << "i" << endl;
    }

    Complex & operator+=(const Complex & rhs){
        cout << "Complex & operator+=" << endl;
        _real += rhs._real;
        _image += rhs._image;
        return *this;
    }

    //前置++的形式
    Complex& operator++(){
        cout << "Complex & operator++()" << endl;
        ++_real;
        ++_image;
        return *this;
    }

    //后置++的形式
    //参数列表中要多加一个int
    //与前置形式进行区分
    Complex operator++(int){
        cout << "Complex operator++(int)" << endl;
        Complex tmp(*this);
        ++_real;
        ++_image;
        return tmp;
    }


    friend 
        Complex operator+(const Complex & lhs, const Complex & rhs);
    friend
    ostream & operator<<(ostream & os,const Complex & rhs);
    friend
    istream & operator>>(istream & is,Complex & rhs);
private:
    int _real;
    int _image;
};

Complex operator+(const Complex & lhs, const Complex & rhs){
    return Complex(lhs._real + rhs._real, lhs._image + rhs._image);
}


ostream & operator<<(ostream & os,const Complex & rhs){
    os << rhs._real << "+" << rhs._image << "i";
    return os;
}

istream & operator>>(istream & is,Complex & rhs){
    cout << "please input a real" << endl;
    /* is >> rhs._real; */
    readInteger(is,rhs._real);
    cout << "please input a image" << endl;
    /* is >> rhs._image; */
    readInteger(is,rhs._image);
    /* is >> rhs._real >> rhs._image; */
    return is;
}


void test0(){
    Complex c1(1,2);
    /* c1 << cout; */
    /* c1.operator<<(cout);//本质 */
    cout << c1 << endl;
    cin >> c1;
    cout << c1 << endl;
}

int main(void){
    test0();
    return 0;
}
