#include <iostream>
using std::cout;
using std::endl;

class Point;//前向声明

class Complex{
public:
    Complex(int real, int image)
    : _real(real)
    , _image(image)
    {}

    Complex(const Point & pt);

    void print() const{
        cout << _real << "+" << _image << "i" << endl;
    }
private:
    int _real;
    int _image;
};

class Point{
public:
    Point(int x, int y = 100)
    : _ix(x)
    , _iy(y)
    {}
    
    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    operator int(){
        cout << "operator int()" << endl;
        return _ix + _iy;
    }

    operator Complex(){
        cout << "operator Complex()" << endl;
        return Complex(_ix,_iy);
    }

    friend class Complex;
private:
    int _ix;
    int _iy;
};

Complex::Complex(const Point & pt)
: _real(pt._ix)
, _image(pt._iy)
{
    cout << "Complex(const Point & pt)" << endl;
}

void test0(){
    //隐式转换，将一个int型数据转换成Point类型对象
    Point pt = 1;
    pt.print();

    int a = 10;
    //将Point类型对象转换成int型数据
    a = pt;
    cout << a << endl;

#if 0
    Complex cx(1,2);
    cx.print();
    //将Point类型对象转换成Complex类对象
    cx = pt;
    cx.print();
#endif

    //通过隐式转换的方式
    Complex cx2(3,9);
    cx2.print();
    //隐式转换，将Point对象转换成Complex对象
    cx2 = pt;
    cx2.print();

}

int main(void){
    test0();
    return 0;
}
