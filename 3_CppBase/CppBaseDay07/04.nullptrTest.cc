#include <iostream>
using std::cout;
using std::endl;

class Bar{
public:
    void test0(){ cout << "Bar::test0()" << endl; }
    void test1(int x){ cout << "Bar::test1(): " << x << endl; }
    void test2(){ cout << "Bar::test2(): " << _data << endl; }

    int _data = 10;
};

void test0(){
    Bar * fp = nullptr;
    fp->test0();
    fp->test1(3);
    fp->test2();
}

int main(void){
    test0();
    return 0;
}
