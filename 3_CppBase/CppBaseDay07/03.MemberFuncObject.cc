#include <iostream>
using std::cout;
using std::endl;

class FFF
{
public:
    void print(int x){
        cout << "FFF::print:" << x << endl;
    }

    void display(int x){
        cout << "FFF::display:" << x << endl;
    }

    void test()
    {}
};

//typedef可以定义新的类型或者类型别名
//函数指针类
typedef void (*Function)(int);

//成员函数指针类
//声明MemberFunction这个类的对象是
//能够指向FFF类中成员函数的函数指针
typedef void (FFF::*MemberFunction)(int);

void print(int x){
    cout << "print:" << x << endl;
}

void display(int x){
    cout << "display:" << x << endl;
}

int main(void){
    Function f = print;
    f(19);//print(19);
    f = display;
    f(27);

    //成员函数指针指向成员函数
    //无法像普通函数指针一样使用简化写法
    //还有一个要求,指向的成员函数需要是FFF类的公有函数
    MemberFunction mf = &FFF::print;
    FFF fff;
    //.* 成员指针访问运算符形式之一
    (fff.*mf)(15);//成员函数指针的调用

    FFF * fp = new FFF();
    fp->display(16);

    mf = &FFF::display;
    //->* 成员指针访问运算符形式之二
    (fp->*mf)(65);//成员函数指针的调用

    //fp = &FFF::test;
    //
    cout << endl;
    fp = nullptr;
    (fp->*mf)(34);




    return 0;
}
