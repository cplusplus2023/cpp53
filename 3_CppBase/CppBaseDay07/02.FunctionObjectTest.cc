#include <iostream>
using std::cout;
using std::endl;

//typedef可以定义新的类型或者类型别名
typedef void(*Function)(int);

void print(int x){
    cout << "print:" << x << endl;
}

void display(int x){
    cout << "display:" << x << endl;
}

int main(void){
    void (*p)(int) = &print;
    (*p)(4);
    p = display;
    p(9);

    void (*p2)(int) = print;
    p2(10);
    p2 = display;
    p2(90);

    Function f;
    f = print;
    f(19);
    f = display;
    f(27);


    return 0;
}
