#include <iostream>
using std::cout;
using std::endl;

class FunctionObject{
public:
    //第一个括号代表重载的是()运算符（函数调用运算符）
    //第二个括号是这个运算符重载函数的参数部分
    int operator()(){
        cout << "FunctionObject operator()()" << endl;
        ++ _count;
        return 1;
    }

    int operator()(int x, int y){
        cout <<"operator()(int,int)" << endl;
        ++ _count;
        return x + y;
    }

    //函数对象相比普通函数，可以去携带状态
    int _count = 0;
};



void test0(){
    //局部静态变量，离开了这个函数也不能访问
    static int count = 0;
    cout << "tes0()" << endl;
    ++count;
}

void test0(int x){
    cout << x << endl;
}

int main(void){
    test0();//普通函数调用的方法

    //希望对象像函数一样去调用
    //使用到函数调用运算符()
    //()这个运算符需要去处理FunctionObject这个类的对象
    //所以在这个类中定义对应的运算符重载函数
    FunctionObject fo;
    //对象的函数类比用法，这类对象称为函数对象
    cout << fo() << endl;
    cout << fo.operator()() << endl;

    cout << fo(5,6) << endl;
    cout << fo.operator()(5,6) << endl;

    cout << "fo._count:" << fo._count << endl;

    test0();
    test0();
    test0();
    test0();
    /* cout << count << endl; */
    return 0;
}
