#include <iostream>
using std::cout;
using std::endl;

//函数重载
int add(int x, int y){
    return x + y;
}

void add(int x, int y, float z){
    cout << "yes" << endl;
}

int add(int x, int y, int z){
    return x + y + z;
}

int add(int x,float y){
    return x + y;
}

int add(float x, int y){
    return x + y;
}

extern "C"
{
    int add(double x,double y){
        return x + y;
    }

    int add2(int x,double y){
        return x + y;
    }
}

void test0(){

}

int main(void){
    test0();
    return 0;
}
