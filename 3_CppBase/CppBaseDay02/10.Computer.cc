#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//类名的命名遵循驼峰规则
class Computer
{
public:
    //动作:成员函数，命名遵循小驼峰规则
    void setBrand(const char * brand){
        strcpy(_brand,brand);
    }

    void setPrice(double price){
        _price = price;
    }

    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }
private:
    //属性:数据成员，命名通常以_作为开头
    int _a;
    char _brand[20];
    double _price;
};

void test0(){
    //内置类型
    int a;
    //自定义类型,类比内置类型去创建一个对象
    Computer pc;
    pc.setBrand("HuaWei");
    pc.setPrice(8800);
    pc.print();
   // pc._price = 30000;
}

void test1(){
    cout << sizeof(Computer) << endl;
}

int main(void){
    test1();
    return 0;
}
