#include <iostream>
using std::cout;
using std::endl;

double division(double a, double b){
    if(b == 0){
        throw "Division by zero";
    }

    if(a == 0){
        throw (int)a;
    }

    return a/b;
}

void test0(){
    double x = 35, y = 70;
    try{
        double z = division(x,y);
        cout << z << endl;
    }catch(const char * msg){
        cout << msg << endl;
    }catch(double x){
        cout << "double:" << x << endl;
    }catch(int x){
        cout << "int:" << x << endl;
    }

    cout << "over" << endl;
}

int main(void){
    test0();
    return 0;
}
