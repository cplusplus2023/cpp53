#include <iostream>
using std::cout;
using std::endl;

void test0(){
    char a[] = {'a','b','c','d'};
    char b[5] = "abcd";
    char c[] = {'a','b','c','d','\0'};
    int d[] = {1,2,3};
    cout << a << endl;
    cout << b << endl;
    cout << c << endl;
    cout << d << endl;
}

void test1(){
    //char * p = "hello";
    const char * p = "hello";
    cout << p << endl;

}

int main(void){
    //test0();
    test1();
    return 0;
}
