#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(int x,int y)
    : _iy(y)
    , _ix(x)
    {
        //严格来说，这是赋值操作，不是初始化
        /* _ix = x; */
        /* _iy = y; */
        cout << "Point(int,int)" << endl;
    }

    void print(){
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    Point pt2(1,2);
    pt2.print();
    cout << sizeof(Point) << endl;


}

int main(void){
    test0();
    return 0;
}
