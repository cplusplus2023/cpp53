#include <iostream>
using std::cout;
using std::endl;

int main()
{
    int a[5]={1,2,3,4,5};
    int (*p)[5] = &a;

    //输出数组首个元素的地址
    //类型为int *
    cout << a << endl;
    cout << &a[0] << endl;
    cout << a + 1 << endl;
    //输出数组的首地址
    //类型为 int (*)[5]
    cout << &a << endl;
    cout << &a + 1 << endl;



    int *ptr=(int *)(&a+1);
    printf("%d,%d",*(a+1),*(ptr-1));
}
