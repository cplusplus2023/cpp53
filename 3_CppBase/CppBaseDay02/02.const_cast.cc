#include <iostream>
using std::cout;
using std::endl;

void test0(){
    const int num = 100;
    //const_cast<int>(num) = 10;//error
    
    //const_cast对常引用、常量指针起作用
    int num2 = 20;
    const int & ref = num2;
    const_cast<int&>(ref) = 200;
    cout << num2 << endl;

    int num3 = 300;
    const int * p = &num3;
    *const_cast<int*>(p) = 3000;
    cout << num3 << endl;
}

void func(int * px){
    *px = 100;
    cout << "*px:" << *px << endl;
    cout << "px:" << px << endl;
}

void test1(){
    const int number = 10;
    //&number的类型const int *
    //func(&number);//error
    func(const_cast<int*>(&number));
    cout << number << endl;
    cout << &number << endl;
}

int main(void){
    test1();
    return 0;
}
