#include <iostream>
using std::cout;
using std::endl;

#define MULTIPLY(X,Y) ((X)*(Y))

inline
int multiply(int x, int y){
    return x * y;
}

void test0(){
    int a = 1, b = 2, c = 3;
    //宏函数的本质是字符串替换
    //如果写宏函数时忘掉了括号
    //1 + 2 * 2 + 3
    printf("%d\n",MULTIPLY(a + b, b + c));

    cout << multiply(a + b, b + c) << endl;
}

int main(void){
    test0();
    return 0;
}
