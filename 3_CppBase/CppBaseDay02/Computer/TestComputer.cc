#include "Computer.hpp"
#include <iostream>
using std::cout;
using std::endl;

extern void test1();

void test0(){
    //内置类型
    int a;
    //自定义类型,类比内置类型去创建一个对象
    Computer pc;
    pc.setBrand("HuaWei");
    pc.setPrice(8800);
    pc.print();
    // pc._price = 30000;
}

int main(void){
    test0();
    test1();
    return 0;
}
