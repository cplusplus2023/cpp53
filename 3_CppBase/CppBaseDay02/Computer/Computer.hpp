#ifndef __Computer_HPP__
#define __Computer_HPP__

#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//类名的命名遵循驼峰规则
class Computer
{
public:
    //动作:成员函数，命名遵循小驼峰规则
    void setBrand(const char * brand);//设置品牌
    void setPrice(double price);//设置价格
    void print();//打印信息
private:
    //属性:数据成员，命名通常以_作为开头
    char _brand[20];
    double _price;
};





#endif
