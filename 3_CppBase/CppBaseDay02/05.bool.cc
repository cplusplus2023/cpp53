#include <iostream>
using std::cout;
using std::endl;

void test0(){
    bool b0 = 0;
    bool b1 = 1;
    bool b2 = -1;
    bool b3 = true;
    bool b4 = false;

    cout << sizeof b0 << endl;
    cout << sizeof(bool) << endl;

    if(b2){
        cout << "true" << endl;
    }else{
        cout << "false" << endl;
    }

    cout << endl;
    cout << b0 << endl;
    cout << b1 << endl;
    cout << b2 << endl;
    cout << b3 << endl;
    cout << b4 << endl;
}

int main(void){
    test0();
    return 0;
}
