#include <iostream>
using std::cout;
using std::endl;

/* int add(int x, int y){ */
/*     return x + y; */
/* } */

int add(int x = 100, int y = 20, int z = 10){
    return x + y + z;
}

int add(int x,float y){
    return x + y;
}

int add(float x, int y){
    return x + y;
}


void test0(){
    //如果没有传入足够的参数，传入的参数和有默认值的参数可以
    //完成调用时，就可以进行缺省调用
    //如果传入了足够的参数，就使用传入的参数去进行计算
    cout << add(1,11) << endl;
    cout << add(15) << endl;
    cout << add() << endl;
}

int main(void){
    test0();
    return 0;
}
