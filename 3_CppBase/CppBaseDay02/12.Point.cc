#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(){
        cout << "Point()" << endl;
    }

    Point(int x,int y)
    {
        _ix = x;
        _iy = y;
        cout << "Point(int,int)" << endl;
    }

    Point(int x){
        _ix = x;
        _iy = 100;
        cout << "Point(int)" << endl;
    }

    void print(){
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }
private:
    int _ix;
    int _iy;
};

void test0(){
    int a;

    Point pt;
    pt.print();

    int b = 1;
    Point pt2(1,2);
    pt2.print();

    Point pt3(50);
    pt3.print();
}

int main(void){
    test0();
    return 0;
}
