#include <iostream>
using std::cout;
using std::endl;

int g_number = 100;

int & func1(){
    cout << "func1 g_number: " << g_number << endl;
    return g_number;
}

int func2(){
    cout << "func2 g_number: " << g_number << endl;
    return g_number;
}

void test0(){
    //func1的返回值是一个引用，一个绑定到g_number的引用
    cout << func1() << endl;
    cout << func2() << endl;
    cout << &func1() << endl;
    cout << &g_number << endl;

    func1() = 10000;
    cout << g_number << endl;
    //不能取地址
    //&func2();  //func2的返回值是一个从g_number复制出来的临时变量
}

/* int & func3(){ */
/*     int l_number = 10; */
/*     return l_number; */
/* } */
/* void test1(){ */
/*     cout << func3() << endl; */
/* } */

int & func4(){
    int * p = new int(1);
    return *p;
}

void test2(){
    cout << func4() << endl;
    delete &func4();
    /* &func4(); */
    int & ref = func4();
    ref = 100;
    delete &ref;
}

int main(void){
    test2();
    return 0;
}
