#include <iostream>
using std::cout;
using std::endl;

void test0(){
    //C语言中的强制转换是一个万能写法
    int * p = (int*)malloc(sizeof(int));
    *p = 1;
    free(p);
    p = nullptr;

    //C++强制转换
    //<> 中是要转换成的目标类型
    //() 中的是要转换的内容
    int * p1 = static_cast<int*>(malloc(sizeof(int)));
    *p1 = 10;
    free(p1);
    p1 = nullptr;

    const char * pstr = "hello";
    int * pint = (int*)pstr;
    cout << *pint << endl;

    int * pint2 = static_cast<int*>(pstr);

}

int main(void){
    test0();
    return 0;
}
