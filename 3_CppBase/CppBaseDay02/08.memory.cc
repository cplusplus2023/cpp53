#include <iostream>
using std::cout;
using std::endl;

int g_number = 1;

namespace wd
{
int w_number = 100;
}

void test0(){
    int l_number = 2;
    static int s_number = 3;
    const char * pstr = "world";
    int * p = new int(1);
    cout << "&l_number:" << &l_number << endl;
    cout << "p:" << p << endl;
    cout << "&s_number:" << &s_number << endl;
    cout << "&wd::w_number:" << &wd::w_number << endl;
    cout << "&g_number:" << &g_number << endl;
    /* cout << pstr << endl; */
    printf("pstr:%p\n",pstr);
    /* cout << "&test0:" << &test0 << endl; */
    printf("&test0:%p\n",&test0);

}

int main(void){
    test0();
    printf("&main:%p\n",&main);
    return 0;
}
