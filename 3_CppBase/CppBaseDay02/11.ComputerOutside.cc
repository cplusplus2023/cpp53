#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

//类名的命名遵循驼峰规则
class Computer
{
public:
    //动作:成员函数，命名遵循小驼峰规则
    void setBrand(const char * brand);//设置品牌

    void setPrice(double price);//设置价格

    void print();//打印信息
private:
    //属性:数据成员，命名通常以_作为开头
    char _brand[20];
    double _price;
};


void Computer::setBrand(const char * brand)
{
    strcpy(_brand,brand);
}

void Computer::setPrice(double price)
{
    _price = price;
}

void Computer::print()
{
    cout << "brand:" << _brand << endl;
    cout << "price:" << _price << endl;
}

void test0(){
    //内置类型
    int a;
    //自定义类型,类比内置类型去创建一个对象
    Computer pc;
    pc.setBrand("HuaWei");
    pc.setPrice(8800);
    pc.print();
    // pc._price = 30000;
}

int main(void){
    test0();
    return 0;
}
