#include <iostream>
using std::cout;
using std::endl;

class A
{
protected:
    A(int a)
    : _a(a)
    {}
public:
    virtual void display() const{
        cout << _a << endl;
    }

private:
    int _a;
};

class B
: public A
{
public:
    B(int a,int b)
    : A(a)//A类的构造函数时protected,依然可以在B类中调用
    , _b(b)
    {}

    void display() const{
        cout << _b << endl;
    }
private:
    int _b;
};

void test0(){
    B b(1,2);
    b.display();
    //A a;
    A* pa = &b;
    pa->display();
}

int main(void){
    test0();
    return 0;
}
