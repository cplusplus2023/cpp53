#include <math.h>
#include <iostream>
using std::cout;
using std::endl;
using std::string;

class Figure{
public:
    virtual string getName() const = 0;
    virtual double getArea() const = 0;
};

//等派生类实现了纯虚函数，就可以创建各自的对象
//将这些对象传给display进行调用
//基类引用绑定派生类对象
//使用这个引用去调用各自的函数实现
void display(Figure & fig){
    cout << fig.getName();
    cout << " 的面积是：";
    cout << fig.getArea() << endl;
}

class Rectangle//矩形
: public Figure
{
public:
    Rectangle(double len,double wid)
    : _length(len)
    , _width(wid)
    {}

    string getName() const override
    {
        return "矩形";
    }

    double getArea() const override
    {
        return _length * _width;
    }
private:
    double _length;
    double _width;
};

class Circle
: public Figure
{
public:
    Circle(double r)
    : _radius(r)
    {}

    string getName() const override
    {
        return "圆形";
    }

    double getArea() const override
    {
        return PI * _radius * _radius;
    }


private:
    static constexpr double PI = 3.141592653;
    double _radius;
};

class Triangle
: public Figure
{
public:
    Triangle(double a,double b,double c)
    : _a(a)
    , _b(b)
    , _c(c)
    {}

    string getName() const override
    {
        return "三角形";
    }

    double getArea() const override
    {
        //海伦公式
        double p = (_a + _b + _c)/2;
        return sqrt(p * (p -_a) * (p - _b)* (p - _c));
    }
    
private:
    double _a,_b,_c;
};


void test0(){
    Rectangle rec(10,20);
    Circle ci(10);
    Triangle tr(3,4,5);

    display(rec);
    cout << endl;
    display(ci);
    cout << endl;
    display(tr);

}

int main(void){
    test0();
    return 0;
}
