#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
    Base(long x){
        /* cout << "Base()" << endl; */
        _base = x;
    }

    virtual void display() const{
        cout << "Base::display()" << endl;
    }

    virtual void show() const{
        cout << "Base::show()" << endl;
    }

    ~Base(){
        /* cout << "~Base()" << endl; */
    }
private:
    long _base;
};


class Derived
: public Base
{
public:
    Derived(long base,long derived)
    : Base(base)//创建基类子对象
    , _derived(derived)
    {
        /* cout << "Derived()" << endl; */
    }

    //想要在派生类中定义虚函数覆盖基类的虚函数
    //很容易打错函数名字，同时又不会报错
    //没有完成有效的覆盖
    /* void dispaly() const{ */
    /* void dispaly() const override */
    void display() const override
    {
        cout << "Derived::display()" << endl;
    }

    /* virtual void show() const{ */
    /* } */

    ~Derived(){
        /* cout << "~Derived()" << endl; */
    }
private:
    long _derived;

};


void print(Base * pbase){
    pbase->display();
    pbase->show();
}

void test0(){
    Base base(10);
    Derived dd(1,2);

    print(&base);
    cout << endl;
    //用一个基类指针指向派生类对象
    //能够操纵的只有基类部分
    print(&dd);

    cout << "sizeof(Base):" << sizeof(Base) << endl;
    cout << "sizeof(Derived):" << sizeof(Derived) << endl;
}

void test1(){
    Derived d1(10,20);
    d1.display();
    d1.show();
}

void test2(){
    Derived d2(100,200);
    Base * pbase = &d2;
    pbase->display();
}

int main(void){
    test2();
    return 0;
}
