#include <iostream>
using std::cout;
using std::endl;

class Grandpa
{
public:
    Grandpa(){ cout << "Grandpa()" << endl; }
    ~Grandpa(){ cout << "~Grandpa()" << endl; }

    virtual void func1() {
        cout << "Grandpa::func1()" << endl;
    }

    virtual void func2(){
        cout << "Grandpa::func2()" << endl;
    }
};

class Parent
: public Grandpa
{
public:
    Parent(){
        cout << "Parent()" << endl;
        func1();
    }

    virtual void func1() {
        cout << "Parent::func1()" << endl;
    }

    virtual void func2(){
        cout << "Parent::func2()" << endl;
    }

    ~Parent(){
        cout << "Parent()" << endl;
        func2();
    }
};

class Son
: public Parent
{
public:
    Son() { cout << "Son()" << endl; }
    ~Son() { cout << "~Son()" << endl; }


    virtual void func1() override {
        cout << "Son::func1()" << endl;
    }

    virtual void func2() override{
        cout << "Son::func2()" << endl;
    }
};

void test1(){
    Son ss1;
}

void test0(){
    Son ss;
    Grandpa * p = &ss;
    p->func1();
    p->func2();
}



int main(void){
    test1();
    return 0;
}
