#include <iostream>
using std::cout;
using std::endl;

class Base{
public:
    Base(long x){
        /* cout << "Base()" << endl; */
        _base = x;
    }

    virtual void display() const{
        cout << "Base::display()" << endl;
    }


    void func1(){
        display();
        cout << _base << endl;
    }

    void func2(){
        Base::display();
    }

    ~Base(){
        /* cout << "~Base()" << endl; */
    }
private:
    long _base = 10;
};


class Derived
: public Base
{
public:
    Derived(long base,long derived)
    : Base(base)//创建基类子对象
    , _derived(derived)
    {
        /* cout << "Derived()" << endl; */
    }

    void display() const override{
        cout << "Derived::display()" << endl;
    }

    ~Derived(){
        /* cout << "~Derived()" << endl; */
    }
private:
    long _derived;

};



void test0(){
    Base base(10);
    Derived derived(1,2);

    base.func1();
    base.func2();

    derived.func1();
    derived.func2();

}

int main(void){
    test0();
    return 0;
}
