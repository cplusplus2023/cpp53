#include <iostream>

using namespace std;

//#define PI 3.14

class Circle
{
public:
    Circle();              // 将半径设为0
    Circle(double r);      // 创建Circle对象时将半径初始化为r
    double getArea();      // 获取圆的面积
    double getPerimeter(); // 获取圆的周长
    void show();           // 将圆的半径、周长、面积输出到屏幕
private:
    double _r;
    //static const int ppp = 3; //ok
    static constexpr double PI = 3.14;
};
//const double Circle::PI = 3.14;


Circle::Circle() {
    _r = 0;
}

Circle::Circle(double r) : _r(r) {}

double Circle::getArea() {
    return PI * _r * _r;
}

double Circle::getPerimeter() {
    return 2 * PI * _r;
}

void Circle::show() {
    cout << "圆的半径为:" << this->_r << endl;
    cout << "圆的周长为:" << getArea() << endl;
    cout << "圆的面积为:" << getPerimeter() << endl;
}

class Cylinder : public Circle {
public:
    Cylinder(double r, double h);//创建Circle对象时将半径初始化为r
    double getVolume(); //获取圆柱体的体积
    void showVolume();//将圆柱体的体积输出到屏幕
private:
    double _h;
};

Cylinder::Cylinder(double r, double h) 
: Circle(r)
, _h(h) 
{}

double Cylinder::getVolume() {
    return _h * getArea();
}

void Cylinder::showVolume() {
    cout << "圆柱体的体积为" << getVolume() << endl;
}




int main() {
    Cylinder cy1(2,10);
    cy1.show();
    cy1.showVolume();
    return 0;
}
