#include <pthread.h>
#include <iostream>
using std::cout;
using std::endl;

class Singleton
{
public:
    static Singleton * getInstance(){
        pthread_once(&_once,init_r);
        return _pInstance;
    }

    static void init_r(){
        _pInstance = new Singleton(1,2);
        atexit(destroy);
    }

    void init(int x, int y){
        _ix = x;
        _iy = y;
    }
    
    //C++11
    //能够禁止一切形式的复制
    Singleton(const Singleton & rhs) = delete;
    Singleton& operator=(const Singleton & rhs) = delete;


    void print() const{
        cout << "(" << this->_ix
            << "," << this->_iy
            << ")" << endl;
    }

private:
    Singleton(){}

    Singleton(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Singleton(int,int)" << endl;
    }

    static void destroy(){
        if(_pInstance){
            delete _pInstance;
            _pInstance = nullptr;
        }
    }
    ~Singleton(){
        cout << "~Singleton()" << endl;
    }

    int _ix;
    int _iy;
    static Singleton * _pInstance;
    static pthread_once_t _once;
};
Singleton* Singleton::_pInstance = nullptr;
pthread_once_t Singleton::_once = PTHREAD_ONCE_INIT;

void test1(){
    Singleton::getInstance()->print();
    Singleton::getInstance()->init(10,80);
    Singleton::getInstance()->print();
    /* Singleton::destroy(); */
    Singleton::getInstance()->init(10,80);
    Singleton::getInstance()->print();
}

int main(void){
    test1();
    return 0;
}
