#include "Line.hpp"
#include <iostream>
using std::cout;
using std::endl;

void test0(){
    Line line(10,20,30,40);
    line.printLine();
    cout << sizeof(Line) << endl;
}

int main(void){
    test0();
    return 0;
}
