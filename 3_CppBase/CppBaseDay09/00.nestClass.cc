#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

class Line
{
    class Point{
    public:
        Point(int x,int y)
            : _ix(x)
              , _iy(y)
        {}
    friend
    ostream & operator<<(ostream & os, const Line::Point & rhs);
    private:
        int _ix;
        int _iy;
    };
public:
    Line(int x1, int y1, int x2, int y2)
        : _pt1(x1,y1)
          , _pt2(x2,y2)
    {}
    friend
    ostream & operator<<(ostream & os, const Line::Point & rhs);
/* private: */
    Point _pt1;
    Point _pt2;
};

//需求：利用输出流运算符输出Point的信息
ostream & operator<<(ostream & os, const Line::Point & rhs){
    os << "(" << rhs._ix << "," << rhs._iy << ")" ;
    return os;
}



void test0(){
    /* cout << sizeof(Line) << endl; */
    /* Line l(1,2,3,4); */
    /* cout << sizeof(l) << endl; */

    //::后面可以跟数据成员、成员函数、成员类型
    //通常将内部类设为私有
    /* Line::Point pt(1,2); */
    /* cout << pt << endl; */

    /* cout << l._pt1 << endl; */
    //无法实现对l的私有成员的访问
    //因为成员访问运算符无法重载
    Line l(1,2,3,4);
    cout << l._pt1 << endl;

}

int main(void){
    test0();
    return 0;
}
