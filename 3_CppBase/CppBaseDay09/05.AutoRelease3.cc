#include <iostream>
using std::cout;
using std::endl;

class Singleton
{
public:
    static Singleton * getInstance(){
        //当多个线程同时进入if语句时，会造成单例对象被创建出多个
        //但是最终只有一个地址值会保存到_pInstance
        //因此造成内存泄漏
        if(_pInstance == nullptr){
            atexit(destroy);
            _pInstance = new Singleton(1,2);
        }
        return _pInstance;
    }

    void init(int x, int y){
        _ix = x;
        _iy = y;
    }
    
    //C++11
    //能够禁止一切形式的复制
    Singleton(const Singleton & rhs) = delete;
    Singleton& operator=(const Singleton & rhs) = delete;

    static void destroy(){
        if(_pInstance){
            delete _pInstance;
            _pInstance = nullptr;
        }
    }

    void print() const{
        cout << "(" << this->_ix
            << "," << this->_iy
            << ")" << endl;
    }

private:
    Singleton(){}

    Singleton(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Singleton(int,int)" << endl;
    }

    ~Singleton(){
        cout << "~Singleton()" << endl;
    }

    int _ix;
    int _iy;
    static Singleton * _pInstance;
};
//饱汉式（懒汉式）—— 懒加载，不使用到该对象，就不会创建
/* Singleton* Singleton::_pInstance = nullptr; */

//进入main函数之前，就已经创建完毕
//之后无论getInstance被调用多少次，都不会再创建新的对象
//饿汉式 —— 最开始就创建（即使不使用这个单例对象）
Singleton* Singleton::_pInstance = getInstance();


void test1(){
    Singleton::getInstance()->print();
    Singleton::getInstance()->init(10,80);
    Singleton::getInstance()->print();
    Singleton::destroy();
}

int main(void){
    test1();
    return 0;
}
