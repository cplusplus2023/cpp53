#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

class Line
{
public:
    class Point{
    public:
        Point(int x,int y)
            : _ix(x)
              , _iy(y)
        {}

        void getLine(const Line & rhs){
            cout << rhs._pt1 << endl;
            cout << rhs._iw << endl;
            cout << Line::_iw << endl;
            cout << _iw << endl;
        }

    friend
    ostream & operator<<(ostream & os, const Line::Point & rhs);
    friend class Line;
    private:
        int _ix;
        int _iy;
        static int _iz;
    };
public:
    Line(int x1, int y1, int x2, int y2)
        : _pt1(x1,y1)
          , _pt2(x2,y2)
    {}

    void getPoint(const Point & rhs){
        cout << rhs._ix << endl;
        cout << rhs._iz << endl;
        cout << Point::_iz << endl;
        //cout << _iz << endl;//error
    }

private:
    Point _pt1;
    Point _pt2;
    static int _iw;
};
int Line::Point::_iz = 100;
int Line::_iw = 10;


ostream & operator<<(ostream & os, const Line::Point & rhs){
    os << "(" << rhs._ix << "," << rhs._iy << ")" ;
    return os;
}


void test0(){
    Line l(1,2,3,4);
    Line::Point pt(5,6);
    pt.getLine(l);

}

int main(void){
    test0();
    return 0;
}
