#include <iostream>
using std::cout;
using std::endl;

class Singleton
{
public:
    static Singleton * getInstance(){
        if(_pInstance == nullptr){
            _pInstance = new Singleton(1,2);
        }
        return _pInstance;
    }

    void init(int x, int y){
        _ix = x;
        _iy = y;
    }
    
    //C++11
    //能够禁止一切形式的复制
    Singleton(const Singleton & rhs) = delete;
    Singleton& operator=(const Singleton & rhs) = delete;

    /* static void destroy(){ */
    /*     if(_pInstance){ */
    /*         delete _pInstance; */
    /*         _pInstance = nullptr; */
    /*     } */
    /* } */

    void print() const{
        cout << "(" << this->_ix
            << "," << this->_iy
            << ")" << endl;
    }

    friend class AutoRelease;
private:
    Singleton(){}

    Singleton(int x,int y)
    : _ix(x)
    , _iy(y)
    {
        cout << "Singleton(int,int)" << endl;
    }

    ~Singleton(){
        cout << "~Singleton()" << endl;
    }

    int _ix;
    int _iy;
    static Singleton * _pInstance;
};
Singleton* Singleton::_pInstance = nullptr;

class AutoRelease{
public:
    AutoRelease(Singleton * p)
    : _p(p)
    {
        cout << "AutoRelease(Singleton*)" << endl;
    }

    ~AutoRelease(){
        cout << "~AutoRelease()" << endl;
        if(_p){
            delete _p;
            _p = nullptr;
        }
    }
private:
    Singleton * _p;
};

void test1(){
    AutoRelease ar(Singleton::getInstance());
    Singleton::getInstance()->print();
    Singleton::getInstance()->init(10,80);
    Singleton::getInstance()->print();
    /* Singleton::destroy(); */
}

int main(void){
    test1();
    return 0;
}
