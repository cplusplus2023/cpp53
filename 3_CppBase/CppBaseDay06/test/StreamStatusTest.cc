#include <limits>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::string;

void printStreamStatus(std::istream & is){
    cout << "is's goodbit:" << is.good() << endl;
    cout << "is's badbit:" << is.bad() << endl;
    cout << "is's failbit:" << is.fail() << endl;
    cout << "is's eofbit:" << is.eof() << endl;
}

void test0(){
    printStreamStatus(cin);
    int num = 0;
    cin >> num;
    cout << "num:" << num << endl;
    printStreamStatus(cin);

#if 1
    if(!cin){
        //恢复流的状态
        cin.clear();
        //清空缓冲区，才能继续使用
        //ignore的第一个参数count表示
        //最多丢弃掉缓冲区中的count个字符
        //直到遇到分隔符 '\n'，就停止舍弃
        cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
        cout << endl;
        printStreamStatus(cin);
    }
#endif
    string line;
    cin >> line;
    cout << "line:" << line << endl;
}

void test1(){
    int num = 0;
    string str;
    //cin >> num这个表达式的返回值是cin对象
    //输入流运算符默认以换行符、空格符为分隔符
    (cin >> num) >> str;

    cout << num << endl;
    cout << str << endl;
}

int main(void){
    test1();
    return 0;
}
