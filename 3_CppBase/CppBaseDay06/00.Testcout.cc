#include <unistd.h>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;

void test0(){
    //cout的缓冲区可以存放1024个字符
    for(int i = 0; i < 1025; ++i){
        cout << 'a' << endl; 
    }
    sleep(2);
}

void test1(){
    //当程序本身的错误信息需要输出，建议使用cerr
    cerr << "hello,world!";
    /* sleep(3); */
    cout << "hello,world!\n";
    sleep(2);
}

//main函数执行完时会刷新缓冲区
int main(void){
    test1();
    return 0;
}
