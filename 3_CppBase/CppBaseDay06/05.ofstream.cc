#include <string.h>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::ofstream;
using std::string;
using std::cerr;

void test0(){
    //当文件不存在时，创建新文件
    //当文件存在时，会清空文件的内容，重新写入新的内容
    /* string filename = "try.txt"; */
    /* ofstream ofs(filename); */

    
    string filename = "try.txt";
    //每次写入前寻位到流结尾
    ofstream ofs(filename,std::ios::app);

    if(!ofs){
        cerr << "ofs open file fail!";
        return;
    }

    string line("\nthis is a new line!");
    ofs << line;

    char buff[100] = "hello,world!";
    ofs.write(buff,strlen(buff));

    ofs.close();

}

int main(void){
    test0();
    return 0;
}
