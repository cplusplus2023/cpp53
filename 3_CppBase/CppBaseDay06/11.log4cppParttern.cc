#include <iostream>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/Priority.hh>
#include <log4cpp/Category.hh>
using std::cout;
using std::endl;
using namespace log4cpp;

void test0(){
    //1.设置日志布局
    PatternLayout * ptn1 = new PatternLayout();
    ptn1->setConversionPattern("%d %c [%p] %m%n");

    //2.创建目的地对象
    OstreamAppender * pos = new OstreamAppender("console",&cout);
    //目的地对象绑定布局
    pos->setLayout(ptn1);

    //3.创建日志记录器（模块、来源）
    /* Category & root = Category::getRoot(); */
    /* Category & salesDepart = Category::getInstance("salesDepart"); */
    Category & salesDepart = Category::getRoot().getInstance("salesDepart");
    //3.1 设置模块本身的优先级
    salesDepart.setPriority(Priority::ERROR);
    //3.2 添加日志目的地
    salesDepart.addAppender(pos);

    //4.记录日志
    salesDepart.emerg("this is an emerg msg");
    salesDepart.fatal("this is a fatal msg");
    salesDepart.alert("this is an alert msg");
    salesDepart.error("this is an error msg");
    salesDepart.warn("this is a warn msg");
    salesDepart.notice("this is a notice msg");
    salesDepart.info("this is a info msg");
    salesDepart.debug("this is a debug msg");

    //5.日志系统退出时，调用shutdown回收资源
    Category::shutdown();

}

int main(void){
    test0();
    return 0;
}
