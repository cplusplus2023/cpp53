#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;

void test0(){
    ifstream ifs;
    //无参构造一个ifstream对象，需要使用open函数
    //打开一个文件，将流对象与文件进行绑定
    //后续操纵这个流对象，就是操作这个文件
    //文件输入流对象需要绑定一个存在的文件
    ifs.open("explicit.cc");

    if(!ifs.good()){
        cerr << "ifs open file fail!" << endl;
        return;
    }

    string word;
    while(ifs >> word){
        cout << word << endl;
    }

    //关闭流
    ifs.close();
}

int main(void){
    test0();
    return 0;
}
