#include <iostream>
#include <log4cpp/OstreamAppender.hh>
#include <log4cpp/FileAppender.hh>
#include <log4cpp/RollingFileAppender.hh>
#include <log4cpp/PatternLayout.hh>
#include <log4cpp/Priority.hh>
#include <log4cpp/Category.hh>
using std::cout;
using std::endl;
using namespace log4cpp;

void test0(){
    //1.设置日志布局
    PatternLayout * ptn1 = new PatternLayout();
    ptn1->setConversionPattern("%d %c [%p] %m%n");

    PatternLayout * ptn2 = new PatternLayout();
    ptn2->setConversionPattern("%d %c [%p] %m%n");

    PatternLayout * ptn3 = new PatternLayout();
    ptn3->setConversionPattern("%d %c [%p] %m%n");

    //2.创建目的地对象
    OstreamAppender * pos = new OstreamAppender("console",&cout);
    //目的地对象绑定布局
    pos->setLayout(ptn1);

    FileAppender * fileApp = new FileAppender("fileApp","wd.log");
    fileApp->setLayout(ptn2);

    //设置单个文件最大的存储空间为5kb,
    //备份个数为9个，总共有10个文件去存储
    auto * rollingfileApp = new RollingFileAppender(
                                                    "rollingfile",
                                                    "rollingwd.log",
                                                     5 * 1024,
                                                     9
                                                                 );
    rollingfileApp->setLayout(ptn3);

    //3.创建日志记录器（模块、来源）
    /* Category & root = Category::getRoot(); */
    /* Category & salesDepart = Category::getInstance("salesDepart"); */
    Category & salesDepart = Category::getRoot().getInstance("salesDepart");
    //3.1 设置模块本身的优先级
    salesDepart.setPriority(Priority::DEBUG);
    //3.2 添加日志目的地
    salesDepart.addAppender(pos);
    salesDepart.addAppender(fileApp);
    salesDepart.addAppender(rollingfileApp);

    //4.记录日志
    int count = 100;
    while(count-- > 0){
    salesDepart.emerg("this is an emerg msg");
    salesDepart.fatal("this is a fatal msg");
    salesDepart.alert("this is an alert msg");
    salesDepart.error("this is an error msg");
    salesDepart.warn("this is a warn msg");
    salesDepart.notice("this is a notice msg");
    salesDepart.info("this is a info msg");
    salesDepart.debug("this is a debug msg");
    }
    //5.日志系统退出时，调用shutdown回收资源
    Category::shutdown();

}

int main(void){
    test0();
    return 0;
}
