#include <iostream>
using std::cout;
using std::endl;

class Complex
{
public:
    Complex(int real, int image)
    : _real(real)
    , _image(image)
    {
        cout << "Complex(int,int)" << endl;
    }

    void print() const{
        cout << _real << "+" << _image << "i" << endl;
    }
    
    Complex operator+(const Complex & rhs){
        cout << "Complex operator+" << endl;
        return Complex(_real + rhs._real,
                       _image + rhs._image);
    }

private:
    int _real;
    int _image;
};


void test0(){
    int a = 1;
    int b = 2;
    int c = a + b;

    Complex c1(1,2);
    Complex c2(3,4);
    Complex c3 = c1 + c2;
    c3.print();
}

int main(void){
    test0();
    return 0;
}
