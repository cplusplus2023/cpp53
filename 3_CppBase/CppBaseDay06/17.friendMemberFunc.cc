#include <math.h>
#include <iostream>
using std::cout;
using std::endl;

//前向声明
class Point;

class Line{
public:
float distance(const Point & lhs, const Point & rhs);

void setPoint(Point & pt, int ix, int iy);
};


class Point{
public:
    Point(int x, int y)
    : _ix(x)
    , _iy(y)
    {}

    friend float Line::distance(const Point & lhs, const Point & rhs);
    friend void Line::setPoint(Point & pt, int ix, int iy);
private:
    int _ix;
    int _iy;
};


float Line::distance(const Point & lhs, const Point & rhs){
    return sqrt((lhs._ix - rhs._ix)*(lhs._ix - rhs._ix) +
                (lhs._iy - rhs._iy)*(lhs._iy - rhs._iy));
}

void Line::setPoint(Point & pt, int ix, int iy){
    pt._ix = ix;
    pt._iy = iy;
}




void test0(){
    Point pt(0,0);
    Point pt2(3,4);
    Line l;
    cout << l.distance(pt,pt2) << endl;
}

int main(void){
    test0();
    return 0;
}
