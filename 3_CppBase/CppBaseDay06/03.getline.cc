#include <string.h>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;

void test0(){
    ifstream ifs("explicit.cc");

    if(!ifs.good()){
        cerr << "ifs open file fail!" << endl;
        return;
    }

#if 0
    //方法一：兼容C的写法
    //使用较少
    char buff[100] = {0};
    while(ifs.getline(buff,sizeof(buff))){
        cout << buff << endl;
        memset(buff,0,sizeof(buff));
    }
#endif

    //方法二
    string line;
    while(std::getline(ifs,line)){
        cout << line << endl;
    }

    //关闭流
    ifs.close();
}

int main(void){
    test0();
    return 0;
}
