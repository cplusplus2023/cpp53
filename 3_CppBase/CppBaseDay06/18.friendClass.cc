#include <math.h>
#include <iostream>
using std::cout;
using std::endl;
/* class Line; */

class Point{
public:
    Point(int x, int y)
    : _ix(x)
    , _iy(y)
    {}

    friend class Line;
    /* void setZ(Line & l, int z); */
private:
    int _ix;
    int _iy;
};


class Line{
public:
    float distance(const Point & lhs, const Point & rhs){
    return sqrt((lhs._ix - rhs._ix)*(lhs._ix - rhs._ix) +
                (lhs._iy - rhs._iy)*(lhs._iy - rhs._iy));
}

void setPoint(Point & pt, int ix, int iy){
    pt._ix = ix;
    pt._iy = iy;
    }
/* private: */
/* int _iz; */
};

/* void Point::setZ(Line & l,int z) */
/* { */
/*     l._iz = z; */
/* } */

void test0(){
    Point pt(0,0);
    Point pt2(3,4);
    Line l;
    cout << l.distance(pt,pt2) << endl;
}

int main(void){
    test0();
    return 0;
}
