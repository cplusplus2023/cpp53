#include <sstream>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::istringstream;

//以后如果以自定义类型的对象作为函数参数
//那么首先考虑 const 类名 & 形参名 的形式
//如果这种形式不符合要求，再去修改
void readConfig(const string & filename){
    ifstream ifs(filename);
    if(!ifs.good()){
        cout << "open file fail!" << endl;
        return;
    }

    string line;
    string key, value;
    //按行从ifs中读取内容，用line保存
    //getline(ifs,line)的返回值还是ifs
    //只要是goodbit状态，循环进行
    //读取完会进入eofbit状态，循环结束
    while(getline(ifs,line)){
        //利用字符串输入流的缓冲区存放line的内容（一行的内容）
        istringstream iss(line);
        //利用输入流运算符以空格符、换行符、制表符为分隔符的特性
        iss >> key >> value;
        cout << key << " -----> " << value << endl; 
    }
}



void test0(){
    readConfig("myserver.conf");
}

int main(void){
    test0();
    return 0;
}
