#include <iostream>
using std::cout;
using std::endl;

class Point{
public:
    //给隐式转换需要用到的构造函数前
    //加上explicit关键字，可以禁止隐式转换
    explicit
    Point(int ix = 0, int iy = 0)
    : _ix(ix)
    , _iy(iy)
    {
        cout << "Point(int,int)" << endl;
    }

    void print() const{
        cout << "(" << _ix
            << "," << _iy
            << ")" << endl;
    }

    ~Point(){
        cout << "~Point()" << endl;
    }

private:
    int _ix;
    int _iy;
};

void test0(){
    /* Point pt(1,2); */
    /* Point pt2(3); */
    /* pt2.print(); */

    /* Point pt3; */

    //发生了隐式转换
    //创建pt4时，赋值号的右操作数是一个int数据
    //会尝试使用10去创建一个Point对象
    //如果创建成功，就将这个对象的内容复制给pt4
    Point pt4 = 10;//Point pt4 = Point(10);
    pt4.print();
}

int main(void){
    test0();
    return 0;
}
