#include <string.h>
#include <fstream>
#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ifstream;
using std::string;

void test0(){
    string filename = "explicit.cc";
    //(1)
    /* //创建好文件输入流对象，游标默认在开头位置 */
    /* ifstream ifs(filename); */

    //(2)
    //ate模式能在打开文件时，直接定位到文件的末尾
    ifstream ifs(filename,std::ios::ate);

    if(!ifs){
        cerr << "ifs open file fail!";
        return;
    }

    //(1)
    //读取一个文件的所有内容
    //先要获取文件的大小
    //将游标放到了文件的最后
    /* ifs.seekg(0,std::ios::end); */
    long length = ifs.tellg();
    cout << length << endl;

    char * pdata = new char[length + 1]();
    //需要将游标再放置到文件开头
    //ifs.seekg(0);
    ifs.seekg(0,std::ios::beg);
    ifs.read(pdata,length);

    //content包含了文件的所有内容，包括空格、换行
    string content(pdata);
    cout << "content:" << content << endl;
    /* cout << pdata << endl; */

    ifs.close();

}

int main(void){
    test0();
    return 0;
}
