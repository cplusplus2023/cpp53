#include <iostream>
using std::cout;
using std::endl;

template <class T1,class T2,class T3>
T1 add(T2 t1,T3 t2)
{
    return t1 + t2;
}

#if 1
template <>
//指定返回类型double，函数参数类型int和float
double add<double,int,float>(int x1,float x2){
    cout << "double = int + float" << endl;
    return x1 + x2;
}

template <>
//指定返回类型float，第一个函数参数类型是int，第二个根据推导
float add<float,int>(int x1,float x2){
    cout << "float  = int + float" << endl;
    return x1 + x2;
}

template <>
//指定返回类型double，函数参数类型都自己推导
double add<double>(double x1,double x2){
    cout << "double = double + double" << endl;
    return x1 + x2;
}
#endif

void test0(){
    int x1 = 16;
    float x2 = 3.4;
    double x3 = 6.8;
    cout << add<double>(x1,x2) << endl;
    cout << add<float>(x1,x2) << endl;
    cout << add<double>(x3,x3) << endl;
}

int main(void){
    test0();
    return 0;
}
