#include <iostream>
using std::cout;
using std::endl;

template <class T>
class Outside
{
public:
    template <class R>
        class Inside
        {
        public:
            Inside(R x)
                : _r(x)
            {}

            void display() const;
        private:
            R _r;
        };

    Outside(T x)
        : _t(x)
    {}

    void display() const{
        cout <<"Outside:";
        _t.display();
    }
private:
    Inside<T> _t;
};

template <class T>
template <class R>
void Outside<T>::Inside<R>::display() const{
    cout << "Outsied::Inside::_r: " << _r << endl;
}


void test0(){
    Outside<int>::Inside<double> obin(3.5);
    obin.display();

    Outside<int> obout(2);
    obout.display();
}

int main(void){
    test0();
    return 0;
}
