#include <iostream>
using std::cout;
using std::endl;


//定义一个函数，可能有很多个不同类型的参数
//不适合一一写出，所以提供了这种方式
//那么如果参数类型没有匹配，也可能造成问题，
//所以引入可变参数模板
/* void display(int x,double y,size_t z,float p,int * p1, ...){ */

/* } */

template <class ...Args>//Args 模板参数包
void display(Args ...args)//args 函数参数包
{
    //输出模板参数包中类型参数个数
    cout << "sizeof...(Args) = " << sizeof...(Args) << endl;
    //输出函数参数包中参数的个数
    cout << "sizeof...(args) = " << sizeof...(args) << endl;
}

void test0(){
    display();
    display(1,"hello",3.3,true);
}

//递归的出口
void print(){
    cout << endl;
}

void print(int x){
    cout << x << endl;
}

//重新定义一个可变参数模板，至少得有一个参数
template <class T,class ...Args>
void print(T x, Args ...args)
{
    cout << typeid(x).name() << " ";
    print(args...);
}

void test1(){
    //调用普通函数
    //不会调用函数模板，因为函数模板至少有一个参数
    print();

    //cout << 2.3 << " ";
    //cout << endl;
    print(2.3);

    //cout << 1 << " ";
    //print("hello",3.6,true);
    //  cout << "hello" << " ";
    //  print(3.6,true);
    //    ...
    print(1,"hello",3.6,true);


    //在剩下一个参数时结束递归
    print(1,"hello",3.6,true,100);
}

int main(void){
    test1();
    return 0;
}
