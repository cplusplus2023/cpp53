#include <string.h>
#include <iostream>
using std::cout;
using std::endl;
using std::string;


//函数模板与普通函数重载
template <class T>
T add(T t1, T t2)
{
    return t1 + t2;
}

//普通函数重载可以解决问题
/* const char * add(const char * p1,const char * p2){ */
/*     //先开空间 */
/*     char * ptmp = new char[strlen(p1) + strlen(p2) + 1](); */
/*     strcpy(ptmp,p1); */
/*     strcat(ptmp,p2); */
/*     return ptmp; */
/* } */

//特化模板
//这里就是告诉编译器这里是一个模板
template <>
const char * add<const char *>(const char * p1,const char * p2){
    //先开空间
    char * ptmp = new char[strlen(p1) + strlen(p2) + 1]();
    strcpy(ptmp,p1);
    strcat(ptmp,p2);
    return ptmp;
}


void test1(){
    double d1 = 1.1, d2 = 2.2;
    cout << "add(d1,d2):" << add(d1,d2) << endl;

    const char * pstr1 = "hello";
    const char * pstr2 = ",world";
    cout << add(pstr1,pstr2) << endl;
}

int main(void){
    test1();
    return 0;
}
