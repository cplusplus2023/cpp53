#include "add.h"
#include <iostream>
using std::cout;
using std::endl;

template <class T>
T add(T t1, T t2)
{
    return t1 + t2;
}

//在这个文件中如果只是写出了函数模板的实现
//并没有调用的话，就不会实例化出模板函数

/* void test1(){ */
/*     cout << add(1,2) << endl; */
/* } */


