#include <iostream>
using std::cout;
using std::endl;

template <class T1,class T2,class T3>
T1 add(T2 t1, T3 t2)
{
    return t1 + t2;
}

/* template <class T1> */
/* T1 add(T1 t1, T1 t2) */
/* { */
/*     return t1 + t2; */
/* } */


void test0(){
    int x = 8;
    float y = 23.6;
    //指定了返回类型是double，函数参数的类型是推导出的int和float
    cout << "add(x,y):" << add<double>(x,y) << endl;
}

void test1(){
    float x = 8.3;
    float y = 3.8;
    //指定了返回类型是int型，函数参数的类型推导出是两个float
    //正常计算，最后返回时只保留了整数，得到12
    cout << "add(x,y):" << add<int>(x,y) << endl;
}

void test2(){
    double x = 9.4;
    double y = 7.7;
    //三个模板参数推导出相同的类型，没有问题
    cout << "add(x,y):" << add<double>(x,y) << endl;
}

void test3(){
    int x = 8;
    float y = 23.6;
    //在调用时指定了返回类型double
    //两个函数参数类型都是int
    //第二个函数参数会先进行类型转换再计算
    //得到结果31
    cout << add<double,int,int>(x,y) << endl;
}

void test4(){
    float x = 8.3;
    float y = 3.8;
    //先将y进行类型转化
    //计算的结果为11.3，
    //因为返回类型指定为int型
    //最后返回11
    cout << add<int,float,int>(x,y) << endl;
}


int main(void){
    test0();
    test1();
    test2();
    test3();
    test4();
    return 0;
}
