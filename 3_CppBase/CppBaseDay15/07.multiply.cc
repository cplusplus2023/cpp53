#include <iostream>
using std::cout;
using std::endl;

template <class T = int,int kBase = 10>
T multiply(T x, T y){
    return x * y * kBase;
}

void test0(){
    int i1 = 3,i2 = 4;
    cout << multiply<int,10>(i1,i2) << endl;
    cout << multiply<int>(i1,i2) << endl;
    cout << multiply(i1,i2) << endl;

    cout << endl;
    double d1 = 1.1, d2 = 2.2;
    cout << multiply(d1,d2) << endl;
    cout << multiply<int>(d1,d2) << endl;
}

int main(void){
    test0();
    return 0;
}
