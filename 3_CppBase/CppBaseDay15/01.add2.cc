#include <iostream>
using std::cout;
using std::endl;
using std::string;


//函数模板与普通函数重载
template <class T1, class T2>
T1 add(T1 t1, T2 t2)
{
    return t1 + t2;
}

short add(short s1, short s2){
    cout << "add(short,short)" << endl;
    return s1 + s2;
}

void test1(){
    short s1 = 1, s2 = 2;
    cout << add(s1,s2) << endl;
}

int main(void){
    test1();
    return 0;
}
