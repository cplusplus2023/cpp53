#include <iostream>
using std::cout;
using std::endl;

template <class T1 = int,class T2 = int,int kBase = 10>
T1 multiply(T2 x,T2 y){
    return x * y * kBase;
}


void test0(){
    double d1 = 1.1, d2 = 2.2;
    //函数的返回类型是按照模板中的类型默认值int
    //函数的参数类型是推导出的两个double
    //得到结果24
    cout << multiply(d1,d2) << endl;
    //
    //得到结果48.4
    cout << multiply<double,double,20>(d1,d2) << endl;
}

int main(void){
    test0();
    return 0;
}
