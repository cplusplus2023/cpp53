#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int a = 10;
    int b = 20;
    int * pflag = &a;
    &a;//左值
    &b;//左值
    //&(a + b);//error,右值
    &pflag;//左值，指针变量的地址
    &*pflag;//左值

    //&10;//右值，字面值常量
    const char * p = "hello";
    &p;//左值
    &"hello";//左值


    int & ref = a;
    //非const引用不能绑定右值
    //int & ref2 = 1;
    //const引用既能绑定左值，又能绑定右值
    const int & ref2 = 1;
    const int & ref3 = a;

    //右值引用只能绑定右值
    int && r_ref = 10;//绑定右值ok
    //int && r_ref2 = a;//绑定左值 error
    &r_ref;//ok，左值

}

int main(void){
    test0(); 
    return 0;
}
