#include <iostream>
using std::cout;
using std::endl;

template <class T>
T add(T t1, T t2)
{
    cout << "通用模板T" << endl;
    return t1 + t2;
}

template <class T1,class T2>
T1 add(T2 t1, T2 t2)
{
    cout << "通用模板T1,T2" << endl;
    return t1 + t2;
}


template <class T1,class T2,class T3>
T1 add(T2 t1, T3 t2)
{
    cout << "通用模板T1,T2,T3" << endl;
    return t1 + t2;
}


void test0(){
    float x = 3.5;
    double y = 7.7;
    cout << add<double>(x,y) << endl;
}

int main(void){
    test0();
    return 0;
}
