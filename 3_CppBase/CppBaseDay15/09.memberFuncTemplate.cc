#include <iostream>
using std::cout;
using std::endl;

class Point
{
public:
    Point(double x,double y)
    : _x(x)
    , _y(y)
    {}

    //定义一个成员函数模板
    //将_x转换成目标类型
    template <class T = int>
    T convert()
    {
        return (T)_x;
    }
private:
    double _x;
    double _y;
};


void test0(){
    Point pt(1.1,2.2);
    cout << pt.convert<int>() << endl;
    cout << pt.convert() << endl;
}

int main(void){
    test0();
    return 0;
}
