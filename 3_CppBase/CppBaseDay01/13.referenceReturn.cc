#include <iostream>
using std::cout;
using std::endl;

int g_number = 100;

int & func1(){
    cout << "func1 g_number: " << g_number << endl;
    return g_number;
}

int func2(){
    cout << "func2 g_number: " << g_number << endl;
    return g_number;
}

void test0(){
    cout << func1() << endl;
    cout << func2() << endl;
    cout << &func1() << endl;
    cout << &g_number << endl;

    func1() = 10000;
    cout << g_number << endl;
    //不能取地址
    //&func2();  //func2的返回值是一个从g_number复制出来的临时变量

}

int main(void){
    test0();
    return 0;
}
