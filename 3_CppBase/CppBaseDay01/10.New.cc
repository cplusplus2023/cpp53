#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int * p = (int*)malloc(sizeof(int));
    *p = 100;
    free(p);

    int * p1 = new int();
    cout << *p1 << endl;
    *p1 = 200;
    cout << *p1 << endl;
    delete p1;//回收p1指向的地址上的内容
    p1 = nullptr;//安全回收,使用完之后将指针设为空指针

    int * p2 = new int(100);
    cout << *p2 << endl;
    delete p2;
    p2 = nullptr;

    //new语句申请数组空间，建议最后加上小括号
    //表示申请空间并进行了初始化
    int * p3 = new int[5]();
    for(int i = 0; i < 5; ++i){
        cout << p3[i] << " ";
    }
    cout << endl;
    delete [] p3;
    p3 = nullptr;

}

int main(void){
    test0();
    return 0;
}
