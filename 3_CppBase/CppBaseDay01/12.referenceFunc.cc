#include <iostream>
using std::cout;
using std::endl;

void swap(int x, int y){//值传递
    int tmp = x;
    x = y;
    y = tmp;
}

void swap(int * px, int * py){//地址传递
    int tmp = *px;
    *px = *py;
    *py = tmp;
}

void swap2(int & x, int & y){//引用传递
    int tmp = x;
    x = y;
    y = tmp;
}


void test0(){
    int a = 1, b = 2;
    cout << a << "  -----  "  << b << endl;
    swap2(a,b);
    cout << a << "  -----  "  << b << endl;

}

void test1(){
    int num = 100;
    const int & ref = num;
    //ref = 200;
    num = 300;
    cout << ref << endl;
}


int main(void){
    test1();
    return 0;
}
