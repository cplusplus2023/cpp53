#include <iostream>
using std::cout;
using std::endl;
#define MAX "hello"

void test0(){
    cout << MAX << endl;
    const int max = 100;
    /* max -= 1; */
    /* const int max = "hello"; */
    cout << max << endl;
}

void test1(){
    int number = 7;
    int number2 = 9;
    //const在*前面，表示常量指针
    //指向一个常量的指针
    const int * p1 = &number;
    //*p1 = 5;//error，不能修改指向的值
    p1 = &number2;//ok 可以修改指向
    cout << *p1 << endl;
}

void test2(){
    int number = 7;
    int number2 = 9;
    //const在*后面，表示指针常量
    //在指针的层面是只读的，也就是不可修改指向
    //这个指针是一个常量
    int * const p1 = &number;
    *p1 = 5;//ok，可以修改指向的值
    //p1 = &number2;//error 不能修改指向
    cout << *p1 << endl;
}

void test3(){//数组指针，是一个指向数组的指针
    int arr[5] = {1,2,3,4,5};
    //p的类型是int(*)[5]
    //这里的5也不能省略，定义指针p时明确这是
    //一个指向存放5个int型元素的数组的指针
    int (*p)[5] = &arr;
    for(int i = 0; i < 5; ++i){
        cout << (*p)[i] << endl;
    }
}

void test4(){//指针数组，是一个数组，存放的元素是指针
    int num = 7, num2 = 8, num3 = 9;
    int * p1 = &num;
    int * p2 = &num2;
    int * p3 = &num3;
    int* arr[3] = {p1,p2,p3};
    for(int i = 0; i < 3; ++i){
        cout << *arr[i] << endl;
    }
}

int add(int x, int y){
    return x + y;
}

//函数指针，本质是指针，指向一个函数
int (*p)(int,int) = &add;

void test5(){
    cout << (*p)(60,30) << endl;
}

int Num = 70;
int* f(){//指针函数,返回类型为指针的函数
    int * p = &Num;
    return p;
}

void test6(){
    cout << *f() << endl;
}


int main(void){
    /* test0(); */
    test6();
    return 0;
}
