#include <iostream>
using std::cout;
using std::endl;

namespace wd
{
int num = 1;

void print(){ cout << "hello" << endl;}
}


namespace wd
{
int num2 = 2;
/* cout << num2 << endl; */
/* print(); */
int num3;
}

void test0(){
    cout << wd::num << endl;
    cout << wd::num2 << endl;
    wd::num3 = 100;
    wd::print();
}

int main(void){
    test0();
    return 0;
}
