#include <iostream>
using std::cout;
using std::endl;

void test0(){
    int number = 1;
    //引用一经绑定，就不能再修改绑定
    int & ref = number;//引用的初始化，就是绑定实体
    //int & ref2;//error 引用不能单独存在
    
    cout << "&number: " << &number << endl;
    cout << "&ref: " << &ref << endl;

    number = 2;
    cout << ref << endl;

    ref = 3;
    cout << number << endl;

    cout << endl;
    int number2 = 100;
    ref = number2;//这里不是更改了绑定，而是修改了值
    cout << "&number2: " << &number2 << endl;
    cout << "&ref: " << &ref << endl;
    cout << ref << endl;
    cout << number << endl;

}

int main(void){
    test0();
    return 0;
}
