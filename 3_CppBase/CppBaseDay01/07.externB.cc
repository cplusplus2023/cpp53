#include <iostream>
using std::cout;
using std::endl;

namespace wd
{
extern int num;
extern void display();
}

namespace
{
extern int num3;
}

/* extern int num2; */

void test0(){
    cout << wd::num << endl;
    wd::display();
    /* cout << num2 << endl; */
    cout << num3 << endl;
}

int main(void){
    test0();
    return 0;
}
