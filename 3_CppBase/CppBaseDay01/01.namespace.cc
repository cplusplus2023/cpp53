#include <iostream>
/* using namespace std; */

int num = 100;

void print(){
    std::cout << "hello" << std::endl;
}

namespace wd
{
int num = 1000;

void print(){
    std::cout << "world" << std::endl;
}
}//end of namespace wd

void test0(){
    //命名空间名 + 作用域限定符::
    std::cout << wd::num << std::endl;

    wd::print();
}

int main(void){
    test0();
    return 0;
}
