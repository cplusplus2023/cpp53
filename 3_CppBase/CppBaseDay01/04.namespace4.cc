#include <iostream>
/* using namespace std; */
using std::cout;
using std::endl;



/* int num = 100; */

/* void print(){ */
/*     cout << "hello" << endl; */
/* } */

namespace wd
{
int num = 1000;

void print(){
    cout << "world" << endl;
}

namespace cpp
{
int num = 20;

void print(){
    cout << "yes" << endl;
}
}//end of namespacec cpp
}//end of namespace wd

using wd::cpp::num;
using wd::cpp::print;

void test0(){
    cout << num << endl;
    print();
}

int main(void){
    test0();
    return 0;
}
