#include <string.h>
#include <iostream>
using std::cout;
using std::endl;

class Computer
{
public:
    Computer() = default;//C++11

    Computer(const char * brand, double price)
        : _brand(new char[strlen(brand) + 1]())
          , _price(price) // _price = price
    {
        strcpy(_brand,brand);
        cout << "Computer(const char *, double)" << endl;
    }

    Computer(const Computer & rhs)
        : _brand(new char[strlen(rhs._brand)]())
          , _price(rhs._price)
    {
        strcpy(_brand,rhs._brand);
        cout << "Computer(const Computer &)" << endl;
    }

    Computer & operator=(const Computer & rhs)
    {
        if(this != &rhs){//1.考虑自复制
            delete [] _brand;//2.回收原本申请的空间
            _brand = new char[strlen(rhs._brand) + 1]();//3.深拷贝
            strcpy(_brand,rhs._brand);
            _price = rhs._price;
        }
        cout << "Computer& operator=(const Computer&)" << endl;
        return *this;//4.返回*this
    }

    ~Computer(){
        if(_brand){
            delete [] _brand;
            _brand = nullptr;
        }
        cout << "~Computer()" << endl;
    }


    void print(){
        cout << "brand:" << _brand << endl;
        cout << "price:" << _price << endl;
    }
private:
    char * _brand;
    double _price;
};

void test0(){
    Computer pc("Apple",15000);
    pc.print();

    Computer pc2;
    pc2.print();
}


int main(void){
    test0();
    return 0;
}
