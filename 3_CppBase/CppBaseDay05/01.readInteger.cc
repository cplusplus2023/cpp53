#include <limits>
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::string;

void readInteger(int & number){
    cout <<"请输入一个整型数据：" << endl;
    //逗号表达式整体的值为最后一个逗号之后的表达式的值
    while(cin >> number, !cin.eof()){
        if(cin.bad()){
            cout << "输入流已经崩溃！" << endl;
            return;
        }else if(cin.fail()){
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
            cout <<"请输入一个整型数据：" << endl;
        }else{
            cout << "number:" << number << endl;
            break;
        }
    }
}

void test0(){
    int num = 0;
    readInteger(num);
    string line;
    cout << cin.good() << endl;
    cin >> line;
    cout << line << endl;
}

int main(void){
    test0();
    return 0;
}
