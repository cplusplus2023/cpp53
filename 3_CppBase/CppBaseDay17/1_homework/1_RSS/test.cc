#include "tinyxml2.h"
#include <iostream>
#include <string>
#include <regex>

using std::cout;
using std::endl;
using std::string;
using std::regex;
using namespace tinyxml2;

void test()
{
    XMLDocument doc;
    //加载文件（C、C++中打开文件)
    doc.LoadFile("coolshell.xml");

    //文件有没有打开成功
    if(doc.ErrorID())
    {
        std::cerr << "LoadFile error" << endl;
        return;
    }

    XMLElement *itemNode = doc.FirstChildElement("rss")
                            ->FirstChildElement("channel")
                            ->FirstChildElement("item");
    /* if(itemNode) */
    while(itemNode)
    {
#if 0
        XMLElement *ptitle = itemNode->FirstChildElement("title");
        if(ptitle)
        {
            const char *ptl = ptitle->GetText();
        }
#endif
        string title = itemNode->FirstChildElement("title")->GetText();
        string link = itemNode->FirstChildElement("link")->GetText();
        string description = itemNode->FirstChildElement("description")->GetText();
        string content = itemNode->FirstChildElement("content:encoded")->GetText();

        //正则表达式
        regex reg("<[^>]+>");//去除html标签的通用正则表达式
        description = regex_replace(description, reg, "");
        content = regex_replace(content, reg, "");

        cout << "title = " << title << endl;
        cout << "link = " << link << endl;
        cout << "description = " << description << endl;
        cout << "content = " << content << endl;

        //找到item的下一个兄弟节点
        itemNode = itemNode->NextSiblingElement("item");
    }
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

