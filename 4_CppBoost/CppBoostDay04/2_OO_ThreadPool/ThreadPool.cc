#include "ThreadPool.h"
#include "WorkThread.h"
#include "Task.h"
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

ThreadPool::ThreadPool(size_t threadNum, size_t queSize)
: _threadNum(threadNum)
, _queSize(queSize)
, _taskQue(_queSize)
, _isExit(false)
{
    //为了防止频繁扩容，就预留空间
    _threads.reserve(_threadNum);
}

ThreadPool::~ThreadPool()
{

}

//线程池的启动与停止
void ThreadPool::start()
{
    //1、创建工作线程
    for(size_t idx = 0; idx != _threadNum; ++idx)
    {
        unique_ptr<Thread> up(new WorkThread(*this));
        _threads.push_back(std::move(up));
        /* _threads.push_back(unique_ptr<Thread>(new WorkThread(*this))); */
    }
    //2、启动所有的工作线程
    for(auto &th : _threads)
    {
        th->start();//将所有的工作线程都运行起来了
    }
}

void ThreadPool::stop()
{
    //现在没有机制能保证子线程将所有的任务执行完毕之后才退出？
    //解决方案：只要任务队列中有任务，就不能回收子线程
    while(!_taskQue.empty())
    {
        sleep(1);
    }

    _isExit = true;//线程池马上要退出，可以将标志位设置为true
    //将所有的在_notEmpty条件变量上的线程全部唤醒，然后让主线程回收
    _taskQue.wakeup();

    for(auto &th : _threads)
    {
        th->stop();//将所有的工作线程都停止运行
    }
}

//添加任务
void ThreadPool::addTask(Task *ptask)
{
    if(ptask)
    {
        _taskQue.push(ptask);
    }
}

//获取任务
Task * ThreadPool::getTask()
{
    return _taskQue.pop();
}

//线程池交给工作线程执行的任务
void ThreadPool::doTask()
{
    //只要线程池不退出，任务队列中有任务，工作线程就应该
    //一直拿任务并且执行
    //
    //如果工作线程在执行getTask拿到任务之后，任务队列为空，那么
    //子线程会继续向下执行，执行process函数，然后主线程也会继续
    //向下执行，执行stop中的while循环之后的代码（也就是将标志
    //位设置为true）。如果子线程执行process的速率比主线程将
    //标志位设置为true要慢，那么当子线程执行完后任务之后，就
    //不会进到while循环中,那么程序就可以跳出来，不会阻塞；如果
    //子线程执行process的速率比主线程将标志位设置为true要快，那么
    //子线程进到while循环的时候，发现_isExit是为false，子线程
    //又进入了循环,发现任务队列中没有数据，getTask就会阻塞.
    while(!_isExit)
    /* while(!_taskQue.empty()) */
    {
        //1、如何获取任务
        Task *ptask = getTask();//pop
        //2、执行任务
        if(ptask)
        {
            ptask->process();//需要执行的任务，此处要体现多态
            /* sleep(3); */
        }
        else
        {
            cout << "ptask == nullptr" << endl;
        }
    }
}
