#ifndef __TASK_H__
#define __TASK_H__

class Task
{
public:
    Task() = default;
    ~Task() {}

    virtual void process() = 0;
};

#endif

