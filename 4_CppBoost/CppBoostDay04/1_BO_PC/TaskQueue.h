#ifndef __TASKQUEUE_H__
#define __TASKQUEUE_H__

#include "MutexLock.h"
#include "Condition.h"
#include <queue>

using std::queue;

class TaskQueue
{
public:
    TaskQueue(size_t queSize);
    ~TaskQueue();

    //任务队列是不是空或者满
    bool empty() const;
    bool full() const;

    //添加与获取
    void push(const int &value);
    int pop();

private:
    size_t _queSize;//任务队列的大小
    queue<int> _que;//存放数据的数据结构
    MutexLock _mutex;//互斥锁的对象
    Condition _notEmpty;//非空条件变量
    Condition _notFull;//非满条件变量

};

#endif
