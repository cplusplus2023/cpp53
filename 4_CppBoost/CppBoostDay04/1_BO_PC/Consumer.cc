#include "Consumer.h"
#include "TaskQueue.h"
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

Consumer::Consumer()
{

}

Consumer::~Consumer()
{

}

void Consumer::consume(TaskQueue &taskQue) 
{
    int cnt = 20;
    while(cnt-- > 0)
    {
        //其实就是为了从TaskQueue中取出任务
        int number = taskQue.pop();
        cout << ">>Consumer consume number = " << number << endl;

        sleep(1);
    }
}
