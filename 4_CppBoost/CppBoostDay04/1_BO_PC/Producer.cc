#include "Producer.h"
#include "TaskQueue.h"
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <iostream>

using std::cout;
using std::endl;

Producer::Producer()
{

}

Producer::~Producer()
{

}

void Producer::produce(TaskQueue &taskQue) 
{
    //种随机种子
    ::srand(::time(nullptr));

    int cnt = 20;
    while(cnt-- > 0)
    {
        int number = ::rand() %100;
        //其实就是为了去将任务放在TaskQueue
        taskQue.push(number);
        cout << ">>Producer produce number = " << number << endl;

        sleep(1);
    }
}
