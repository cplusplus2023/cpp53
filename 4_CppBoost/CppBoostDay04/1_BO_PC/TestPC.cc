#include "Producer.h"
#include "Consumer.h"
#include "Thread.h"
#include "TaskQueue.h"
#include <iostream>
#include <memory>
#include <functional>

using std::cout;
using std::endl;
using std::unique_ptr;
using std::bind;

void test()
{
    TaskQueue taskQue(10);
    
    Producer pro1;
    Consumer con1;

    Thread pro(std::bind(&Producer::produce, &pro1, std::ref(taskQue)));
    Thread con(std::bind(&Consumer::consume, &con1, std::ref(taskQue)));

    //让生产者与消费者运行起来
    pro.start();
    con.start();

    //让主线程等待子线程退出
    pro.stop();
    con.stop();
}

void test2()
{
    TaskQueue taskQue(10);

    unique_ptr<Producer> pro1(new Producer());
    unique_ptr<Consumer> con1(new Consumer());

    Thread pro(std::bind(&Producer::produce, pro1.get(), std::ref(taskQue)));
    Thread con(std::bind(&Consumer::consume, con1.get(), std::ref(taskQue)));

    //让生产者与消费者运行起来
    pro.start();
    con.start();

    //让主线程等待子线程退出
    pro.stop();
    con.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

