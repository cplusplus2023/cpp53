#include "ThreadPool.h"
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
{
public:
    void process()
    {
        //具体的逻辑可以根据需求进行
        ::srand(::clock());
        int number = ::rand()%100;
        cout << ">>BO_Threadpool.process number = " << number << endl;
    }
};

void test()
{
    unique_ptr<MyTask> ptask(new MyTask());

    ThreadPool pool(4, 10);//线程池对象创建出来

    //工作线程都应该创建并且启动，等待着任务的到来
    pool.start();//启动线程池

    int cnt = 20;
    while(cnt-- > 0)
    {
        cout << "cnt = " << cnt << endl;
        pool.addTask(std::bind(&MyTask::process, ptask.get()));
    }

    pool.stop();//线程池退出
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

