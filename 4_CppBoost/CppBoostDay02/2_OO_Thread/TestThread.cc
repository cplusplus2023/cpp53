#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyThread
: public Thread
{
public:
private:
    void run() override
    {
        //具体逻辑可以根据具体场景修改
        while(1)
        {
            cout << "MyThread is running!!!" << endl;
            sleep(1);
        }
    }
};

void test()
{
    unique_ptr<Thread> pt(new MyThread());

    pt->start();
    pt->stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

