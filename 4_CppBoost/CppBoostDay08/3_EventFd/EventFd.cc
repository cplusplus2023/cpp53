#include "EventFd.h"
#include <unistd.h>
#include <poll.h>
#include <stdio.h>
#include <sys/eventfd.h>

#include <iostream>

using std::cout;
using std::endl;

EventFd::EventFd(EventFdCallback &&cb)
: _evtfd(createEventFd())
, _cb(std::move(cb))//回调函数的注册
, _isStarted(false)
{

}

EventFd::~EventFd()
{
    close(_evtfd);
}

//启动与停止
void EventFd::start()
{
    struct pollfd pfd;
    pfd.fd = _evtfd;
    pfd.events = POLLIN;

    _isStarted = true;

    while(_isStarted)
    {
        int nready = ::poll(&pfd, 1, 5000);
        if(-1 == nready && errno == EINTR)
        {
            continue;
        }
        else if(-1 == nready)
        {
            perror("-1 == nready");
            return;
        }
        else if(0 == nready)
        {
            cout << ">>poll timeout!!!" << endl;
        }
        else
        {
            if(pfd.revents & POLLIN)
            {
                handleRead();//阻塞等待
                if(_cb)
                {
                    //回调函数的执行
                    _cb();//被唤醒之后，就可以执行任务
                }
            }
        }
    }
}

void EventFd::stop()
{
    _isStarted = false;
}

//唤醒，也就是执行write操作
void EventFd::wakeup()
{
    uint64_t one = 1;
    ssize_t ret = ::write(_evtfd, &one, sizeof(one));
    if(ret != sizeof(one))
    {
        perror("wakeup");
        return;
    }
}

//执行read
void EventFd::handleRead()
{
    uint64_t one = 1;
    ssize_t ret = ::read(_evtfd, &one, sizeof(one));
    if(ret != sizeof(one))
    {
        perror("handleRead");
        return;
    }
}

//创建文件描述符的函数eventfd
int EventFd::createEventFd()
{
    int fd = eventfd(10, 0);
    if(fd < 0)
    {
        perror("createEventFd");
        return fd;
    }
    return fd;
}
