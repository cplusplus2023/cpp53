#include "EventFd.h"
#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <functional>

using std::cout;
using std::endl;
using std::bind;

class MyTask
{
public:
    void process()
    {
        cout << "MyTask is running" << endl;
        /* while(1) */
        /* { */
        /*     cout << "MyTask is running" << endl; */
        /* } */
    }
};

void test()
{
    MyTask task;
    EventFd efd(bind(&MyTask::process, &task));
    Thread th(bind(&EventFd::start, &efd));
    th.start();
    
    int cnt = 10;
    while(cnt-- > 0)
    {
        efd.wakeup();//去唤醒另外一个线程
        sleep(1);
    }

    efd.stop();
    th.stop();
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

