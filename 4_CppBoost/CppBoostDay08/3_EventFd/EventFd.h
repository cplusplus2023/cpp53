#ifndef __EVENTFD_H__
#define __EVENTFD_H__

#include <functional>

using std::function;

class EventFd
{
    using EventFdCallback = function<void()>;
public:
    EventFd(EventFdCallback &&cb);
    ~EventFd();

    //启动与停止
    void start();
    void stop();

    //唤醒，也就是执行write操作
    void wakeup();

private:
    //执行read
    void handleRead();
    //创建文件描述符的函数eventfd
    int createEventFd();

private:
    int _evtfd;//eventfd系统调用返回的文件描述符
    EventFdCallback _cb;//唤醒之后所做的任务
    bool _isStarted;//标识是否运行的标志


};

#endif
