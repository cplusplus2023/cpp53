#ifndef __THREAD_H__
#define __THREAD_H__

#include <pthread.h>
#include <functional>

using std::function;

class Thread
{
    using ThreadCallback = function<void()>;
public:
    Thread(const ThreadCallback &cb);
    Thread(ThreadCallback &&cb);
    ~Thread();

    //线程的运行与停止
    void start();
    void stop();

private:
    //线程入口函数
    static void *threadFunc(void *arg);
private:
    pthread_t _thid;//线程id
    bool _isRunning;//线程是否在运行的标志
    ThreadCallback _cb;//线程需要执行的任务
};

#endif
