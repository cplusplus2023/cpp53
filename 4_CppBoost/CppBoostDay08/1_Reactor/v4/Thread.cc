#include "Thread.h"
#include <stdio.h>
#include <iostream>

using std::cout;
using std::endl;

Thread::Thread(const ThreadCallback &cb)
: _thid(0)
, _isRunning(false)
, _cb(cb)
{

}

Thread::Thread(ThreadCallback &&cb)
: _thid(0)
, _isRunning(false)
, _cb(std::move(cb))//注册回调函数
{

}

Thread::~Thread()
{

}

//线程的运行与停止
void Thread::start()
{
    //自己的函数想跳转的话，可以使用ctags
    //shift + k,可以跳转到库函数的定义
    //threadFunc需要设计为静态函数，不然与pthread_create的第三个
    //参数不匹配
    int ret = pthread_create(&_thid, nullptr, threadFunc, this);
    if(ret)
    {
        perror("pthread_create");
        return;
    }

    _isRunning = true;
}

void Thread::stop()
{
    if(_isRunning)
    {
        int ret = pthread_join(_thid, nullptr);
        if(ret)
        {
            perror("pthread_join");
            return;
        }

        _isRunning = false;
    }
}

//线程入口函数
void *Thread::threadFunc(void *arg)
{
    Thread *pth = static_cast<Thread *>(arg);
    if(pth)
    {
        pth->_cb();//回调函数的执行
    }
    else
    {
        cout << "pth == nullptr" << endl;
    }

    /* return nullptr; */
    pthread_exit(nullptr);
}
