#include "TaskQueue.h"

TaskQueue::TaskQueue(size_t queSize)
: _queSize(queSize)
, _que()
, _mutex()
, _notEmpty(_mutex)
, _notFull(_mutex)
, _flag(true)
{

}

TaskQueue::~TaskQueue()
{

}

//任务队列是不是空或者满
bool TaskQueue::empty() const
{
    return 0 == _que.size();
}

bool TaskQueue::full() const
{
    return _queSize == _que.size();
}

//添加与获取
void TaskQueue::push(ElemType &&value)
{
    MutexLockGuard autoLock(_mutex);//栈对象
    
    while(full())//虚假唤醒
    {
        _notFull.wait();//生产者在睡眠
    }

    _que.push(std::move(value));
    _notEmpty.notify();
}

ElemType TaskQueue::pop()
{
    MutexLockGuard autoLock(_mutex);//栈对象

    while(empty() && _flag)
    {
        _notEmpty.wait();//消费者睡眠
    }

    if(_flag)
    {
        ElemType tmp = _que.front();//获取之后
        _que.pop();//再删除第一个元素

        _notFull.notify();

        return tmp;
    }
    else
    {
        return nullptr; 
    }
}

void TaskQueue::wakeup()
{
    _flag = false;
    _notEmpty.notifyAll();
}
