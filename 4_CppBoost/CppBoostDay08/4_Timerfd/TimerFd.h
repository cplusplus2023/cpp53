#ifndef __TIMERFD_H__
#define __TIMERFD_H__

#include <functional>

using std::function;

class TimerFd
{
    using TimerFdCallback = function<void()>;
public:
    TimerFd(int initSec, int peridocSec, TimerFdCallback &&cb);
    ~TimerFd();

    //开始运行与结束
    void start();
    void stop();

private:
    //封装了read函数
    void handleRead();
    //使用TimerFd创建文件描述符
    int createTimerFd();
    //设置定时器
    void setTimer(int initSec, int peridocSec);

private:
    int _timerfd;//定时器文件描述符
    int _initSec;//起始时间
    int _peridocSec;//周期时间
    TimerFdCallback _cb;//待执行的任务
    bool _isStarted;//表示TimerFd是不是在运行


};

#endif
