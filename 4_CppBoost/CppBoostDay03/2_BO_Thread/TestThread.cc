#include "Thread.h"
#include <unistd.h>
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

class MyTask
{
public:
    void process() 
    {
        //具体逻辑可以根据具体场景修改
        while(1)
        {
            cout << "MyTask is running!!!" << endl;
            sleep(1);
        }
    }
};

void func() 
{
    //具体逻辑可以根据具体场景修改
    while(1)
    {
        cout << "func is running!!!" << endl;
        sleep(1);
    }
}

void func2(int x) 
{
    //具体逻辑可以根据具体场景修改
    while(1)
    {
        cout << "func2 is running!!!" << endl;
        sleep(x);
    }
}
void test()
{
    MyTask task;
    Thread th(std::bind(&MyTask::process, &task));

    th.start();
    th.stop();
}

void test2()
{
    /* Thread th(std::bind(&func)); */
    /* Thread th(std::move(func)); */
    Thread th(func);
    /* Thread th(std::move(func2));//error */

    th.start();
    th.stop();
}

int main(int argc, char *argv[])
{
    test2();
    return 0;
}

