#ifndef __THREAD_H__
#define __THREAD_H__

#include <pthread.h>

class Thread
{
public:
    Thread();
    virtual ~Thread();

    //线程的运行与停止
    void start();
    void stop();

private:
    //线程入口函数
    static void *threadFunc(void *arg);
    //线程需要执行的任务
    virtual void run() = 0;
private:
    pthread_t _thid;//线程id
    bool _isRunning;//线程是否在运行的标志
};

#endif
