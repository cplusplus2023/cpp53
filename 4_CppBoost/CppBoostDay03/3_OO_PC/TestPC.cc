#include "Producer.h"
#include "Consumer.h"
#include "TaskQueue.h"
#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::unique_ptr;

void test()
{
    TaskQueue taskQue(10);

    Producer pro(taskQue);
    Consumer con(taskQue);

    //让生产者与消费者运行起来
    pro.start();
    con.start();

    //让主线程等待子线程退出
    pro.stop();
    con.stop();
}

void test2()
{
    TaskQueue taskQue(10);

    unique_ptr<Thread> pro(new Producer(taskQue));
    unique_ptr<Thread> con(new Consumer(taskQue));

    //让生产者与消费者运行起来
    pro->start();
    con->start();

    //让主线程等待子线程退出
    pro->stop();
    con->stop();

#if 0
    MutexLock mutex1;
    MutexLock mutex2 = mutex1;//error

    MutexLock mutex3;
    mutex3 = mutex1;//error
#endif
}

int main(int argc, char *argv[])
{
    test();
    return 0;
}

