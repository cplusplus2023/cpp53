#include "TaskQueue.h"

TaskQueue::TaskQueue(size_t queSize)
: _queSize(queSize)
, _que()
, _mutex()
, _notEmpty(_mutex)
, _notFull(_mutex)
{

}

TaskQueue::~TaskQueue()
{

}

//任务队列是不是空或者满
bool TaskQueue::empty() const
{
#if 0
    if(0 == _que.size())
    {
        return true;
    }
    else
    {
        return false;
    }
#endif
    return 0 == _que.size();
}

bool TaskQueue::full() const
{
    return _queSize == _que.size();
}

//添加与获取
void TaskQueue::push(const int &value)
{
    //1、上锁
    //RAII的思想：利用栈对象的生命周期管理资源
    MutexLockGuard autoLock(_mutex);//栈对象
    /* _mutex.lock(); */
    //2、判断TaskQueue是不是满的
    //2.1、如果是满的
    while(full())//虚假唤醒
    {
        _notFull.wait();//生产者在睡眠
    }

    /* if(//) */
    /* { */
    /*     return ; */
    /* } */
    //2.2、如果不是满的
    //3、生产任务，放在queue存起来,并且唤醒消费者
    _que.push(value);
    _notEmpty.notify();

    //4、解锁
    /* _mutex.unlock(); */
}

int TaskQueue::pop()
{
    //1、上锁
    MutexLockGuard autoLock(_mutex);//栈对象
    /* _mutex.lock(); */
    //2、判断TaskQueue是不是空的
    //2.1、如果是空的
    while(empty())
    {
        _notEmpty.wait();//消费者睡眠
    }
    //2.2、如果不是空的
    //3、执行任务,从queue取起来,并且唤醒生产者
    int tmp = _que.front();//获取之后
    _que.pop();//再删除第一个元素

    _notFull.notify();

    //4、解锁
    /* _mutex.unlock(); */

    return tmp;
}
