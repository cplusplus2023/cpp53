#ifndef __EVENTLOOP_H__
#define __EVENTLOOP_H__

#include <sys/epoll.h>
#include <vector>
#include <map>
#include <memory>

using std::vector;
using std::map;
using std::shared_ptr;

class Acceptor;//前向声明
class TcpConnection;//前向声明

class EventLoop
{
    using TcpConnectionPtr = shared_ptr<TcpConnection>;
public:
    EventLoop(Acceptor &accptor);
    ~EventLoop();

    //循环进行还是没有进行
    void loop();
    void unloop();

private:
    //封装epoll_wait的函数
    void waitEpollFd();

    //处理新的连接
    void handleNewConnection();
    //处理消息的收发
    void handleMessage(int fd);

    //创建文件描述符_epfd
    int createEpollFd();
    //将文件描述符放在红黑树上进行监听
    void addEpollReadFd(int fd);
    //将文件描述符从红黑树上摘除
    void delEpollReadFd(int fd);

private:
    int _epfd;//epoll_create创建文件描述符
    vector<struct epoll_event> _evtlist;//存放满足条件的文件描述符集合
    bool _isLooping;//标识循环是否在进行
    Acceptor &_acceptor;//需要调用Acceptor中的accept函数
    map<int, TcpConnectionPtr> _conns;//存放文件描述符与连接的键值对
};

#endif
