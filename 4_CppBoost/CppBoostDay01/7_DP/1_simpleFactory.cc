#include <math.h>
#include <iostream>
#include <string>
#include <memory>

using std::cout;
using std::endl;
using std::string;
using std::unique_ptr;

class Figure
{
public:
    virtual void display() const = 0;
    virtual double area() const = 0;
    virtual ~Figure() {  }
};

class Rectangle
: public Figure
{
public:
    Rectangle(double length = 0, double width = 0)
    : _length(length)
    , _width(width)
    {
        cout << "Rectangle(double = 0, double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Rectangle";
    }

    double area() const override
    {
        return _length * _width;
    }

    ~Rectangle()
    {
        cout << "~Rectangle()" << endl;
    }
private:
    double _length;
    double _width;
};

class Circle
: public Figure
{
public:
    Circle(double radius = 0)
    : _radius(radius)
    {
        cout << "Circle(double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Circle";
    }

    double area() const override
    {
        return 3.14 * _radius *_radius;;
    }

    ~Circle()
    {
        cout << "~Circle()" << endl;
    }
private:
    double _radius;
};

class Triangle
: public Figure
{
public:
    Triangle(double a = 0, double b = 0, double c = 0)
    : _a(a)
    , _b(b)
    , _c(c)
    {
        cout << "Triangle(double = 0, double = 0, double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Triangle";
    }

    double area() const override
    {
        //海伦公式
        double tmp = (_a + _b + _c)/2;

        return sqrt(tmp * (tmp - _a) * (tmp - _b) * (tmp - _c));
    }

    ~Triangle()
    {
        cout << "~Triangle()" << endl;
    }
private:
    double _a;
    double _b;
    double _c;
};

void func(Figure *pfig)
{
    pfig->display();
    cout << "的面积 : " << pfig->area() << endl;
}

//根据产品的名字可以生产对应的产品，这就是简单工厂方法
//静态工厂
//
//违背的设计原则：
//1、违反了单一职责原则
//2、违反了开放闭合原则
//3、违反了依赖导致原则
class Factory
{
public:
    static Figure *create(const string &name)
    {
        if(name == "rectangle")
        {
            //需要读配置文件(.conf、.txt、.xml、.data、.yang)
            //获取文件中的数据
            //将数据传递到每一个对象中
            Rectangle *pret = new Rectangle(10, 20);
    
            return pret;
        }
        else if(name == "circle")
        {
            //需要读配置文件(.conf、.txt、.xml、.data、.yang)
            //获取文件中的数据
            //将数据传递到每一个对象中
            Circle *pcir = new Circle(10);
    
            return pcir;
        }
        else if(name == "triangle")
        {
            //需要读配置文件(.conf、.txt、.xml、.data、.yang)
            //获取文件中的数据
            //将数据传递到每一个对象中
            Triangle *ptri= new Triangle(3, 4, 5);
    
            return ptri;
        }
        else
        {
            return nullptr;
        }
    }
#if 0
    static Figure *createRectangle()
    {
        //需要读配置文件(.conf、.txt、.xml、.data、.yang)
        //获取文件中的数据
        //将数据传递到每一个对象中
        Rectangle *pret = new Rectangle(10, 20);
    
        return pret;
    }
    
    static Figure *createCircle()
    {
        //需要读配置文件(.conf、.txt、.xml、.data、.yang)
        //获取文件中的数据
        //将数据传递到每一个对象中
        Circle *pcir = new Circle(10);
    
        return pcir;
    }
    
    static Figure *createTriangle()
    {
        //需要读配置文件(.conf、.txt、.xml、.data、.yang)
        //获取文件中的数据
        //将数据传递到每一个对象中
        Triangle *ptri= new Triangle(3, 4, 5);
    
        return ptri;
    }
#endif
};


int main(int argc, char **argv)
{
    unique_ptr<Figure> pret(Factory::create("rectangle"));
    unique_ptr<Figure> pcir(Factory::create("circle"));
    unique_ptr<Figure> ptri(Factory::create("triangle"));

    func(pret.get());
    func(pcir.get());
    func(ptri.get());

    return 0;
}

