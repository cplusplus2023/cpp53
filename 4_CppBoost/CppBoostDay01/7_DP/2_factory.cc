#include <math.h>
#include <iostream>
#include <string>
#include <memory>

using std::cout;
using std::endl;
using std::string;
using std::unique_ptr;

class Figure
{
public:
    virtual void display() const = 0;
    virtual double area() const = 0;
    virtual ~Figure() {  }
};

class Rectangle
: public Figure
{
public:
    Rectangle(double length = 0, double width = 0)
    : _length(length)
    , _width(width)
    {
        cout << "Rectangle(double = 0, double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Rectangle";
    }

    double area() const override
    {
        return _length * _width;
    }

    ~Rectangle()
    {
        cout << "~Rectangle()" << endl;
    }
private:
    double _length;
    double _width;
};

class Circle
: public Figure
{
public:
    Circle(double radius = 0)
    : _radius(radius)
    {
        cout << "Circle(double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Circle";
    }

    double area() const override
    {
        return 3.14 * _radius *_radius;;
    }

    ~Circle()
    {
        cout << "~Circle()" << endl;
    }
private:
    double _radius;
};

class Triangle
: public Figure
{
public:
    Triangle(double a = 0, double b = 0, double c = 0)
    : _a(a)
    , _b(b)
    , _c(c)
    {
        cout << "Triangle(double = 0, double = 0, double = 0)" << endl;
    }

    void display() const override
    {
        cout << "Triangle";
    }

    double area() const override
    {
        //海伦公式
        double tmp = (_a + _b + _c)/2;

        return sqrt(tmp * (tmp - _a) * (tmp - _b) * (tmp - _c));
    }

    ~Triangle()
    {
        cout << "~Triangle()" << endl;
    }
private:
    double _a;
    double _b;
    double _c;
};

void func(Figure *pfig)
{
    pfig->display();
    cout << "的面积 : " << pfig->area() << endl;
}

//工厂方法(工厂设计模式)
//特点：为每一个产品创建一个工厂,该工厂就可以生产该类型的产品
//缺点：因为产品的个数与工厂的数量是对应的，那么如果产品的数量增多之后，
//那么工厂的数量会急剧上升(在生活中，这么多工厂就难以管理)
//
//
//使用设计原则：
//1、使用了单一职责原则
//2、使用了开放闭合原则
//3、使用了依赖导致原则

class Factory
{
public:
    virtual Figure *create() = 0;
    //如果一个基类设计了虚函数，往往需要将其析构函数设计为虚函数
    virtual ~Factory() {}
};

class RectangleFactory
: public Factory
{
public:
    virtual Figure *create() override
    {
        //需要读配置文件(.conf、.txt、.xml、.data、.yang)
        //获取文件中的数据
        //将数据传递到每一个对象中
        Rectangle *pret = new Rectangle(10, 20);
    
        return pret;
    }
};

class CircleFactory
: public Factory
{
public:
    virtual Figure *create() override
    {
        //需要读配置文件(.conf、.txt、.xml、.data、.yang)
        //获取文件中的数据
        //将数据传递到每一个对象中
        Circle *pcir = new Circle(10);
    
        return pcir;
    }
};

class TriangleFactory
: public Factory
{
public:
    virtual Figure *create() override
    {
        //需要读配置文件(.conf、.txt、.xml、.data、.yang)
        //获取文件中的数据
        //将数据传递到每一个对象中
        Triangle *ptri= new Triangle(3, 4, 5);
    
        return ptri;
    }
};

int main(int argc, char **argv)
{
    unique_ptr<Factory> recFac(new RectangleFactory());
    unique_ptr<Figure> prec(recFac->create());

    unique_ptr<Factory> cirFac(new CircleFactory());
    unique_ptr<Figure> pcir(cirFac->create());

    unique_ptr<Factory> triFac(new TriangleFactory());
    unique_ptr<Figure> ptri(triFac->create());

    func(prec.get());
    func(pcir.get());
    func(ptri.get());

    return 0;
}

