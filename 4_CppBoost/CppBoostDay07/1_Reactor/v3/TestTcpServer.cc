#include "TcpServer.h"
#include "Acceptor.h"
#include "TcpConnection.h"
#include "EventLoop.h"
#include <iostream>
#include <unistd.h>

using std::cout;
using std::endl;

void onNewConnection(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has connected!!!" << endl;
}

void onMessage(const TcpConnectionPtr &con)
{
    //接收客户端的数据（ready）
    string msg = con->receive();
    cout << ">>recv client msg = " << msg << endl;

    //得到客户端发送过来的数据之后，业务逻辑的处理还没有进行
    //decode、compute、encode
    //如果业务逻辑的处理非常的麻烦，非常的耗时，那么该版本会
    //遇到瓶颈

    //将数据发送给客户端(send)
    con->send(msg);
}

void onClose(const TcpConnectionPtr &con)
{
    cout << con->toString() << " has closed!!!" << endl;
}

void test()
{
    TcpServer server("127.0.0.1", 8888);

    server.setAllCallback(std::move(onNewConnection)
                          , std::move(onMessage)
                          , std::move(onClose));

    server.start();//启动服务器
}

int main(int argc, char **argv)
{
    test();
    return 0;
}

