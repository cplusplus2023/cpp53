#include <53func.h>
int main(int argc, char *argv[])
{
    // ./5_client_udp 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    // 赋值用具体类型 传参时强转成公共类型
    bind(sockfd,(struct sockaddr *)&addr, sizeof(addr));
    char buf[4096] = {0};
    struct sockaddr_in client_addr;
    socklen_t client_addr_length = sizeof(client_addr);
    recvfrom(sockfd,buf,sizeof(buf),0,
             (struct sockaddr *)&client_addr,&client_addr_length);
    printf("buf = %s\n", buf);
    printf("client ip = %s, port = %d\n",
           inet_ntoa(client_addr.sin_addr), 
           ntohs(client_addr.sin_port));
    sendto(sockfd,"wozai",5,0,
            (struct sockaddr *)&client_addr,client_addr_length);
    return 0;

}

