#include <53func.h>
int main(int argc, char *argv[])
{
    // ./6_aqiang_udp 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    sendto(sockfd,"zaima",5,0,
           (struct sockaddr *)&addr,sizeof(addr));
    char buf[4096] = {0};
    fd_set rdset;
    while(1){
        FD_ZERO(&rdset);
        FD_SET(sockfd,&rdset);
        FD_SET(STDIN_FILENO,&rdset);
        select(sockfd+1,&rdset,NULL,NULL,NULL);
        if(FD_ISSET(STDIN_FILENO,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){
                sendto(sockfd,buf,0,0,
                    (struct sockaddr *)&addr,sizeof(addr));
                break;
            }
            sendto(sockfd,buf,strlen(buf),0,
                   (struct sockaddr *)&addr,sizeof(addr));
        }
        if(FD_ISSET(sockfd,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = recvfrom(sockfd,buf,sizeof(buf),0,NULL,NULL);
            if(sret == 0){
                printf("peer has quit!\n");
                break;
            }
            printf("buf = %s\n", buf);
        }
    }
    return 0;

}

