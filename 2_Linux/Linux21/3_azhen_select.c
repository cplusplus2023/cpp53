#include <53func.h>
int main(int argc, char *argv[])
{
    // ./3_azhen_select 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    int flag = 1; // 给SO_REUSEADDR属性的参数分配内存
    int ret = setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&flag,sizeof(flag));
    // 赋值用具体类型 传参时强转成公共类型
    ret = bind(sockfd,(struct sockaddr *)&addr, sizeof(addr));
    // 服务端 bind 只能绑定服务端地址
    ERROR_CHECK(ret,-1,"bind");
    listen(sockfd,10);
    struct sockaddr_in client_addr;
    // client_addr_length 应该要有一个合适的初始值
    socklen_t client_addr_length = sizeof(client_addr);
    int netfd = accept(sockfd,
                       (struct sockaddr *)&client_addr,
                       &client_addr_length);
    printf("client ip = %s, port = %d\n",
           inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
    char buf[4096] = {0};
    ssize_t sret;
    // 创建监听集合
    fd_set rdset;
    while(1){
        // 初始化
        FD_ZERO(&rdset);
        // 设置监听
        FD_SET(STDIN_FILENO,&rdset);
        FD_SET(netfd,&rdset); // netfd导致的阻塞
        select(netfd+1,&rdset,NULL,NULL,NULL); // 内核轮询，用户等待
        if(FD_ISSET(STDIN_FILENO,&rdset)){ // stdin来消息
            bzero(buf,sizeof(buf));
            sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){
                send(netfd,"nishigehaoren",13,0);
                break;
            }
            send(netfd,buf,strlen(buf),0);
        }
        if(FD_ISSET(netfd,&rdset)){
            bzero(buf,sizeof(buf));
            sret = recv(netfd,buf,sizeof(buf),0);
            if(sret == 0){
                printf("Hehe\n");
                break;
            }
            printf("buf = %s\n", buf);
        }
    }
    close(netfd);
    close(sockfd);
    return 0;
}

