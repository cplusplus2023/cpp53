#include <53func.h>
int main(int argc, char *argv[])
{
    // ./1_bind 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    // 赋值用具体类型 传参时强转成公共类型
    int ret = bind(sockfd,(struct sockaddr *)&addr, sizeof(addr));
    // bind 只能绑定本地地址
    printf("ret = %d\n", ret);
    return 0;
}

