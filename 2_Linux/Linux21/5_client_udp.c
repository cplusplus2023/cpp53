#include <53func.h>
int main(int argc, char *argv[])
{
    // ./5_client_udp 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    sendto(sockfd,"zaima",5,0,
           (struct sockaddr *)&addr,sizeof(addr));
    char buf[4096] = {0};
    recvfrom(sockfd,buf,sizeof(buf),0,NULL,NULL);
    printf("buf = %s\n", buf);
    return 0;
}

