#include <53func.h>
int main(int argc, char *argv[])
{
    // ./2_server 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    // 赋值用具体类型 传参时强转成公共类型
    int ret = bind(sockfd,(struct sockaddr *)&addr, sizeof(addr));
    // 服务端 bind 只能绑定服务端地址
    printf("ret = %d\n", ret);
    listen(sockfd,10);
    struct sockaddr_in client_addr;
    // client_addr_length 应该要有一个合适的初始值
    socklen_t client_addr_length = sizeof(client_addr);
    int netfd = accept(sockfd,
                       (struct sockaddr *)&client_addr,
                       &client_addr_length);
    printf("client ip = %s, port = %d\n",
           inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
    char buf[4096] = {0};
    sleep(1);
    ssize_t sret = recv(netfd,buf,sizeof(buf),0);
    printf("sret = %ld, buf = %s\n",sret, buf);
    return 0;
}

