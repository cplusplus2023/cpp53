#include <53func.h>
int main(int argc, char *argv[])
{
    // ./2_client 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET; // AF_INET ipv4
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    // 赋值用具体类型 传参时强转成公共类型
    int ret = connect(sockfd,(struct sockaddr *)&addr, sizeof(addr));
    // connect 填的是服务端地址
    ERROR_CHECK(ret,-1,"connect");
    //ssize_t sret = send(sockfd,"niganma",7,0);
    //printf("sret = %ld\n", sret);
    //sret = send(sockfd,"aiyou",5,0);
    //printf("sret = %ld\n", sret);
    char buf[4096] = {0};
    ssize_t sret = send(sockfd,buf,4096,0);
    printf("sret = %ld\n", sret);
    return 0;
}

