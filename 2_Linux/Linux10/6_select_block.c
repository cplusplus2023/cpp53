#include <53func.h>
int main(int argc, char *argv[])
{
    // ./6_select_block 1.pipe
    ARGS_CHECK(argc,2);
    int fd1 = open(argv[1],O_RDWR);
    int fd2 = open(argv[1],O_RDWR);
    // 准备一个读集合和一个写集合
    fd_set rdset;
    fd_set wrset;
    int cnt = 0;
    char buf[4096] = {0};
    while(1){
        FD_ZERO(&rdset);
        FD_ZERO(&wrset);
        FD_SET(fd1,&rdset);
        FD_SET(fd2,&wrset);
        int ret = select(fd2+1,&rdset,&wrset,NULL,NULL);
        printf("ret = %d\n", ret);
        if(FD_ISSET(fd1,&rdset)){
            printf("read ready, cnt = %d\n", cnt++);
            read(fd1,buf,2048);
        }
        if(FD_ISSET(fd2,&wrset)){
            printf("write ready, cnt = %d\n", cnt++);
            write(fd2,buf,4096);
        }
        sleep(1);
    }
    return 0;
}

