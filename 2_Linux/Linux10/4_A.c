#include <53func.h>
typedef struct train_s {
    int length; // 火车头 固定4B
    char data[1000]; // 火车车厢 长度不固定的 1000 上限
} train_t;
int main(int argc, char *argv[])
{
    // ./4_A file1 1.pipe
    ARGS_CHECK(argc,3);
    int fd = open(argv[1],O_RDWR);
    int pipe_fdw = open(argv[2],O_WRONLY);

    train_t train;
    // 发文件名
    train.length = strlen(argv[1]);
    memcpy(train.data,argv[1],train.length);
    write(pipe_fdw,&train,sizeof(train.length)+train.length);
    // 发文件的内容
    ssize_t sret = read(fd,train.data,sizeof(train.data));
    train.length = sret;
    write(pipe_fdw,&train,sizeof(train.length)+train.length);
    return 0;
}

