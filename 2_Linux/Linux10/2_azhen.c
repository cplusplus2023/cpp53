#include <53func.h>
int main(int argc, char *argv[])
{
    // ./10_azhen_select 1.pipe 2.pipe 
    ARGS_CHECK(argc,3);
    int fdw = open(argv[1],O_WRONLY);
    int fdr = open(argv[2],O_RDONLY);
    printf("azhen is ready!\n");
    char buf[4096] = {0};
    ssize_t sret;
    fd_set rdset; // 目前的资源都是读阻塞
    struct timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    time_t now = time(NULL);
    printf("curtime = %s\n", ctime(&now));
    while(1){
        FD_ZERO(&rdset);
        FD_SET(STDIN_FILENO,&rdset); // 把标准输入加入监听
        FD_SET(fdr, &rdset); // 把管道的读端加入监听
        int ret = select(fdr+1,&rdset,NULL,NULL,&timeout);
        now = time(NULL);
        printf("curtime = %s\n", ctime(&now));
        if(ret == 0){
            printf("timeout !\n");
            break;
        }
        if(FD_ISSET(STDIN_FILENO,&rdset)){
            memset(buf,0,sizeof(buf));
            sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){
                write(fdw,"nishigehaoren",13);
                break;
            }
            write(fdw,buf,strlen(buf));
        }
        if(FD_ISSET(fdr,&rdset)){
            timeout.tv_sec = 10;
            timeout.tv_usec = 0;
            memset(buf,0,sizeof(buf));
            sret = read(fdr,buf,sizeof(buf));
            if(sret == 0){
                printf("Hehe!\n");
                break;
            }
            printf("buf = %s\n", buf);
        }
    }
    close(fdr);
    close(fdw);
    return 0;
}

