#include <53func.h>
int main(int argc, char *argv[])
{
    // ./5_block_read 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    sleep(20);
    printf("sleep over!\n");
    char buf[4096];
    read(fdr,buf,4096);
    sleep(20);
    close(fdr);
    return 0;
}

