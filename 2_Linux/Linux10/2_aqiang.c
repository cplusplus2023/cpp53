#include <53func.h>
int main(int argc, char *argv[])
{
    // ./2_aqiang 1.pipe 2.pipe 
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    int fdw = open(argv[2],O_WRONLY);
    printf("aqiang is ready!\n");
    char buf[4096] = {0};
    ssize_t sret;
    fd_set rdset; 
    while(1){
        FD_ZERO(&rdset);
        FD_SET(STDIN_FILENO,&rdset); // 把标准输入加入监听
        FD_SET(fdr, &rdset); // 把管道的读端加入监听
        select(fdr+1,&rdset,NULL,NULL,NULL);
        if(FD_ISSET(STDIN_FILENO,&rdset)){
            // stdin就绪
            memset(buf,0,sizeof(buf));
            sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){
                write(fdw,"nishigehaoren",13);
                break;
            }
            time_t now = time(NULL);
            char *p = ctime(&now);
            write(fdw,p,strlen(p));
            write(fdw,buf,strlen(buf));
        }
        if(FD_ISSET(fdr,&rdset)){
            // fdr就绪
            memset(buf,0,sizeof(buf));
            sret = read(fdr,buf,sizeof(buf));
            if(sret == 0){
                printf("Hehe!\n");
                break;
            }
            printf("buf = %s\n", buf);
        }
    }
    close(fdr);
    close(fdw);
    return 0;
}

