#include <53func.h>
int main(int argc, char *argv[])
{
    // ./5_block_write 1.pipe
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1],O_WRONLY);

    char buf[4096] = {0};
    int cnt = 0;
    ssize_t total = 0;
    while(1){
        ssize_t sret = write(fdw,buf,4096);
        total += sret;
        printf("cnt = %d, total = %ld\n",cnt++,total);
    }
    return 0;
}

