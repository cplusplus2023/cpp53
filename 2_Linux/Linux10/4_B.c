#include <53func.h>
typedef struct train_s {
    int length;
    char data[1000];
} train_t;
int main(int argc, char *argv[])
{
    // ./4_B 1.pipe
    ARGS_CHECK(argc,2);
    int pipe_fdr = open(argv[1],O_RDONLY);

    train_t train;
    char filename[4096] = {0};
    read(pipe_fdr,&train.length, sizeof(train.length));
    read(pipe_fdr,train.data,train.length);
    memcpy(filename,train.data,train.length);

    mkdir("dir1",0777);
    char filepath[8192];
    sprintf(filepath,"%s/%s","dir1",filename);
    int fd = open(filepath,O_RDWR|O_CREAT,0666);

    read(pipe_fdr,&train.length, sizeof(train.length));
    read(pipe_fdr,train.data,train.length);
    write(fd,train.data,train.length);
    return 0;
}

