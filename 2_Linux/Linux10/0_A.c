#include <53func.h>
int main(int argc, char *argv[])
{
    // ./0_A 1.pipe 
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1],O_WRONLY);

    printf("Hello World 1\n");

    int backup = 10;
    dup2(STDOUT_FILENO,backup);
    dup2(fdw,STDOUT_FILENO);
    printf("Hello World 2\n");

    dup2(backup,STDOUT_FILENO);
    printf("Hello World 3\n");
    return 0;
}

