#include <53func.h>
#include <mysql/mysql.h>
int main(int argc, char *argv[])
{
    MYSQL * mysql = mysql_init(NULL);
    MYSQL * ret = mysql_real_connect(mysql,"localhost","root",
                                     "123","53test",
                                     0,NULL,0);
    if(ret == NULL){
        fprintf(stderr,"mysql_real_connect:%s\n", mysql_error(mysql));
        return -1;
    }
    char sql[] = "select * from pet;";
    int qret = mysql_query(mysql,sql);
    if(qret != 0){
        fprintf(stderr,"mysql_query 1:%s\n",mysql_error(mysql));
        return -1;
    }
    MYSQL_RES * result = mysql_store_result(mysql);
    char sql1[] = "select * from pet";
    qret = mysql_query(mysql,sql1);
    if(qret != 0){
        fprintf(stderr,"mysql_query 2:%s\n",mysql_error(mysql));
        return -1;
    }
    result = mysql_store_result(mysql);
    printf("rows = %lu, cols = %d\n", mysql_num_rows(result), mysql_num_fields(result));
    MYSQL_ROW row;
    while((row = mysql_fetch_row(result)) != NULL){
        for(int i = 0; i < 6; ++i){
            printf("%s\t", row[i]);
        }
        printf("\n");
    }
    return 0;
}

