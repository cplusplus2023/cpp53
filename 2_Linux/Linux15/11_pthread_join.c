#include <53func.h>
void *threadFunc(void *arg){
    int * pStack = (int *)arg;
    printf("child, *pStack = %d\n", *pStack);
    ++*pStack;
    return (void *)123;
}
int main(int argc, char *argv[])
{
    int stack = 1000;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&stack); // int * --> void *
    //void *retval;
    //pthread_join(tid,&retval);
    //printf("main, stack = %d, retval = %ld\n", stack, (long) retval);
    pthread_join(tid,NULL);
    printf("main, stack = %d\n", stack);
    return 0;
}

