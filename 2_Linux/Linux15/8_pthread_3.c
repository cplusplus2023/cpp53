#include <53func.h>
void *threadFunc(void *arg){
    long value = (long )arg;
    printf("value = %ld\n", value);
}
int main(int argc, char *argv[])
{
    long value = 1001;
    pthread_t tid1,tid2,tid3;
    pthread_create(&tid1,NULL,threadFunc,(void *)value);
    ++value;
    pthread_create(&tid2,NULL,threadFunc,(void *)value);
    ++value;
    pthread_create(&tid3,NULL,threadFunc,(void *)value);
    sleep(3);
    return 0;
}

