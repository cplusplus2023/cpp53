#include <53func.h>
void *threadFunc(void *arg){
    while(1){
        //printf("I still alive!\n");
        pthread_testcancel();
    }
}
int main(int argc, char *argv[])
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL); 
    sleep(1);
    pthread_cancel(tid);
    void *retval;
    pthread_join(tid,&retval);
    printf("main retval = %ld\n",(long) retval);
    return 0;
}

