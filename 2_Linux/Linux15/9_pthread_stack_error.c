#include <53func.h>
void *threadFunc(void *arg){
    int * pStack = (int *)arg;
    printf("child, *pStack = %d\n", *pStack);
    ++*pStack;
}
void func(){
    int stack = 1000;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&stack); // int * --> void *
}
int main(int argc, char *argv[])
{
    func();
    sleep(1);
    return 0;
}

