#include <53func.h>
void *threadFunc(void *arg){
    int * pStack = (int *)arg;
    printf("child, *pStack = %d\n", *pStack);
    ++*pStack;
}
int main(int argc, char *argv[])
{
    int stack = 1000;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&stack); // int * --> void *
    sleep(1);
    printf("main, stack = %d\n", stack);
    return 0;
}

