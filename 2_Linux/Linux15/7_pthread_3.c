#include <53func.h>
void *threadFunc(void *arg){
    int * pValue = (int *)arg;
    printf("value = %d\n", *pValue);
}
int main(int argc, char *argv[])
{
    int value = 1001;
    pthread_t tid1,tid2,tid3;
    pthread_create(&tid1,NULL,threadFunc,&value);
    ++value;
    pthread_create(&tid2,NULL,threadFunc,&value);
    ++value;
    pthread_create(&tid3,NULL,threadFunc,&value);
    sleep(3);
    return 0;
}

