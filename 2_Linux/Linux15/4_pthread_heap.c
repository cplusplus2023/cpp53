#include <53func.h>
void *threadFunc(void *arg){
    // threadFunc的arg遵循的原则：传之前是什么类型，就恢复成什么类型
    int *p = (int *)arg;
    printf("child, Heap = %d\n", *p);
    ++*p;
}
int main(int argc, char *argv[])
{
    // malloc
    int * p = (int *)malloc(sizeof(int));
    *p = 1000;
    // pthread_create
    pthread_t tid;
    // pthread_create 第四个参数用于 主线程向子线程发消息
    pthread_create(&tid,NULL,threadFunc,p); //int * -> void *
    sleep(1);
    printf("parent, Heap = %d\n", *p);
    return 0;
}

