#include <53func.h>
void handler(int signum){
    time_t now = time(NULL);
    printf("signum = %d, curtime = %s\n", signum, ctime(&now));
}
int main(int argc, char *argv[])
{
    signal(SIGPROF,handler);
    handler(0);

    struct itimerval itimer;
    itimer.it_value.tv_sec = 3;
    itimer.it_value.tv_usec = 0;
    itimer.it_interval.tv_sec = 1;
    itimer.it_interval.tv_usec = 0;

    setitimer(ITIMER_PROF,&itimer,NULL);
    while(1);
    return 0;
}

