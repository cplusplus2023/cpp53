#include <53func.h>
void *threadFunc(void *arg){
    printf("I am child thread!\n");
}
int main(int argc, char *argv[])
{
    pthread_t tid;
    pthread_create(&tid,NULL,
                   threadFunc,NULL);
    printf("I am main thread!\n");
    usleep(25);
    return 0;
}

