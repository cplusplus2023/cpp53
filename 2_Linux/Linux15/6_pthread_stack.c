#include <53func.h>
void * threadFunc(void *arg){
    long value = (long )arg;
    printf("child, value = %ld\n", value);
    ++value;
}
int main(int argc, char *argv[])
{
    long value = 1000;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,(void *)value);
    sleep(1);
    printf("main, value = %ld\n", value);
    return 0;
}

