#include "TaskQueue.h"
#include "ThreadPool.h"
int main(int argc, char *argv[])
{
    // ./server 192.168.38.128 1234 3
    ARGS_CHECK(argc,4);
    threadPool_t threadPool;
    int workerNum = atoi(argv[3]);
    initThreadPool(workerNum,&threadPool);
    makeWorker(&threadPool); 
    int sockfd;
    tcpInit(argv[1],argv[2],&sockfd);

    int epfd = epoll_create(1);
    epollAdd(epfd,sockfd);

    struct epoll_event readyset[10];
    while(1){
        int readynum = epoll_wait(epfd,readyset,10,-1);
        for(int i = 0; i < readynum; ++i){
            if(readyset[i].data.fd == sockfd){
                int netfd = accept(sockfd,NULL,NULL);
                pthread_mutex_lock(&threadPool.mutex);
                enQueue(&threadPool.taskQueue,netfd); 
                printf("main thread send a task!\n");
                pthread_cond_signal(&threadPool.cond);
                pthread_mutex_unlock(&threadPool.mutex);
            }
        }
    }
    return 0;
}

