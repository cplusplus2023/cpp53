#ifndef __TASK_QUEUE__
#define __TASK_QUEUE__
#include <53func.h>
typedef struct task_s {
    int netfd;
    struct task_s * pNext;
} task_t;
typedef struct taskQueue_s {
    task_t * pFront;
    task_t * pRear;
    int queueSize;
} taskQueue_t;
int initTaskQueue(taskQueue_t * ptaskQueue);
int enQueue(taskQueue_t * ptaskQueue, int netfd);
int deQueue(taskQueue_t * ptaskQueue);
#endif
