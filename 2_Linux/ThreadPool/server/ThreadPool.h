#ifndef __THREAD_POOL__
#define __THREAD_POOL__
#include <53func.h>
#include "TaskQueue.h"
typedef struct threadPool_s {
    int workerNum;
    pthread_t *tidArr; //初始化的时候额外申请内存
    taskQueue_t taskQueue;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} threadPool_t;
int initThreadPool(int workerNum, threadPool_t * pthreadPool);
int makeWorker(threadPool_t *pthreadPool);
void * threadFunc(void *arg);
int tcpInit(char *ip, char *port, int *psockfd);
int epollAdd(int epfd, int fd);
int epollDel(int epfd, int fd);
int transFile(int sockfd);
#endif
