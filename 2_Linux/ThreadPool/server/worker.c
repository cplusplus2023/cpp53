#include "ThreadPool.h"
int makeWorker(threadPool_t *pthreadPool){
    for(int i = 0; i < pthreadPool->workerNum; ++i){
        pthread_create(&pthreadPool->tidArr[i],NULL,
                       threadFunc, pthreadPool);
    }
    return 0;
}
void * threadFunc(void *arg){
    threadPool_t * pthreadPool = (threadPool_t *)arg;
    while(1){
        pthread_mutex_lock(&pthreadPool->mutex);
        while(pthreadPool->taskQueue.queueSize == 0){
            pthread_cond_wait(&pthreadPool->cond, &pthreadPool->mutex);
        }
        printf("tid = %ld, worker thread got a task!\n",
               pthread_self());
        int netfd = pthreadPool->taskQueue.pFront->netfd;
        deQueue(&pthreadPool->taskQueue);
        pthread_mutex_unlock(&pthreadPool->mutex);
        transFile(netfd);
        close(netfd);
    }
    pthread_exit(NULL);
}
