#include "ThreadPool.h"
int initThreadPool(int workerNum, threadPool_t * pthreadPool){
    pthreadPool->workerNum = workerNum;
    pthreadPool->tidArr = (pthread_t *)calloc(workerNum,sizeof(pthread_t));
    initTaskQueue(&pthreadPool->taskQueue); 
    pthread_mutex_init(&pthreadPool->mutex,NULL);
    pthread_cond_init(&pthreadPool->cond,NULL);
    return 0;
}
