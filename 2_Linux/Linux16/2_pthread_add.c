#include <53func.h>
#define NUM 10000000
int val = 0;
void *threadFunc(void *arg){
    for(int i = 0; i < NUM; ++i){
        ++val;
    }
}
int main(int argc, char *argv[])
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    for(int i = 0; i < NUM; ++i){
        ++val;
    }
    pthread_join(tid,NULL);
    printf("val = %d\n", val);
    return 0;
}

