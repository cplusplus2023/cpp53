#include <53func.h>
typedef struct node_s {
    int num;
    struct node_s * pNext;
} node_t;
typedef struct linklist_s {
    node_t * pHead; 
    node_t * pTail;
    int size;
} linklist_t;
int tailInsert(linklist_t * plinklist, int num){
    node_t * pNew = (node_t *)malloc(sizeof(node_t));
    pNew->num = num;
    pNew->pNext = NULL;

    if(plinklist->size == 0){
        plinklist->pHead = pNew;
        plinklist->pTail = pNew;
    }
    else{
        plinklist->pTail->pNext = pNew;
        plinklist->pTail = pNew;
    }
    ++plinklist->size;
    return 0;
}
int visitList(linklist_t *plinklist){
    node_t * pCur = plinklist->pHead;
    while(pCur){
        printf("%d ", pCur->num);
        pCur = pCur->pNext;
    }
    printf("\n");
    return 0;
}
void * threadFunc(void *arg){
    linklist_t * plinklist = (linklist_t *)arg;
    visitList(plinklist);
    return NULL;
}
int main(int argc, char *argv[])
{
    linklist_t linklist;
    memset(&linklist,0,sizeof(linklist));
    for(int i = 0; i < 8; ++i){
        tailInsert(&linklist,i);
    }
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&linklist); // linklist_t * -> void *
    pthread_join(tid,NULL);
    return 0;
}

