#include <53func.h>
void *threadFunc(void *arg){
    int *pfd = (int *)arg;//void*->int*
    ssize_t sret = write(*pfd,"hello",5);
    ERROR_CHECK(sret,-1,"write");
    //close(*pfd);
}
int main(int argc, char *argv[])
{
    int fd = open("file1",O_RDWR);
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&fd);// int*->void*
    pthread_join(tid,NULL);
    ssize_t sret = write(fd,"world",5);
    ERROR_CHECK(sret,-1,"write main");
    close(fd);
    return 0;
}

