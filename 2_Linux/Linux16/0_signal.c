#include <53func.h>
void handler(int signum, siginfo_t* siginfo, void * p){
    printf("before signum = %d, pid = %d\n", signum, siginfo->si_pid);
    sleep(5);
    printf("after signum = %d, pid = %d\n", signum, siginfo->si_pid);
    sigset_t pending;
    sigpending(&pending);
    if(sigismember(&pending,SIGQUIT)){
        printf("SIGQUIT is pending!\n");
    }
    else{
        printf("SIGQUIT is not pending!\n");
    }
}
int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act,0,sizeof(act));
    act.sa_sigaction = handler;
    act.sa_flags = SA_RESTART|SA_SIGINFO;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask,SIGQUIT);
    sigaction(SIGINT,&act,NULL);

    char buf[4096] = {0};
    ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
    printf("sret = %ld, buf = %s\n", sret, buf);
    return 0;
}

