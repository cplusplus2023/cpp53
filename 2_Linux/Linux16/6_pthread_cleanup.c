#include <53func.h>
void wrapper_freep1(void *p1){
    printf("free p1\n");
    free(p1);
}
void wrapper_freep2(void *p2){
    printf("free p2\n");
    free(p2);
}
void wrapper_closefd3(void * arg){
    // &fd3 int * -> void *
    int * pfd3 = (int *)arg;
    printf("close fd3\n");
    close(*pfd3);
}
void *threadFunc(void *arg){
    void *p1 = malloc(4);
    pthread_cleanup_push(wrapper_freep1,p1);
    void *p2 = malloc(4);
    pthread_cleanup_push(wrapper_freep2,p2);
    int fd3 = open("file1",O_RDWR);
    pthread_cleanup_push(wrapper_closefd3,&fd3);
    //close(fd3);
    pthread_cleanup_pop(1);
    //free(p2);
    pthread_cleanup_pop(1);
    //free(p1);
    ///pthread_cleanup_pop(1);
    pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    pthread_join(tid,NULL);
    return 0;
}

