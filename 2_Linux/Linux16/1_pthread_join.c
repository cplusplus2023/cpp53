#include <53func.h>
void *threadFunc(void *arg){
    printf("Hello world!\n");
}
int main(int argc, char *argv[])
{
    pthread_t tid1, tid2, tid3;
    pthread_create(&tid1,NULL,threadFunc,NULL);
    pthread_create(&tid2,NULL,threadFunc,NULL);
    pthread_create(&tid3,NULL,threadFunc,NULL);

    pthread_join(tid1,NULL);
    pthread_join(tid2,NULL);
    pthread_join(tid3,NULL);
    return 0;
}

