#include <53func.h>
int setnonblock(int fd){
    int flag = fcntl(fd,F_GETFL); // 获取原来的open flag
    flag = flag|O_NONBLOCK; // 新增一个非阻塞属性
    int ret = fcntl(fd,F_SETFL,flag); // 修改 open flag
    ERROR_CHECK(ret,-1,"fcntl");
    return 0;
}
int main(int argc, char *argv[])
{
    char buf[4] = {0};
    setnonblock(STDIN_FILENO);
    while(1){
        bzero(buf,sizeof(buf));
        ssize_t sret = read(STDIN_FILENO,buf,3);
        printf("sret = %ld, buf = %s\n", sret, buf);
        sleep(1);
    }
    return 0;
}

