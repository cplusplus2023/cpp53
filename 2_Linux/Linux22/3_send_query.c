#include <53func.h>
int main(int argc, char *argv[])
{
    char query[] = "GET / HTTP/1.0\r\n"
        "Host:www.baidu.com\r\n"
        "\r\n";
    char hostname[] = "www.baidu.com";
    struct hostent *phost = gethostbyname(hostname);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(80);
    memcpy(&addr.sin_addr, phost->h_addr_list[0], 4);

    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    int ret = connect(sockfd,(struct sockaddr *)&addr,sizeof(addr));
    ERROR_CHECK(ret,-1,"connect");
    send(sockfd,query,strlen(query),0);
    char buf[40960] = {0};
    while(1){
        ssize_t sret = recv(sockfd,buf,sizeof(buf),0);
        if(sret == 0){
            break;
        }
        printf("%s\n", buf);
    }
    return 0;
}

