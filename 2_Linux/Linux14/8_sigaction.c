#include <53func.h>
void handler(int signum){
    printf("before signum = %d\n", signum);
    sleep(5);
    printf("after signum = %d\n", signum);
}
int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act,0,sizeof(act)); // 把每个字段清零
    act.sa_handler = handler; // 回调函数选择一参数的版本
    sigaction(SIGINT,&act,NULL);
    sigaction(SIGQUIT,&act,NULL);
    while(1){
        sleep(1);
    }

    return 0;
}

