#include <53func.h>
void handler(int signum){
    printf("before signum = %d\n", signum);
    sleep(5);
    printf("after signum = %d\n", signum);
    sigset_t pending;
    sigpending(&pending);
    if(sigismember(&pending,SIGQUIT)){
        printf("SIGQUIT is pending!\n");
    }
    else{
        printf("SIGQUIT is not pending!\n");
    }
}
int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act,0,sizeof(act)); // 把每个字段清零
    act.sa_handler = handler; // 回调函数选择一参数的版本
    act.sa_flags = SA_RESTART;
    sigemptyset(&act.sa_mask);
    sigaddset(&act.sa_mask,SIGQUIT);
    // SA_RESTART 自动重启低速系统调用
    // SA_RESETHAND 注册一次 生效一次
    // SA_NODEFER 递送过程中，不屏蔽本信号
    sigaction(SIGINT,&act,NULL);
    sigaction(SIGQUIT,&act,NULL);

    char buf[4096] = {0};
    ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
    printf("sret = %ld, buf = %s\n", sret, buf);
    return 0;
}

