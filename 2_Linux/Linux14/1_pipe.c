#include <53func.h>
int main(int argc, char *argv[])
{
    int fds[2];
    pipe(fds);
    if(fork()){ //父读孩子写
        close(fds[1]);
        char buf[4096] = {0};
        //close(fds[0]);
        sleep(10);
        printf("sleep over!\n");
        ssize_t sret = read(fds[0],buf,sizeof(buf));
        printf("sret = %ld, buf = %s\n", sret, buf);
        int status;
        wait(&status);
        if(WIFEXITED(status)){
            printf("normal exit, return value = %d\n", WEXITSTATUS(status));
        }
        else if(WIFSIGNALED(status)){
            printf("abnormal quit, sig = %d\n", WTERMSIG(status));
        }
    }
    else{
        close(fds[0]);
        close(fds[1]);
        //sleep(10);
        //printf("sleep over!\n");
        //write(fds[1],"howareyou",9);
        //char buf[4096] = {0};
        //int cnt = 0;
        //while(1){
        //    write(fds[1],buf,4096);
        //    printf("cnt = %d\n", cnt++);
        //}
    }

    return 0;
}

