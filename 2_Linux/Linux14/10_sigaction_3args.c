#include <53func.h>
void handler(int signum, siginfo_t *psiginfo, void *p){
    printf("signum = %d\n", signum);
    printf("pid = %d, uid = %d\n", psiginfo->si_pid, psiginfo->si_uid);
}
int main(int argc, char *argv[])
{
    struct sigaction act;
    memset(&act,0,sizeof(act)); // 把每个字段清零
    act.sa_sigaction = handler; // 使用sa_sigaction 而不是 sa_handler
    act.sa_flags = SA_RESTART|SA_SIGINFO;
    // SA_SIGINFO 选用3参数的回调
    sigaction(SIGINT,&act,NULL);
    sigaction(SIGQUIT,&act,NULL);

    char buf[4096] = {0};
    ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
    printf("sret = %ld, buf = %s\n", sret, buf);
    return 0;
}

