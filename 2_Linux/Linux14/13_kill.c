#include <53func.h>
int main(int argc, char *argv[])
{
    // ./13_kill pid
    ARGS_CHECK(argc,2);
    int ret = kill(atoi(argv[1]),9);
    ERROR_CHECK(ret,-1,"kill");
    return 0;
}

