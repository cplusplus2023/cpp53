#include <53func.h>
int main(int argc, char *argv[])
{
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set,SIGQUIT);
    sigprocmask(SIG_BLOCK,&set,NULL);
    sleep(10);
    sigset_t pending;
    sigpending(&pending);
    if(sigismember(&pending,SIGQUIT)){
        printf("SIGQUIT is pending!\n");
    }
    else{
        printf("SIGQUIT is not pending!\n");
    }
    sigprocmask(SIG_UNBLOCK,&set,NULL);
    return 0;
}

