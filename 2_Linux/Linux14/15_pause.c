#include <53func.h>
void handler(int signum){
    time_t now = time(NULL);
    printf("signum = %d, curtime = %s\n", signum, ctime(&now));
}
int main(int argc, char *argv[])
{
    signal(SIGALRM,handler);
    handler(0);
    alarm(10);
    pause();
    return 0;
}

