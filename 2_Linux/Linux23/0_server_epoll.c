#include <53func.h>
typedef struct connInfo_s {
    int isAlive;
    int netfd;
    time_t lastActive;
} connInfo_t;
int main(int argc, char *argv[])
{
    // ./0_server_epoll 192.168.38.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_STREAM,0);
    int flag = 1;
    setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&flag,sizeof(flag));
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[2]));
    addr.sin_addr.s_addr = inet_addr(argv[1]);
    bind(sockfd,(struct sockaddr *)&addr,sizeof(addr));
    listen(sockfd,10);
    char buf[4096];

    connInfo_t connInfo[1024];
    for(int i = 0;i < 1024; ++i){
        connInfo[i].isAlive = 0;
    }
    int curidx = 0;
    //数组/哈希表 fd --> idx
    int fd2idx[1024] = {0};
    for(int fd = 0; fd < 1024; ++fd){
        fd2idx[fd] = -1;
    }

    int epfd = epoll_create(1);
    struct epoll_event event;
    event.events = EPOLLIN;
    event.data.fd = sockfd;
    epoll_ctl(epfd,EPOLL_CTL_ADD,sockfd,&event);

    struct epoll_event readyset[1024];
    while(1){
        int readynum = epoll_wait(epfd,readyset,1024,1000);
        time_t now = time(NULL);
        printf("now = %s\n", ctime(&now));
        for(int i = 0; i < readynum; ++i){
            if(readyset[i].data.fd == sockfd){
                connInfo[curidx].netfd = accept(sockfd,NULL,NULL);
                connInfo[curidx].isAlive = 1;
                connInfo[curidx].lastActive = time(NULL);
                fd2idx[connInfo[curidx].netfd] = curidx;//存储文件描述符和数组下标的对应关系
                printf("curidx = %d, netfd = %d\n", curidx, connInfo[curidx].netfd);

                event.events = EPOLLIN;
                event.data.fd = connInfo[curidx].netfd;
                epoll_ctl(epfd,EPOLL_CTL_ADD,connInfo[curidx].netfd, &event);

                ++curidx;
            }
            else{
                int netfd = readyset[i].data.fd;
                int k = fd2idx[netfd];
                bzero(buf,sizeof(buf));
                ssize_t sret = recv(netfd,buf,sizeof(buf),0);
                printf("recv idx = %d, netfd = %d\n", k, netfd);
                if(sret == 0){
                    printf("%d is closed!\n", k);
                    // connInfo
                    connInfo[k].isAlive = 0;
                    fd2idx[netfd] = -1;
                    // 监听集合
                    epoll_ctl(epfd,EPOLL_CTL_DEL,netfd,NULL);
                    close(netfd);
                    continue;
                }

                connInfo[k].lastActive = time(NULL);
                for(int j = 0; j < curidx; ++j){
                    if(connInfo[j].isAlive == 0 || j == k){
                        continue;
                    }
                    send(connInfo[j].netfd,buf,strlen(buf),0);
                }
            }
        }
        for(int i = 0; i < curidx; ++i){
            if(connInfo[i].isAlive == 1 && now - connInfo[i].lastActive > 10){
                printf("%d is kicked!\n", i);
                connInfo[i].isAlive = 0;
                fd2idx[connInfo[i].netfd] = -1;
                epoll_ctl(epfd,EPOLL_CTL_DEL,connInfo[i].netfd,NULL);
                close(connInfo[i].netfd);
            }
        }
    }
    return 0;
}

