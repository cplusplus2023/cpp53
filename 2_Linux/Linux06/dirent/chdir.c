#include <func.h>

int main(int argc, char* argv[])
{
    // ./chdir path
    // 参数校验
    ARGS_CHECK(argc, 2);
    
    char* cwd = getcwd(NULL, 0);
    puts(cwd);
    free(cwd);

    int ret = chdir(argv[1]);
    ERROR_CHECK(ret, -1, "chdir");

    cwd = getcwd(NULL, 0);
    puts(cwd);
    free(cwd);

    return 0;
}

