#include <func.h>

int main(void)
{
    // ./getcwd
    char* cwd = getcwd(NULL, 0);
    ERROR_CHECK(cwd, NULL, "getcwd");

    puts(cwd);
    free(cwd);
    return 0;
}

