#include <func.h>

// tree.c
void dfs_print(const char* path, int width);	/* width: 缩进的空格数目 */

int directories = 0, files = 0;

int main(int argc, char *argv[])
{
    // ./tree dir
    ARGS_CHECK(argc, 2);

    puts(argv[1]);		// 打印目录的名字

    dfs_print(argv[1], 4);	// 递归打印每一个目录项

    printf("\n%d directories, %d files\n", directories, files);		// 最后打印统计信息
    return 0;
}

void dfs_print(const char* path, int width) {
    // 打开目录流
    DIR* pdir = opendir(path);
    ERROR_CHECK(pdir, NULL, "opendir");

    // 遍历目录流，依次打印每一个目录项
    errno = 0;
    struct dirent* pdirent;
    while ((pdirent = readdir(pdir)) != NULL) {
        // 忽略.和.., 以及隐藏文件
        char* name = pdirent->d_name;
        if (name[0] == '.') {
            continue;
        }

        for(int i = 0; i < width; i++) {
            putchar(' ');
        }
        puts(name);

        if (pdirent->d_type == DT_DIR) {
            directories++;
            // 拼接路径
            char subpath[1024];
            sprintf(subpath, "%s/%s", path, name);
            dfs_print(subpath, width + 4);
        } else {
            files++;
        }
    } // pdirent == NULL

    if (errno) {
        closedir(pdir);
        perror("readdir");
        exit(1);
    }

    closedir(pdir);
}
