#include <func.h>

int main(int argc, char* argv[])
{
    // ./readdir dir
    ARGS_CHECK(argc, 2);

    // 打开目录流
    DIR* pdir = opendir(argv[1]);
    ERROR_CHECK(pdir, NULL, "opendir");

    // 遍历目录流，读取每一个目录项
    errno = 0;
    struct dirent* pdirent = NULL;
    
    while ((pdirent = readdir(pdir)) != NULL) {
        printf("%ld %ld %hu %d %s\n", 
               pdirent->d_ino,
               pdirent->d_off,
               pdirent->d_reclen,
               pdirent->d_type,
               pdirent->d_name);

        long pos = telldir(pdir);
        printf("pos: %ld\n", pos);
    } // pdirent == NULL 

    if (errno) {
        perror("readdir");
        closedir(pdir);
        exit(1);
    }

    // 关闭目录流
    closedir(pdir);
    return 0;
}

