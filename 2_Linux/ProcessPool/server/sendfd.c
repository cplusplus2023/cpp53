#include <53func.h>
int sendfd(int sockfd, int fdtosend){
    struct msghdr hdr;
    bzero(&hdr,sizeof(hdr)); // name --> NULL namelen --> 0
    // 正文部分
    char buf1[] = "hello"; 
    struct iovec iov[1];
    iov[0].iov_base = buf1; // iov 收集离散区域的地址和长度
    iov[0].iov_len = 5;
    hdr.msg_iov = iov; // hdr和iov联系起来
    hdr.msg_iovlen = 1;
    // 控制部分 data打算放一个文件描述符
    struct cmsghdr * pcmsg = (struct cmsghdr *)malloc(CMSG_LEN(sizeof(int))); 
    pcmsg->cmsg_len = CMSG_LEN(sizeof(int));
    pcmsg->cmsg_level = SOL_SOCKET;
    pcmsg->cmsg_type = SCM_RIGHTS; // 说明发的是文件对象
    *(int *)CMSG_DATA(pcmsg) = fdtosend;
    hdr.msg_control = pcmsg;
    hdr.msg_controllen = CMSG_LEN(sizeof(int));
    sendmsg(sockfd,&hdr,0);
    return 0;
}
int recvfd(int sockfd, int *pfdtorecv){
    struct msghdr hdr;
    bzero(&hdr,sizeof(hdr)); // name --> NULL namelen --> 0
    // 正文部分
    char buf1[6] = {0}; 
    struct iovec iov[1];
    iov[0].iov_base = buf1; // iov 收集离散区域的地址和长度
    iov[0].iov_len = 5;
    hdr.msg_iov = iov; // hdr和iov联系起来
    hdr.msg_iovlen = 1;
    // 控制部分 data打算放一个文件描述符
    struct cmsghdr * pcmsg = (struct cmsghdr *)malloc(CMSG_LEN(sizeof(int))); 
    pcmsg->cmsg_len = CMSG_LEN(sizeof(int));
    pcmsg->cmsg_level = SOL_SOCKET;
    pcmsg->cmsg_type = SCM_RIGHTS; // 说明发的是文件对象
    hdr.msg_control = pcmsg;
    hdr.msg_controllen = CMSG_LEN(sizeof(int));
    recvmsg(sockfd,&hdr,0);
    *pfdtorecv = *(int *)CMSG_DATA(pcmsg);
    return 0;
}

