#include "head.h"
int makeWorker(int workerNum, workerData_t *workerDataArr){
    for(int i = 0; i < workerNum; ++i){
        int fds[2];
        socketpair(AF_UNIX,SOCK_STREAM,0,fds);
        pid_t pid = fork();
        if(pid == 0){
            // 只有子进程
            close(fds[0]);
            handleEvent(fds[1]);
            // 让子进程离不开这个if结构
        }
        close(fds[1]);
        workerDataArr[i].status = FREE;
        workerDataArr[i].pid = pid;
        workerDataArr[i].socketpair = fds[0];
        printf("%d worker, pid = %d, socketfd = %d\n", i, workerDataArr[i].pid, fds[0]);
    }
    return 0;
}
int handleEvent(int sockfd){
    while(1){
        // 接收任务
        int netfd;
        recvfd(sockfd,&netfd);
        // 执行任务
        transFile(netfd);
        close(netfd);
        // 任务完成之后 通知父进程
        pid_t pid = getpid();
        write(sockfd,&pid,sizeof(pid));
    }
}
