#include "head.h"
int tcpInit(char *ip, char *port, int *psockfd){
    *psockfd = socket(AF_INET,SOCK_STREAM,0);
    int flag = 1;
    setsockopt(*psockfd,SOL_SOCKET,SO_REUSEADDR,&flag,sizeof(flag));
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(port));
    addr.sin_addr.s_addr = inet_addr(ip);
    bind(*psockfd,(struct sockaddr *)&addr,sizeof(addr));
    listen(*psockfd,10);
    return 0;
}
