#include <53func.h>
enum {
    FREE,
    BUSY
};
typedef struct workerData_s {
    pid_t pid;
    int status; // FREE BUSY
    int socketpair; // 主进程和子进程通信用的管道
} workerData_t;
int makeWorker(int workerNum, workerData_t *workerDataArr);
int tcpInit(char *ip, char *port, int *psockfd);
int epollAdd(int epfd, int fd);
int epollDel(int epfd, int fd);
int sendfd(int sockfd, int fdtosend);
int recvfd(int sockfd, int *pfdtorecv);
int handleEvent(int sockfd);
int transFile(int sockfd);
