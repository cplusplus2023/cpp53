#include <53func.h>
int main(int argc, char *argv[])
{
    // ./5_redirect file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    printf("\n");
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
    dup(fd);
    dup(fd);
    printf("Hello\n");
    fprintf(stderr,"World\n");
    return 0;
}

