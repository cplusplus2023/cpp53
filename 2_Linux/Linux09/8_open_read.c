#include <53func.h>
int main(int argc, char *argv[])
{
    //  ./8_open_read 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open");
    printf("read side is opened!\n");
    
    close(fdr);
    sleep(1000);
    //sleep(5);
    //printf("sleep over!\n");
    //char buf[4096] = {0};
    //ssize_t sret = read(fdr,buf,sizeof(buf));
    //printf("sret = %ld, buf = %s\n", sret, buf);
    //sret = read(fdr,buf,sizeof(buf));
    //printf("sret = %ld, buf = %s\n", sret, buf);
    return 0;
}

