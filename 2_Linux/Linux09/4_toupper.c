#include <53func.h>
int main(int argc, char *argv[])
{
    // ./4_toupper text.txt 
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ftruncate(fd,498);
    char *p = (char *)mmap(NULL,498,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    for(int i = 0; i < 498; ++i){
        if(islower(p[i])){
            p[i] -= 32;
        }
        else if(ispunct(p[i])){
            p[i] = ' ';
        }
    }
    munmap(p,498);
    close(fd);
    return 0;
}

