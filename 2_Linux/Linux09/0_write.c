#include <53func.h>
typedef struct student_s {
    int num;
    //char *name;
    char name[256];
    float score;
} student_t;
int main(int argc, char *argv[])
{
    // ./0_write file1
    ARGS_CHECK(argc,2);
    student_t stu[3] = {
        {1001,"Caixukun",88.8},
        {1002,"Wuyifan",59.9},
        {1003,"Liyifeng",77.77}
    };
    int fd = open(argv[1],O_WRONLY|O_CREAT|O_TRUNC,0666);
    ERROR_CHECK(fd,-1,"open");
    
    write(fd,stu,sizeof(stu));

    close(fd);
    return 0;
}

