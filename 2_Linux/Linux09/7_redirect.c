#include <53func.h>
int main(int argc, char *argv[])
{
    // ./7_redirect file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);

    printf("我过来啦\n");

    int backup = 5;
    dup2(STDOUT_FILENO,backup);
    dup2(fd,STDOUT_FILENO);

    printf("我过去啦\n");

    dup2(backup,STDOUT_FILENO);

    printf("我又过来啦\n");
    return 0;
}

