#include <53func.h>
int main(int argc, char *argv[])
{
    // ./2_same file1 file2 
    ARGS_CHECK(argc,3);
    int fd1 = open(argv[1],O_RDWR);
    int fd2 = open(argv[2],O_RDWR);

    ssize_t sret1,sret2;
    char buf1[4096], buf2[4096];
    while(1){
        memset(buf1,0,sizeof(buf1));
        sret1 = read(fd1,buf1,sizeof(buf1));
        memset(buf2,0,sizeof(buf2));
        sret2 = read(fd2,buf2,sizeof(buf2));
        if(sret1 != sret2){
            printf("Not the same !\n");
            break;
        }

        if(memcmp(buf1,buf2,sret1) != 0){
            printf("Not the same !\n");
            break;
        }

        if(sret1 == 0){
            printf("The same!\n");
            break;
        }
    }
    close(fd2);
    close(fd1);
    return 0;
}

