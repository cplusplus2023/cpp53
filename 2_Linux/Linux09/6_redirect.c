#include <53func.h>
int main(int argc, char *argv[])
{
    // ./6_redirect file1 file2
    ARGS_CHECK(argc,3);
    int fd1 = open(argv[1],O_RDWR);
    int fd2 = open(argv[2],O_RDWR);
    printf("\n");
    close(STDOUT_FILENO);
    dup(fd1);
    close(STDERR_FILENO);
    dup(fd2);

    printf("Hello\n");
    fprintf(stderr,"World\n");
    return 0;
}

