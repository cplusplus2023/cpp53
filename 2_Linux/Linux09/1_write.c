#include <53func.h>
typedef struct student_s {
    int num;
    //char *name;
    char name[256];
    float score;
} student_t;
int main(int argc, char *argv[])
{
    // ./0_write file1
    ARGS_CHECK(argc,2);
    student_t stu[3] = {
        {1001,"Caixukun",88.8},
        {1002,"Wuyifan",59.9},
        {1003,"Liyifeng",77.77}
    };
    int fd = open(argv[1],O_RDWR|O_CREAT|O_TRUNC,0666);
    ERROR_CHECK(fd,-1,"open");
    
    write(fd,stu,sizeof(stu));

    lseek(fd,0,SEEK_SET);

    student_t stuToRead[3];
    ssize_t sret = read(fd,stuToRead,sizeof(stuToRead));
    ERROR_CHECK(sret,-1,"read");
    for(int i = 0; i < 3; ++i){
        printf("%d %s %5.2lf\n",
               stuToRead[i].num, stuToRead[i].name, stuToRead[i].score);
    }
    close(fd);
    return 0;
}

