#include <53func.h>
typedef struct student_s {
    int num;
    //char *name;
    char name[256];
    float score;
} student_t;
int main(int argc, char *argv[])
{
    // ./0_read file1
    ARGS_CHECK(argc,2);
    student_t stu[3]; 
    int fd = open(argv[1],O_RDONLY);
    ERROR_CHECK(fd,-1,"open");
    
    read(fd,stu,sizeof(stu));

    for(int i = 0; i < 3; ++i){
        printf("%d %s %5.2f\n",
               stu[i].num,stu[i].name,stu[i].score);
    }
    close(fd);
    return 0;
}

