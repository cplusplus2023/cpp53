#include <52func.h>
int main(int argc, char *argv[])
{
    // ./5_read_binary file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    int i;
    ssize_t sret = read(fd,&i,sizeof(i));
    printf("sret = %ld, i = %d\n", sret, i);
    close(fd);
    return 0;
}

