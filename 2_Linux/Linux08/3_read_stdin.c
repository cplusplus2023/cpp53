#include <52func.h>
int main()
{
    // stdin ---> 0
    char buf[4096] = {0};
    ssize_t sret = read(STDIN_FILENO,buf,4096);
    ERROR_CHECK(sret,-1,"read");
    printf("sret = %ld, buf = %s\n", sret, buf);
    return 0;
}

