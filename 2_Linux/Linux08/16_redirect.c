#include <53func.h>
int main(int argc, char *argv[])
{
    // ./16_redirect file1 
    ARGS_CHECK(argc,2);
    int oldfd = open(argv[1],O_RDWR);
    printf("You can see me!\n");
    printf("oldfd = %d\n", oldfd);
    close(STDOUT_FILENO);
    int newfd = dup(oldfd);
    printf("newfd = %d\n", newfd);
    printf("You can see me?\n");
    return 0;
}

