#include <53func.h>
int main(int argc, char *argv[])
{
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    off_t filesize = lseek(fd,0,SEEK_END);
    printf("filesize = %ld\n", filesize);
    return 0;
}

