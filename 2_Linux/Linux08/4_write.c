#include <52func.h>
int main(int argc, char *argv[])
{
    // ./4_write file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_WRONLY);
    ERROR_CHECK(fd,-1,"open");
    char buf[1024] = "Hello";
    //ssize_t sret = write(fd,buf,sizeof(buf));
    ssize_t sret = write(fd,buf,strlen(buf));
    printf("sret = %ld\n", sret);
    close(fd);
    return 0;
}

