#include <52func.h>
int main(int argc, char *argv[])
{
    // ./5_write_binary file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    int i = 12345678;
    ssize_t sret = write(fd,&i,sizeof(i));
    printf("sret = %ld\n", sret);
    close(fd);
    return 0;
}

