#include <53func.h>
int main(int argc, char *argv[])
{
    // ./9_mmap file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR); // 必须写O_RDWR
    ERROR_CHECK(fd,-1,"open");
    // 建议 执行mmap之前，都执行一下ftruncate
    ftruncate(fd,10);
    char * p = (char *)mmap(NULL,10,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    ERROR_CHECK(p,MAP_FAILED,"mmap");
    for(int i = 0; i < 10; ++i){
        printf("%c", p[i]); // p[i] 访问内存的方式
    }
    printf("\n");
    p[9] = 'D'; // 修改内存
    munmap(p,10);
    close(fd);
    return 0;
}

