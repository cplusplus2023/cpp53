#include <52func.h>
int main(int argc, char *argv[])
{
    // ./6_mycp src dest
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY|O_CREAT|O_TRUNC, 0666);
    ERROR_CHECK(fdw,-1,"open fdw");

    char buf[4096] = {0};
    ssize_t sret;
    sret = read(fdr,buf,sizeof(buf));
    // write(fdw, buf, sizeof(buf)); 错误的 总是4096
    // write(fdw,buf,strlen(buf)); strlen不能获取二进制数据的长度
    write(fdw, buf, sret);
    close(fdw);
    close(fdr);
    return 0;
}

