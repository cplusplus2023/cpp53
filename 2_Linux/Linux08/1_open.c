#include <52func.h>
int main(int argc, char *argv[])
{
    // ./1_open file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR|O_CREAT,0666);
    // 0666 依然会受到umask的影响
    ERROR_CHECK(fd,-1,"open");
    printf("fd = %d\n", fd);
    close(fd);
    return 0;
}

