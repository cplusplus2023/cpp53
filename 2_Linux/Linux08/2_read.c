#include <52func.h>
int main(int argc, char *argv[])
{
    // ./2_read file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDONLY);
    ERROR_CHECK(fd,-1,"open");
    printf("fd = %d\n", fd);
    char buf[1024] = {0};
    ssize_t sret = read(fd,buf,3);
    printf("sret = %ld, buf = %s\n", sret, buf);
    memset(buf,0,1024); // read之前记得清空数据
    sret = read(fd,buf,3);
    printf("sret = %ld, buf = %s\n", sret, buf);
    memset(buf,0,1024); // read之前记得清空数据
    sret = read(fd,buf,3);
    printf("sret = %ld, buf = %s\n", sret, buf);
    memset(buf,0,1024); // read之前记得清空数据
    sret = read(fd,buf,3);
    printf("sret = %ld, buf = %s\n", sret, buf);
    memset(buf,0,1024); // read之前记得清空数据
    sret = read(fd,buf,3);
    printf("sret = %ld, buf = %s\n", sret, buf);
    close(fd);
    return 0;
}

