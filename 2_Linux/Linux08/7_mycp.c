#include <52func.h>
int main(int argc, char *argv[])
{
    // ./7_mycp src dest
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY|O_CREAT|O_TRUNC, 0666);
    ERROR_CHECK(fdw,-1,"open fdw");

    // 增量编写法
    char buf[4096*1024] = {0};
    //char buf[1] = {0};
    ssize_t sret;
    while(1){
        memset(buf,0,sizeof(buf));
        sret = read(fdr,buf,sizeof(buf));
        if(sret == 0){
            break;
        }
        write(fdw, buf, sret);
    }
    close(fdw);
    close(fdr);
    return 0;
}

