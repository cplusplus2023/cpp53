#include <53func.h>
int main(int argc, char *argv[])
{
    // ./14_redirect file1 
    ARGS_CHECK(argc,2);
    printf("You can see me!\n");
    close(STDOUT_FILENO);
    int fd = open(argv[1],O_RDWR);
    printf("fd = %d\n", fd);
    printf("You can see me?\n");
    return 0;
}

