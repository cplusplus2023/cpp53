#include <53func.h>
int main(int argc, char *argv[])
{
    // ./2_gethostbyname www.baidu.com
    ARGS_CHECK(argc,2);
    struct hostent * phost = gethostbyname(argv[1]);
    if(phost == NULL){
        herror("gethostbyname");
        return -1;
    }
    printf("official name = %s\n", phost->h_name);
    for(int i = 0; phost->h_aliases[i] != NULL; ++i){
        printf("alias name = %s\n", phost->h_aliases[i]);
    }
    for(int i = 0; phost->h_addr_list[i] != NULL; ++i){
        char buf[256] = {0};
        inet_ntop(phost->h_addrtype, phost->h_addr_list[i],
                  buf,sizeof(buf));
        printf("buf = %s\n", buf);
    }
    return 0;
}

