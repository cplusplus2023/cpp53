#include <53func.h>
int main(int argc, char *argv[])
{
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    int ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"lock 1");
    printf("lock 1\n");
    ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"lock 2");
    printf("lock 2\n");
    
    pthread_mutex_unlock(&mutex);
    pthread_mutex_unlock(&mutex);
    return 0;
}

