#include <53func.h>
int main(int argc, char *argv[])
{
    pthread_mutexattr_t mutexattr;
    pthread_mutexattr_init(&mutexattr);
    pthread_mutexattr_settype(&mutexattr,PTHREAD_MUTEX_ERRORCHECK);
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex,&mutexattr);
    int ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"lock 1");
    printf("lock 1\n");
    ret = pthread_mutex_lock(&mutex);
    THREAD_ERROR_CHECK(ret,"lock 2");
    printf("lock 2\n");
    
    pthread_mutex_unlock(&mutex);
    pthread_mutex_unlock(&mutex);
    return 0;
}

