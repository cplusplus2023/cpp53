#include <53func.h>
typedef struct shareRes_s {
    int flag;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
} shareRes_t;
void A(){
    printf("Before A\n");
    sleep(3);
    printf("After A\n");
}
void B(){
    printf("Before B\n");
    sleep(3);
    printf("After B\n");
}
void * threadFunc(void *arg){
    shareRes_t * pShareRes = (shareRes_t *)arg;
    pthread_mutex_lock(&pShareRes->mutex);
    if(pShareRes->flag == 0){
        pthread_cond_wait(&pShareRes->cond,&pShareRes->mutex);
    }
    pthread_mutex_unlock(&pShareRes->mutex);

    B();
    pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
    shareRes_t shareRes;
    shareRes.flag = 0;
    pthread_mutex_init(&shareRes.mutex,NULL);
    pthread_cond_init(&shareRes.cond,NULL);
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&shareRes);
    
    A();
    pthread_mutex_lock(&shareRes.mutex);
    shareRes.flag = 1;
    pthread_cond_signal(&shareRes.cond);
    pthread_mutex_unlock(&shareRes.mutex);

    pthread_join(tid,NULL);
    return 0;
}

