#include <53func.h>
#define NUM 10000000
void * threadFunc(void *arg){
    int * pValue = (int *)arg;
    for(int i = 0; i < NUM; ++i){
        ++*pValue;
    }
    pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
    int value = 0;
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,&value);
    for(int i = 0; i < NUM; ++i){
        ++value;
    }
    pthread_join(tid,NULL);
    printf("value = %d\n", value);
    return 0;
}

