#include <53func.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
void *threadFunc(void *arg){
    pthread_mutex_lock(&mutex);
    printf("I am child thread. I got a lock!\n");
    pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    sleep(1);
    pthread_mutex_lock(&mutex);
    printf("I am main thread. I got a lock!\n");
    pthread_mutex_unlock(&mutex);
    pthread_join(tid,NULL);
    return 0;
}

