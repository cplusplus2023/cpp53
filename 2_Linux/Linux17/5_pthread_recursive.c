#include <53func.h>
pthread_mutex_t mutex;
void * threadFunc(void *arg){
    sleep(1);
    pthread_mutex_lock(&mutex);
    printf("child lock 1\n");
    pthread_mutex_unlock(&mutex);
    pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
    pthread_mutexattr_t mutexattr;
    pthread_mutexattr_init(&mutexattr);
    pthread_mutexattr_settype(&mutexattr,PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&mutex,&mutexattr);
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    pthread_mutex_lock(&mutex);
    printf("main lock 1\n");
    sleep(3);
    pthread_mutex_lock(&mutex);
    printf("main lock 2\n");
    printf("main unlock 1\n");
    pthread_mutex_unlock(&mutex);
    printf("main unlock 2\n");
    pthread_mutex_unlock(&mutex);
    pthread_join(tid,NULL);
    return 0;
}

