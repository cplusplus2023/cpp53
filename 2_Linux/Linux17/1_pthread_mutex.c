#include <53func.h>
#define NUM 10000000
typedef struct shareRes_s {
    int value;
    pthread_mutex_t mutex;
} shareRes_t;
void * threadFunc(void *arg){
    shareRes_t * pShareRes = (shareRes_t *)arg;
    for(int i = 0;i < NUM; ++i){
        pthread_mutex_lock(&pShareRes->mutex);
        ++pShareRes->value;
        pthread_mutex_unlock(&pShareRes->mutex);
    }
    pthread_exit(NULL);
}
int main(int argc, char *argv[])
{
    shareRes_t shareRes;
    shareRes.value = 0;
    pthread_mutex_init(&shareRes.mutex,NULL);
    pthread_t tid;
    struct timeval begTime,endTime;
    gettimeofday(&begTime,NULL);
    pthread_create(&tid,NULL,threadFunc,&shareRes);// shareRes_t * --> void *
    for(int i = 0; i < NUM; ++i){
        pthread_mutex_lock(&shareRes.mutex);
        ++shareRes.value;
        pthread_mutex_unlock(&shareRes.mutex);
    }
    pthread_join(tid,NULL);
    gettimeofday(&endTime,NULL);
    printf("total time = %ld us\n",(endTime.tv_sec - begTime.tv_sec)*1000000 + endTime.tv_usec - begTime.tv_usec);
    printf("value = %d\n", shareRes.value);
    return 0;
}

