#include <53func.h>
int main()
{
    int fds[2];
    pipe(fds);
    printf("fds[0] = %d, fds[1] = %d\n", fds[0], fds[1]);
    // fds[0] 读
    // fds[1] 写
    write(fds[1],"niganma",7);
    char buf[4096];
    read(fds[0],buf,sizeof(buf));
    printf("read from pipe, buf = %s\n", buf);
    return 0;
}

