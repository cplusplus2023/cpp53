#include <53func.h>
int main(int argc, char *argv[])
{
    int fds[2];
    pipe(fds);
    if(fork()){
        // 父进程
        close(fds[0]); // 关掉读端
        write(fds[1],"aiyou",5);
        close(fds[1]);
        wait(NULL);
    }
    else {
        // 子进程
        close(fds[1]); // 关掉写端
        char buf[4096] = {0};
        read(fds[0],buf,sizeof(buf));
        printf("child, message from pipe, buf = %s\n", buf);
        close(fds[0]);
    }
    return 0;
}

