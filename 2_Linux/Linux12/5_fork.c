#include <53func.h>
int global = 3;
int main(int argc, char *argv[])
{
    int stack = 1;
    int *pHeap = (int *)malloc(sizeof(int));
    *pHeap = 2;
    if(fork()){
        // 非0 父进程
        printf("I am parent, stack = %d, heap = %d, global = %d\n",
               stack, *pHeap, global);
        sleep(1);
    }
    else{
        // 为0 子进程
        printf("I am child, stack = %d, heap = %d, global = %d\n",
               stack, *pHeap, global);
    }
    return 0;
}

