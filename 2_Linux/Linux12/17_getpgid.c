#include <53func.h>
int main(int argc, char *argv[])
{
    if(fork()){
        printf("I am parent, pid = %d, pgid = %d\n",
               getpid(), getpgid(0));
        while(1){
            sleep(1);
        }
        wait(NULL);
    }
    else{
        printf("I am child, pid = %d, pgid = %d\n",
               getpid(), getpgid(0));
        setpgid(0,0);//脱离原来的进程组，成立新的
        printf("I am child, pid = %d, pgid = %d\n",
              getpid(), getpgid(0));
        while(1){
            sleep(1);
        }
    }
    return 0;
}

