#include <53func.h>
void Daemon(){
    // 创建一个会话
    if(fork()){
        exit(0); // 父进程直接终止
    }
    setsid();
    // 关闭所有的输入和输出
    for(int i = 0; i < 3; ++i){
        close(i);
    }
    // 重置和启动环境相关的属性
    chdir("/");
    umask(0);
}
int main(int argc, char *argv[])
{
    Daemon();
    for(int i = 0; i < 20; ++i){
        syslog(LOG_INFO,"Daemond i = %d\n", i);
        sleep(3);
    }
    return 0;
}

