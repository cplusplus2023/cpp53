#include <53func.h>
int main(int argc, char *argv[])
{
    printf("Hello\n");
    pid_t pid = fork();
    if(pid == 0){
        printf("I am child! pid = %d\n", pid);
    }
    else{
        printf("I am parent! pid = %d\n", pid);
        //sleep(1);
    }
    return 0;
}

