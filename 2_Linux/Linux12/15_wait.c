#include <53func.h>
int main(int argc, char *argv[])
{
    if(fork()){
        int status;
        wait(&status);
        if(WIFEXITED(status)){
            printf("normal exit! return value = %d\n", WEXITSTATUS(status));
        }
        else if(WIFSIGNALED(status)){
            printf("abnormal quit! terminal signal = %d\n",WTERMSIG(status));
        }
    }
    else{
        //return -1;
        while(1){
            sleep(1);
        }
    }
}

