#include <53func.h>
int main(int argc, char *argv[])
{
    if(fork()){
        printf("I am parent, pid = %d\n", getpid());
        while(1){
            sleep(1);
        }
    }
    else{
        printf("I am child, pid = %d\n", getpid());
    }
    return 0;
}

