#include <53func.h>
int main(int argc, char *argv[])
{
    // ./add 3 4
    printf("pid = %d\n", getpid());
    ARGS_CHECK(argc,3);
    int lhs = atoi(argv[1]);
    int rhs = atoi(argv[2]);
    printf("add = %d\n", lhs + rhs);
    return 0;
}

